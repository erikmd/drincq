(* Copyright (c) ENS de Lyon and Inria. All rights reserved. *)

Require Import ZArith MoreZArith.
Require Import Reals.
Require Import Flocq.Core.Fcore_Raux.
Require Import Psatz.
Require Import Tactics.

Set Implicit Arguments.

Open Scope R_scope.
Open Scope Z_scope.

(** Sign of a real number *)
Definition Rsign x :=
match Rcompare x R0 with
| Lt => (-1)%R
| Eq => R0
| Gt => 1%R
end.

Theorem Rabs_Rsign_Rmult : forall x y, x <> R0 -> Rabs (Rsign x * y) = Rabs y.
Proof.
intros x y Zy.
unfold Rsign; destruct (Rcompare_spec x R0) as [LT|EQ|GT].
now replace (-1 * y)%R with (-y)%R by ring; rewrite Rabs_Ropp.
contradiction.
now replace (1 * y)%R with y by ring.
Qed.

Theorem Rabs_Rsign_Rminus :
  forall x y, x <> R0 -> (0 <= y)%R -> Rabs (Rsign x * y - x) = Rabs (y - Rabs x)%R.
Proof.
intros x y Zx Py.
unfold Rsign; destruct (Rcompare_spec x R0) as [LT|EQ|GT].
(* LT *)
replace (-1 * y - x)%R with (- (y + x))%R by ring; rewrite Rabs_Ropp.
now f_equal; rewrite Rabs_left; try ring.
(* EQ *)
contradiction.
(* GT *)
replace (1 * y - x)%R with (y - x)%R by ring.
now f_equal; rewrite Rabs_right; auto with real; apply Rgt_ge.
Qed.

Theorem Rmult_Rsign_Rabs : forall x : R,
( x = Rsign x * Rabs x )%R.
Proof.
intros x.
unfold Rsign; destruct (Rcompare_spec x R0) as [LT|EQ|GT].
(* LT *)
rewrite Rabs_left; trivial; field.
(* EQ *)
now rewrite EQ, Rmult_0_l.
(* GT *)
rewrite Rabs_pos_eq; [field|now apply Rlt_le].
Qed.

(** Parity of the radix *)

Theorem Z2R_beta_even :
  forall b, (exists n, Z2R n = Z2R b / 2)%R -> Zeven b = true.
Proof.
clear; intros b [n Hn].
assert (Z2R b = Z2R (n * 2))%R as HA.
rewrite Z2R_mult, Hn; simpl; field.
assert (b = n * 2)%Z as HB by now apply eq_Z2R.
rewrite HB.
destruct n; trivial.
(* pose proof Zpos_xO as H'. *)
now rewrite Zmult_comm.
now rewrite Zmult_comm.
Qed.

Theorem Z2R_beta_even_div_two :
  forall b, Zeven b = true -> (Z2R b / 2)%R = Z2R (Zdiv b 2).
Proof.
clear; intros b Hb.
destruct (Zeven_ex b) as [p Hp].
rewrite Hb in Hp.
rewrite Zplus_0_r in Hp.
rewrite Hp.
replace (2 * p / 2)%Z with p.
rewrite Z2R_mult; simpl.
replace (2 * Z2R p / 2)%R with (Z2R p); trivial.
field.
now rewrite Zmult_div_two.
Qed.

(** About Z2R *)

Theorem Rabs_Z2R_succ : forall m beta e, 0 <= e ->
( Rabs (Z2R m) < bpow beta e + 1 -> Rabs (Z2R m) <= bpow beta e )%R.
Proof.
intros m beta e Ze.
rewrite <- Z2R_Zpower with (1 := Ze).
rewrite <- Z2R_abs.
rewrite <- (Z2R_plus _ 1).
intro H.
apply Z2R_le.
apply Zlt_succ_le.
now apply lt_Z2R.
Qed.

(** Powers of the radix *)
Section Powers.
Variable beta : radix.

Definition is_pow (x : R) := exists e, Rabs x = bpow beta e.

Theorem is_pow_dec : forall (x : R), {is_pow x} + {~ is_pow x}.
Proof.
intros x.
(* destruct (total_order_T x 0) as [[Nx|Zx]|Px]. *)
pose (e := ln_beta beta x).
destruct e as [e E].
destruct (Req_EM_T x 0) as [Zx|NZx].
(* x = R0 *)
right.
intros Kx.
destruct Kx as [n Hn].
rewrite Zx in Hn.
pose proof bpow_gt_0 beta n.
rewrite Rabs_R0 in Hn.
rewrite Hn in H.
now apply Rlt_irrefl in H.
(* x <> R0 *)
specialize (E NZx).
destruct Rle_lt_or_eq_dec with (1 := (proj1 E)) as [I|J].
(* _ < Rabs x *)
right.
intro Bx.
destruct Bx as [n Hn].
pose proof ln_beta_bpow beta n as Hb.
rewrite <- Hn in Hb.
pose proof ln_beta_unique beta x e E as Hunq.
rewrite ln_beta_abs in Hb.
rewrite Hunq in Hb.
assert (n = e - 1)%Z as HA by auto with zarith.
rewrite HA in Hn.
rewrite Hn in I.
now apply Rlt_irrefl in I.
(* _ = Rabs x *)
left.
exists (e - 1).
now symmetry.
Qed.

Theorem is_pow_opp : forall x : R, is_pow x -> is_pow (- x).
Proof.
intros x [e Bx].
red.
exists e.
now rewrite Rabs_Ropp.
Qed.

End Powers.

(** Inequalities *)

Theorem Rneq_lt : forall x y, x <> y -> (x < y \/ y < x)%R.
Proof.
intros x y H.
now destruct (Rtotal_order x y) as [I|[J|K]]; [left|contradiction|right].
Qed.

Theorem Rabs_Rdiv_pos : forall x y, (0 < y)%R -> Rabs (x / y) = (Rabs x / y)%R.
Proof.
intros.
unfold Rdiv.
rewrite Rabs_mult, Rabs_Rinv.
rewrite (Rabs_pos_eq y).
apply refl_equal.
now apply Rlt_le.
now apply Rgt_not_eq.
Qed.

Theorem R0_lt_dec : forall x : R, (x <> 0 -> {0 < x} + {x < 0})%R.
(* Similar to Rneq_lt *)
Proof.
intros x NZx.
now destruct (total_order_T x 0) as [[L|E]|G]; [right|contradiction|left].
Qed.

Theorem Rdiv_mult_distr :
  forall x y z : R,
    y <> R0 -> z <> R0 -> (x / (y * z) = (x / y) / z)%R.
Proof.
intros x y z NZy NZz.
unfold Rdiv.
rewrite Rinv_mult_distr; trivial.
now auto with real.
Qed.

Theorem Rdiv_pos :
  forall x y : R, (0 < x)%R -> (0 < y)%R -> (0 < x / y)%R.
Proof.
intros x y Px Py.
unfold Rdiv.
apply Rinv_0_lt_compat in Py.
now apply Rmult_lt_0_compat.
Qed.

Theorem Rdiv_half_lt :
  forall x : R, (0 < x)%R -> (x / 2 < x)%R.
Proof.
now intros; simp2; psatzl R.
Qed.

Theorem Rinv_lt_1 :
  forall y : R, (1 < y)%R -> (/ y < 1)%R.
Proof.
intros y Hy.
apply Rmult_lt_reg_l with y.
now apply Rlt_trans with (2 := Hy); auto with real.
rewrite Rmult_1_r, Rinv_r; trivial.
apply Rgt_not_eq; apply Rlt_gt.
now apply Rlt_trans with (2 := Hy); auto with real.
Qed.

Theorem Rdiv_radix_lt :
  forall (beta : radix) x, (0 < x)%R -> (x / Z2R beta < x)%R.
intros beta x Px; simpl.
pose proof radix_gt_1 beta as h1.
pose proof Z2R_lt _ _ h1 as h2.
unfold Rdiv.
pose proof Rinv_lt_1 h2 as h3.
rewrite <- Rmult_1_r with (r := x) at 2.
now apply Rmult_lt_compat_l.
Qed.

Theorem Rdiv_opp_l : forall x y : R, y <> R0 -> ( -x / y = -(x / y) )%R.
Proof.
intros x y NZy.
now field.
Qed.
