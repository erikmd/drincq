(* Copyright (c) ENS de Lyon and Inria. All rights reserved. *)

Require Export ZArith MoreZArith.
Require Export Reals MoreReals.
Require Export Flocq.Core.Fcore.
Require Export Tactics.
Require Import Psatz.
Require Import Flocq.Calc.Fcalc_digits.
Require Import Flocq.Prop.Fprop_div_sqrt_error.
Require Import Flocq.Calc.Fcalc_ops.
Require Import Flocq.Calc.Fcalc_bracket.
Require Import Flocq.Calc.Fcalc_round.
Require Import Flocq.Prop.Fprop_relative.
Require Import Flocq.Prop.Fprop_plus_error.

Set Implicit Arguments.

Theorem Float_idem :
  forall beta (f : float beta), Float beta (Fnum f) (Fexp f) = f.
Proof.
now destruct f.
Qed.

Theorem generic_format_Rsign :
  forall beta fexp x y,
  generic_format beta fexp y -> generic_format beta fexp (Rsign x * y).
Proof.
clear; intros beta fexp x y Fy.
unfold Rsign; destruct (Rcompare_spec x R0) as [LT|EQ|GT].
now replace (-1 * y)%R with (-y)%R by ring; apply generic_format_opp.
now replace (0 * y)%R with R0 by ring; apply generic_format_0.
now replace (1 * y)%R with y by ring.
Qed.

Theorem Znearest_scale :
  forall choice f m n, (1 <= f)%R ->
  Z2R (Znearest choice (m * f)) = (Z2R n * f)%R ->
  Znearest choice m = n.
Proof.
intros choice f m n [Hf|Hf] H.
apply Znearest_imp.
generalize (Znearest_N choice (m * f)).
rewrite ->H, <- Rmult_minus_distr_r.
rewrite ->Rabs_mult, ->(Rabs_pos_eq f).
intros H'.
apply Rmult_lt_reg_r with f.
apply Rle_lt_trans with (2 := Hf).
now apply (Z2R_le 0 1).
apply Rle_lt_trans with (1 := H').
rewrite <- Rmult_1_r at 1.
apply Rmult_lt_compat_l with (2 := Hf).
apply Rinv_0_lt_compat.
now apply (Z2R_lt 0 2).
apply Rlt_le.
apply Rle_lt_trans with (2 := Hf).
now apply (Z2R_le 0 1).
rewrite <- Hf, 2!Rmult_1_r in H.
now apply eq_Z2R.
Qed.

(** For the FLT format *)

Theorem FLT_exp_le : forall emin prec e, (e - FLT_exp emin prec e <= prec)%Z.
Proof.
intros emin prec e.
unfold FLT_exp.
pose proof (Zle_max_l (e - prec) emin).
omega.
Qed.

(** For the FLX format *)

Theorem FLX_exp_ge : forall p e, (e - p <= FLX_exp p e)%Z.
Proof.
intros p e.
apply Zplus_le_compat_r.
apply Zle_refl.
Qed.

Theorem FLX_format_Rabs_Fnum :
  forall beta prec x (fx : float beta),
    x = F2R fx ->
    (Rabs (Z2R (Fnum fx)) < bpow beta prec)%R ->
    generic_format beta (FLX_exp prec) x.
Proof.
clear; intros beta prec x fx Hx Hm.
case (Zle_or_lt 0 prec) as [Hp|Hp].
apply generic_format_FLX.
exists fx.
refine (conj Hx _).
apply lt_Z2R.
now rewrite ->Z2R_abs, ->Z2R_Zpower.
rewrite Hx ; clear -Hm Hp.
destruct fx as (mx,ex).
simpl in Hm.
replace mx with Z0.
rewrite F2R_0.
apply generic_format_0.
assert (Zabs mx < 1)%Z.
apply lt_Z2R.
rewrite Z2R_abs.
apply Rlt_le_trans with (1 := Hm).
apply Rlt_le.
apply bpow_lt with (1 := Hp).
clear -H ; zify ; omega.
Qed.

Theorem FLX_format_Rabs_Fexp:
  forall (beta : radix) (prec : Z) (x : R) (fx : float beta),
  x = F2R fx ->
  (Rabs x < bpow beta (prec + Fexp fx))%R ->
  generic_format beta (FLX_exp prec) x.
Proof.
intros beta prec x (mx,ex) Hx Habs.
apply FLX_format_Rabs_Fnum with (1 := Hx).
apply Rmult_lt_reg_r with (bpow beta ex).
apply bpow_gt_0.
rewrite <- bpow_plus.
rewrite ->Hx, <- F2R_Zabs in Habs.
now rewrite <- Z2R_abs.
Qed.

Section PrelimFLX.
Local Open Scope Z_scope.
Variable beta : radix.
Variables p p' : Z.

Class Prec'_ge_0 := prec'_ge_0 : (0 <= p').
Context { prec'_ge_0_ : Prec'_ge_0 }.

Local Notation fexp := (FLX_exp p).
Local Notation fexp2 := (FLX_exp (p + p')).
Local Notation format := (generic_format beta fexp).
Local Notation format2 := (generic_format beta fexp2).

Lemma formats : forall x, format x -> format2 x.
Proof.
intros x.
apply generic_inclusion_ln_beta.
intros _.
red in (type of prec'_ge_0_).
unfold FLX_exp; auto with zarith.
Qed.

Notation pow e := (bpow beta e).
Notation cexp := (canonic_exp beta fexp).
Notation mant := (scaled_mantissa beta fexp).

Theorem FLX_mant_le :
  0 <= p -> forall x, format x -> Zabs (Ztrunc (mant x)) <= beta^p - 1.
Proof.
clear; intros H x x_.
apply Zlt_le_pred.
apply lt_Z2R.
rewrite Z2R_abs.
rewrite <- scaled_mantissa_generic with (1 := x_).
rewrite ->Z2R_Zpower with (1 := H).
pattern p at 2 ; replace p with (ln_beta beta x - canonic_exp beta fexp x)%Z.
apply abs_scaled_mantissa_lt_bpow.
unfold canonic_exp, FLX_exp.
ring.
Qed.

Theorem FLX_Rabs_le :
  0 <= p -> forall x : R,
    generic_format beta (FLX_exp p) x ->
    (Rabs x <= (pow p - 1) * pow (cexp x))%R.
Proof.
clear; intros H x x_.
rewrite ->x_ at 1.
unfold F2R at 1.
simpl.
rewrite Rabs_mult.
rewrite ->Rabs_pos_eq with (x := pow (cexp x)).
2: now apply bpow_ge_0.
apply Rmult_le_compat_r.
  now apply bpow_ge_0.
rewrite <- Z2R_Zpower; trivial.
change 1%R with (Z2R 1%Z).
rewrite <- Z2R_minus.
rewrite <- Z2R_abs.
apply Z2R_le.
now apply FLX_mant_le.
Qed.

Theorem format_plus_cexp :
  forall q x y, format x -> format y -> 0 <= p -> p <= q ->
    cexp y <= cexp x -> cexp x <= cexp y + q ->
    generic_format beta (FLX_exp (p + q)) (Rplus x y).
Proof.
clear; intros q A B A_ B_ Hp0 Hpq Hc Hcq.
(* Goal: apply format_add with (1 := A_) (2 := B_); simpl. *)
pose proof (abs_lt_bpow_prec beta fexp p (FLX_exp_ge p)) as H.
pose proof (H A) as HA.
pose proof (H B) as HB.
assert (HC : cexp A <= cexp B + q - 1 \/ cexp A = cexp B + q) by omega.
assert (GB : (Rabs (A + B) < pow (p + q + cexp B))%R).
  destruct HC as [Hok | Heq].
    apply Rle_lt_trans with (1 := Rabs_triang _ _).
    apply Rlt_le_trans with (1 := Rplus_lt_compat _ _ _ _ HA HB).
    apply Rle_trans with (r2 := (2 * (pow (p + cexp B + q - 1)))%R).
      rewrite double; apply Rplus_le_compat; apply bpow_le; omega.
    apply Rle_trans with (r2 := (Z2R beta * pow (p + cexp B + q - 1))%R).
      apply Rmult_le_compat_r.
        apply Rlt_le; apply bpow_gt_0.
      change 2%R with (Z2R 2).
      apply Z2R_le.
      apply Zle_bool_imp_le with (1 := radix_prop beta).
    rewrite <- bpow_1.
    rewrite <- bpow_plus.
    apply bpow_le; omega.
  rewrite ->A_, ->B_ at 1.
  rewrite ->F2R_change_exp with (e' := cexp B); trivial.
  rewrite <- F2R_plus, Fplus_same_exp.
  apply F2R_lt_bpow; simpl.
  pose proof (@FLX_mant_le Hp0 _ A_).
  pose proof (@FLX_mant_le Hp0 _ B_).
  rewrite Heq.
  replace (cexp B + q - cexp B) with (q) by omega.
  replace (p + q + cexp B - cexp B) with (p + q) by omega.
  apply Zle_lt_trans with (1 := Zabs_triangle _ _).
  rewrite Zabs_Zmult.
  set (M := Zabs (Ztrunc (mant A))) in *.
  set (N := Zabs (Ztrunc (mant B))) in *.
  assert (HD : 0 <= beta ^ p /\ 0 <= beta ^ q).
    now split; apply Zpower_ge_0.
  destruct HD as [Hp Hq].
  rewrite Zabs_eq; trivial.
  rewrite Zpower_exp.
  3: omega.
  2: now apply Zle_ge.
  (* Now it is evident, but not immediate *)
  clear - H0 H1 Hp Hq Hp0 Hpq.
  set (P := beta^p) in *; set (Q := beta^q) in *.
  apply Zle_lt_trans with (m := (P - 1) * Q + (P - 1)).
  apply Zplus_le_compat; trivial.
  apply Zmult_le_compat_r; trivial.
  assert (HE : P <= Q).
    unfold P, Q.
    now change 2 with (beta:Z); apply Zpower_le.
  ring_simplify; omega.
assert (GA : (Rabs (A + B) < pow (p + q + cexp A))%R).
  apply Rlt_le_trans with (1 := GB).
  rewrite bpow_plus; set (epq := pow (p + q)).
  rewrite bpow_plus; fold epq.
  apply Rmult_le_compat_l.
    now apply bpow_ge_0.
  now apply bpow_le.
eapply generic_format_plus_prec ; try eassumption.
apply FLX_exp_ge.
Qed.

Context { valid_exp : Valid_exp fexp }.

Context { p_gt_0 : Prec_gt_0 p }.

Variable choice : Z -> bool.

Theorem FLX_round_N_eq0 :
  forall x : R, round beta fexp (Znearest choice) x = R0 -> x = R0.
Proof.
intros x Hrnd0.
destruct (Req_dec x R0) as [Zx|Zx]; [trivial|exfalso].
destruct (ln_beta beta x) as [k Hk].
specialize (Hk Zx).
refine (_ (proj1 (round_bounded_large _ _ (Znearest choice) x k _ Hk))).
rewrite ->Hrnd0, ->Rabs_R0.
apply Rlt_not_le.
apply bpow_gt_0.
unfold FLX_exp.
unfold Prec_gt_0 in p_gt_0.
clear -p_gt_0 ; omega.
Qed.

End PrelimFLX.

(** For generic formats *)

Section Canonic_exp.

Variable beta : radix.
Variable fexp : Z -> Z.

Context {monotone_exp_ : Monotone_exp fexp}.

Theorem canonic_exp_monotone_abs :
  forall x y,
    (x <> R0) ->
    (Rabs x <= Rabs y)%R ->
    canonic_exp beta fexp x <= canonic_exp beta fexp y.
Proof.
intros x y Hx0 Hxy.
apply monotone_exp_.
now apply ln_beta_le_abs.
Qed.

End Canonic_exp.

Section PrelimGeneric.
Variable (beta : radix).
Variable (fexp : Z -> Z).
Notation format := (generic_format beta fexp).

Theorem bpow_ln_beta :
  forall (x : R) (e : Z), x = bpow beta e -> e = ln_beta beta x - 1.
Proof.
intros x e H; rewrite H.
now rewrite ln_beta_bpow; auto with zarith.
Qed.

Theorem POSpow_ln_beta :
  forall x : R, is_pow beta x -> (0 < x)%R ->
    x = (bpow beta (ln_beta beta x - 1)).
Proof.
intros x Bx Px.
destruct Bx as [e Bx].
rewrite Rabs_pos_eq in Bx; auto with real.
rewrite <- bpow_ln_beta with (1 := Bx).
exact Bx.
Qed.

Theorem notpow_ln_beta :
  forall x : R, ~ is_pow beta x ->
    Req_bool x (bpow beta (ln_beta beta x - 1)) = false.
Proof.
intros x NBx.
(* apply not_true_is_false. intro Keq. apply NBx. *)
apply Req_bool_false. intro Keq. apply NBx.
pose (e := ln_beta beta x - 1).
exists e.
rewrite ->Keq at 1.
pose proof bpow_gt_0 beta e.
now rewrite Rabs_pos_eq; try apply Rlt_le.
Qed.

Theorem ln_beta_div_beta :
  forall x : R, x <> R0 ->
    ( ln_beta beta (x / Z2R beta) = ln_beta beta x - 1 :> Z )%Z.
Proof.
intros x.
unfold Rdiv.
replace (/ Z2R beta)%R with (bpow beta (-1)).
intros NZx.
now rewrite ->ln_beta_mult_bpow with (1 := NZx).
unfold bpow.
repeat f_equal; unfold Zpower_pos; simpl.
now apply Zmult_1_r.
Qed.

Theorem POSpow_pred :
  forall (x : R), format x -> is_pow beta x -> (0 < x)%R ->
  ( pred beta fexp x = x - ulp beta fexp (x / Z2R beta) )%R.
Proof with auto'.
intros x Fx Bx Px.
unfold pred, ulp.
pose proof POSpow_ln_beta Bx Px as B'x.
assert (xn0 : x <> 0%R).
  now apply not_eq_sym;  apply Rlt_not_eq; exact Px.
rewrite Req_bool_false.
2: apply Rmult_integral_contrapositive_currified; trivial.
2: apply Rinv_neq_0_compat.
2: now apply not_eq_sym; apply Rlt_not_eq; apply radix_pos.
destruct Bx as [e Bx].
rewrite Rabs_pos_eq in Bx; auto with real.
pose proof bpow_ln_beta e Bx as He.
unfold canonic_exp.
rewrite ->ln_beta_div_beta; trivial.
unfold succ.
rewrite Rle_bool_false; [|now psatzl R].
rewrite !Ropp_involutive; unfold pred_pos.
now rewrite Req_bool_true, Bx, He.
Qed.

Theorem notpow_pred :
  forall (x : R), format x -> ~ is_pow beta x ->
  ( pred beta fexp x = x - ulp beta fexp x )%R.
Proof with auto'.
intros x Fx NBx.
case(Req_bool_spec x 0); intros H0.
  unfold pred.
  rewrite succ_eq_pos; rewrite H0, Ropp_0; [|now apply Rle_refl].
  now rewrite Ropp_plus_distr, Ropp_0.
unfold pred, succ.
case (Rle_bool_spec 0 (-x)); intros hx.
  rewrite Ropp_plus_distr, !Ropp_involutive.
  now rewrite ulp_opp.
now unfold pred_pos; rewrite !Ropp_involutive, (notpow_ln_beta NBx).
Qed.

Notation DN x := (round beta fexp Zfloor x).
Notation UP x := (round beta fexp Zceil x).

Theorem ulp_pos : forall x, (x <> 0%R) ->(0 < ulp beta fexp x)%R.
Proof.
now intros x xneq0; rewrite ulp_neq_0; trivial; apply bpow_gt_0.
Qed.

Theorem round_DN_UP_le :
  forall x, ( round beta fexp Zfloor x <= x <= round beta fexp Zceil x )%R.
Proof.
intros x; split.
(* Zfloor *)
unfold round, F2R; simpl.
apply Rle_trans with
  (r2 := (scaled_mantissa beta fexp x * bpow beta (canonic_exp beta fexp x))%R).
apply Rmult_le_compat_r.
now apply Rlt_le; apply bpow_gt_0.
now apply Zfloor_lb.
now rewrite scaled_mantissa_mult_bpow; apply Rle_refl.
(* Zceil *)
unfold round, F2R; simpl.
apply Rle_trans with
  (r2 := (scaled_mantissa beta fexp x * bpow beta (canonic_exp beta fexp x))%R).
now rewrite scaled_mantissa_mult_bpow; apply Rle_refl.
apply Rmult_le_compat_r.
now apply Rlt_le; apply bpow_gt_0.
now apply Zceil_ub.
Qed.

Theorem round_DN_UP_generic :
  forall x,
    generic_format beta fexp x ->
    round beta fexp Zfloor x = round beta fexp Zceil x.
Proof with auto'.
intros x H.
unfold round.
now rewrite ->scaled_mantissa_generic, ->Zfloor_Z2R, ->Zceil_Z2R.
Qed.

(** Successor and Predecessor functions over the full real line *)

Definition Succ x :=
match Rcompare x R0 with
  | Lt => Ropp (pred beta fexp (Ropp x))
  | Eq => R0
  | Gt => Rplus x (ulp beta fexp x)
end.

Definition Pred x :=
match Rcompare x R0 with
  | Lt => Rminus x (ulp beta fexp (Ropp x))
  | Eq => R0
  | Gt => pred beta fexp x
end.

Theorem Pred_lt_Succ :
  forall x : R, x <> R0 -> ( Pred x < x < Succ x )%R.
Proof.
intros x Zx.
unfold Pred, Succ; destruct (Rcompare_spec x R0) as [LT|EQ|GT].
  2: easy.
  pose proof (ulp_pos (Ropp_neq_0_compat _ Zx)) as Hu.
  pose proof pred_lt_id beta fexp (Ropp x).
  now psatzl R.
pose proof ulp_pos Zx as Hu.
pose proof pred_lt_id beta fexp x.
now psatzl R.
Qed.

Theorem Pred_le_Succ :
  forall x : R, ( Pred x <= x <= Succ x )%R.
Proof.
intros x.
destruct (Req_dec x R0) as [Zx|Zx].
now rewrite Zx; unfold Pred, Succ; rewrite Rcompare_Eq; psatzl R.
pose proof Pred_lt_Succ Zx.
now psatzl R.
Qed.

Theorem Pred_opp : forall x : R, Pred (Ropp x) = Ropp (Succ x).
Proof.
intros x.
unfold Pred, Succ.
destruct (Rcompare_spec x R0) as [LT|EQ|GT];
  destruct (Rcompare_spec (Ropp x) R0) as [LT'|EQ'|GT']; try psatzl R.
now rewrite !ulp_opp; psatzl R.
Qed.

Theorem Succ_opp : forall x : R, Succ (Ropp x) = Ropp (Pred x).
Proof.
intros x.
rewrite <- (Ropp_involutive x) at 2.
rewrite Pred_opp.
now auto with real.
Qed.

Context { valid_exp : Valid_exp fexp }.

Theorem Succ_le_lt :
  forall (x y : R), generic_format beta fexp x -> generic_format beta fexp y ->
  (x < y)%R -> (Succ x <= y)%R.
Proof with auto'.
intros x y Fx Fy Hlt.
unfold Pred, Succ.
destruct (Rcompare_spec x R0) as [LT|EQ|GT];
  destruct (Rcompare_spec (Ropp x) R0) as [LT'|EQ'|GT']; try psatzl R.
assert (- y <= pred beta fexp (- x))%R as HA.
now' apply le_pred_lt; try apply generic_format_opp.
now psatzl R.
generalize (@succ_le_lt beta fexp valid_exp x y Fx Fy Hlt).
now rewrite succ_eq_pos; [|psatzl R].
Qed.

Theorem le_Pred_lt :
  forall (x y : R), generic_format beta fexp x -> generic_format beta fexp y ->
  (x < y)%R -> (x <= Pred y)%R.
Proof with auto'.
intros x y Fx Fy Hlt.
unfold Pred, Succ.
destruct (Rcompare_spec y R0) as [LT|EQ|GT];
  destruct (Rcompare_spec (Ropp y) R0) as [LT'|EQ'|GT']; try psatzl R.
assert (- y + ulp beta fexp (- y) <= -x)%R as HA.
pose proof (@succ_le_lt beta fexp valid_exp (Ropp y) (Ropp x)) as H0.
rewrite succ_eq_pos in H0; [|psatzl R].
now apply H0; try apply generic_format_opp; trivial; psatzl R.
now psatzl R.
now apply le_pred_lt.
Qed.

Theorem generic_format_Succ :
  forall x : R,
    generic_format beta fexp x ->
    generic_format beta fexp (Succ x).
Proof with auto'.
intros x Fx.
unfold Succ; destruct (Rcompare_spec x R0) as [LT|EQ|GT].
apply generic_format_opp; apply generic_format_pred; try psatzl R...
now apply generic_format_opp.
now apply generic_format_0.
generalize (@generic_format_succ beta fexp valid_exp x Fx).
now unfold succ; rewrite Rle_bool_true; [|now psatzl R].
Qed.

Theorem generic_format_Pred :
  forall x : R,
    generic_format beta fexp x ->
    generic_format beta fexp (Pred x).
Proof with auto'.
intros x Fx.
unfold Pred; destruct (Rcompare_spec x R0) as [LT|EQ|GT].
+ rewrite <- (Ropp_involutive x) at 1.
  replace (- - x - _)%R with (- (- x + ulp beta fexp (- x)))%R.
  2: ring.
  apply generic_format_opp.
  pose proof (@generic_format_succ beta fexp valid_exp (Ropp x)) as Hx.
  unfold succ in Hx; rewrite Rle_bool_true in Hx; [|psatzl R].
  now apply Hx; apply generic_format_opp.
+ now apply generic_format_0.
+ now apply generic_format_pred.
Qed.

Theorem generic_format_iff_DN_UP_eq :
  forall x,
    generic_format beta fexp x
    <->
    round beta fexp Zfloor x = round beta fexp Zceil x.
Proof with auto'.
intros x; split; intros H.
now apply round_DN_UP_generic.
assert (x = round beta fexp Zfloor x) as HA.
pose proof round_DN_UP_le x as HB.
apply Rle_antisym.
rewrite H; exact (proj2 HB).
exact (proj1 HB).
now' rewrite HA; apply generic_format_round.
Qed.

Theorem round_DN_UP_lt :
  forall x, ~ generic_format beta fexp x ->
  ( round beta fexp Zfloor x < x < round beta fexp Zceil x )%R.
Proof.
intros x Fx.
pose proof round_DN_UP_le x as Hx.
split.
  destruct Hx as [Hx _].
  apply Rnot_le_lt; intro Hle.
  assert (x = round beta fexp Zfloor x) by now apply Rle_antisym.
  rewrite H in Fx.
  contradict Fx.
  now' apply generic_format_round.
destruct Hx as [_ Hx].
apply Rnot_le_lt; intro Hle.
assert (x = round beta fexp Zceil x) by now apply Rle_antisym.
rewrite H in Fx.
contradict Fx.
now' apply generic_format_round.
Qed.

Variable rnd : R -> Z.
Context { valid_rnd : Valid_rnd rnd }.

Theorem round_DN_UP_round :
  forall x,
  ( round beta fexp Zfloor x <= round beta fexp rnd x
  <= round beta fexp Zceil x )%R.
Proof with auto'.
intros x.
pose proof round_DN_UP_le x as Hx.
assert (round beta fexp Zfloor x <= round beta fexp Zceil x)%R as H'x.
now apply Rle_trans with (1 := proj1 Hx) (2 := proj2 Hx).
pose proof @round_DN_or_UP beta fexp rnd valid_rnd x as Hrnd.
now destruct Hrnd as [Hrnd|Hrnd]; rewrite Hrnd; split; auto with real.
Qed.

(** The set of floating-point numbers is discrete *)

Theorem float_discr_DN_UP :
( forall x y, DN x < y -> y < UP x -> ~ generic_format beta fexp y )%R.
Proof.
intros x y _y y_.
intro Fy.
pose proof round_DN_pt beta fexp x as (D1,(D2,D3)).
pose proof round_UP_pt beta fexp x as (U1,(U2,U3)).
specialize (D3 _ Fy).
specialize (U3 _ Fy).
destruct (Rtotal_order x y) as [LT|[EQ|GT]]; psatzl R.
Qed.

End PrelimGeneric.

Section More_ulp.

(** Lemmas that were removed from Fcore_ulp.v *)

Variable (beta : radix).
Variable (fexp : Z -> Z).
Notation F := (generic_format beta fexp).
Notation u := (ulp beta fexp).
Context { valid_exp : Valid_exp fexp }.

Theorem pred_ge_bpow :
  forall x e,  F x ->
  x <> u x ->
  (bpow beta e < x)%R ->
  (bpow beta e <= x - u x)%R.
Proof.
intros x e Fx Hx' Hx.
(* *)
assert (1 <= Ztrunc (scaled_mantissa beta fexp x))%Z.
assert (0 <  Ztrunc (scaled_mantissa beta fexp x))%Z.
apply F2R_gt_0_reg with beta (canonic_exp beta fexp x).
rewrite <- Fx.
apply Rle_lt_trans with (2:=Hx).
apply bpow_ge_0.
omega.
case (Zle_lt_or_eq _ _ H); intros Hm.
(* *)
pattern x at 1 ; rewrite Fx.
rewrite ulp_neq_0;
  [|now apply Rgt_not_eq; apply Rlt_trans with (1 := bpow_gt_0 _ _) (2 := Hx)].
unfold F2R. simpl.
pattern (bpow beta (canonic_exp beta fexp x)) at 2 ; rewrite <- Rmult_1_l.
rewrite <- Rmult_minus_distr_r.
change 1%R with (Z2R 1).
rewrite <- Z2R_minus.
change (bpow beta e <= F2R (Float beta (Ztrunc (scaled_mantissa beta fexp x) - 1) (canonic_exp beta fexp x)))%R.
apply bpow_le_F2R_m1; trivial.
now rewrite <- Fx.
(* *)
contradict Hx'.
pattern x at 1; rewrite Fx.
rewrite  <- Hm.
rewrite ulp_neq_0;
  [|now apply Rgt_not_eq; apply Rlt_trans with (1 := bpow_gt_0 _ _) (2 := Hx)].
unfold F2R; simpl.
now rewrite Rmult_1_l.
Qed.

Lemma generic_format_pred_1:
  forall x, (0 < x)%R -> F x ->
  x <> bpow beta (ln_beta beta x - 1) ->
  F (x - u x).
Proof.
intros x Zx Fx Hx.
destruct (ln_beta beta x) as (ex, Ex).
simpl in Hx.
specialize (Ex (Rgt_not_eq _ _ Zx)).
assert (Ex' : (bpow beta (ex - 1) < x < bpow beta ex)%R).
rewrite Rabs_pos_eq in Ex.
destruct Ex as (H,H'); destruct H; split; trivial.
contradict Hx; easy.
now apply Rlt_le.
unfold generic_format, scaled_mantissa, canonic_exp.
rewrite ln_beta_unique with beta (x - u x)%R ex.
pattern x at 1 3 ; rewrite Fx.
rewrite ulp_neq_0; [|now apply Rgt_not_eq].
unfold scaled_mantissa.
rewrite canonic_exp_fexp with (1 := Ex).
unfold F2R. simpl.
rewrite Rmult_minus_distr_r.
rewrite Rmult_assoc.
rewrite <- bpow_plus, Zplus_opp_r, Rmult_1_r.
change (bpow beta 0) with (Z2R 1).
rewrite <- Z2R_minus.
rewrite Ztrunc_Z2R.
rewrite Z2R_minus.
rewrite Rmult_minus_distr_r.
now rewrite Rmult_1_l.
rewrite Rabs_pos_eq.
split.
apply pred_ge_bpow; trivial.
rewrite ulp_neq_0; [|now apply Rgt_not_eq].
intro H.
assert (ex-1 < canonic_exp beta fexp x  < ex)%Z.
split ; apply (lt_bpow beta) ; rewrite <- H ; easy.
clear -H0. omega.
apply Ex'.
apply Rle_lt_trans with (2 := proj2 Ex').
pattern x at 3 ; rewrite <- Rplus_0_r.
apply Rplus_le_compat_l.
rewrite <-Ropp_0.
apply Ropp_le_contravar.
rewrite ulp_neq_0; [|now apply Rgt_not_eq].
apply bpow_ge_0.
apply Rle_0_minus.
pattern x at 2; rewrite Fx.
rewrite ulp_neq_0; [|now apply Rgt_not_eq].
unfold F2R; simpl.
pattern (bpow beta (canonic_exp beta fexp x)) at 1; rewrite <- Rmult_1_l.
apply Rmult_le_compat_r.
apply bpow_ge_0.
replace 1%R with (Z2R 1) by reflexivity.
apply Z2R_le.
assert (0 <  Ztrunc (scaled_mantissa beta fexp x))%Z.
apply F2R_gt_0_reg with beta (canonic_exp beta fexp x).
rewrite <- Fx.
apply Rle_lt_trans with (2:=proj1 Ex').
apply bpow_ge_0.
omega.
Qed.

Theorem generic_format_plus_ulp :
  forall { monotone_exp : Monotone_exp fexp },
  forall x, (x <> 0)%R ->
  F x -> F (x + u x).
Proof with auto with typeclass_instances.
intros monotone_exp x Zx Fx.
destruct (Rtotal_order x 0) as [Hx|[Hx|Hx]].
rewrite <- Ropp_involutive.
apply generic_format_opp.
rewrite Ropp_plus_distr, <- ulp_opp.
apply generic_format_opp in Fx.
destruct (Req_dec (-x) (bpow beta (ln_beta beta (-x) - 1))) as [Hx'|Hx'].
rewrite Hx' in Fx |- *.
apply generic_format_bpow_inv' in Fx...
rewrite ulp_neq_0; [|now apply Rgt_not_eq; apply bpow_gt_0].
unfold canonic_exp.
rewrite ln_beta_bpow.
revert Fx.
generalize (ln_beta_val _ _ (ln_beta beta (-x)) - 1)%Z.
clear -monotone_exp valid_exp.
intros e He.
destruct (Zle_lt_or_eq _ _ He) as [He1|He1].
assert (He2 : e = (e - fexp (e + 1) + fexp (e + 1))%Z) by ring.
rewrite He2 at 1.
rewrite bpow_plus.
assert (Hb := Z2R_Zpower beta _ (Zle_minus_le_0 _ _ He)).
match goal with |- F (?a * ?b + - ?b) =>
  replace (a * b + -b)%R with ((a - 1) * b)%R by ring end.
rewrite <- Hb.
rewrite <- (Z2R_minus _ 1).
change (F (F2R (Float beta (Zpower beta (e - fexp (e + 1)) - 1) (fexp (e + 1))))).
apply generic_format_F2R.
intros Zb.
unfold canonic_exp.
rewrite ln_beta_F2R with (1 := Zb).
rewrite (ln_beta_unique beta _ (e - fexp (e + 1))).
apply monotone_exp.
rewrite <- He2.
apply Zle_succ.
rewrite Rabs_pos_eq.
rewrite Z2R_minus, Hb.
split.
apply Rplus_le_reg_r with (- bpow beta (e - fexp (e + 1) - 1) + Z2R 1)%R.
apply Rmult_le_reg_r with (bpow beta (-(e - fexp (e+1) - 1))).
apply bpow_gt_0.
ring_simplify.
apply Rle_trans with R1.
rewrite Rmult_1_l.
apply (bpow_le _ _ 0).
clear -He1 ; omega.
rewrite Ropp_mult_distr_l_reverse.
rewrite <- 2!bpow_plus.
ring_simplify (e - fexp (e + 1) - 1 + - (e - fexp (e + 1) - 1))%Z.
ring_simplify (- (e - fexp (e + 1) - 1) + (e - fexp (e + 1)))%Z.
rewrite bpow_1.
rewrite <- (Z2R_plus (-1) _).
apply (Z2R_le 1).
generalize (Zle_bool_imp_le _ _ (radix_prop beta)).
clear ; omega.
rewrite <- (Rplus_0_r (bpow beta (e - fexp (e + 1)))) at 2.
apply Rplus_lt_compat_l.
now apply (Z2R_lt (-1) 0).
rewrite Z2R_minus.
apply Rle_0_minus.
rewrite Hb.
apply (bpow_le _ 0).
now apply Zle_minus_le_0.
rewrite He1, Rplus_opp_r.
apply generic_format_0.
apply generic_format_pred_1 ; try easy.
rewrite <- Ropp_0.
now apply Ropp_lt_contravar.
now elim Zx.
rewrite <-succ_eq_pos; auto with real.
now apply generic_format_succ.
Qed.

Theorem generic_format_minus_ulp :
  forall { monotone_exp : Monotone_exp fexp },
  forall x, (x <> 0)%R ->
  F x -> F (x - u x).
Proof.
intros monotone_exp x Zx Fx.
replace (x - u x)%R with (-(-x + u x))%R by ring.
apply generic_format_opp.
rewrite <- ulp_opp.
apply generic_format_plus_ulp.
contradict Zx.
rewrite <- (Ropp_involutive x), Zx.
apply Ropp_0.
now apply generic_format_opp.
Qed.

End More_ulp.
