(* Copyright (c) ENS de Lyon and Inria. All rights reserved. *)

(* File created by Erik Martin-Dorel on 2011-10-27 *)

(** Some Ltac abbreviations *)

Ltac auto' := auto with typeclass_instances.

Ltac easy' := easy || now auto'.

(** A closing tactical which takes typeclass_instances into account *)

Tactic Notation "now'" tactic(t) := t; easy'.

(** In a [Proof with auto with typeclass_instances.],
if [tac...] solves the current subgoal but [tac.] doesn't,
[now tac...] will fail, while it is now possible to invoke
[now' tac.] *)

(** ---- *)

(** Define a tactic [wolog] similar to [cut] & SSReflect's [wlog]. *)

Definition Tag (A : Type) := True.
Definition Get (A : Type) := A.
Definition Wolog :=
  fun (fact goal : Type) (impl : fact -> goal) (aux : fact) => impl aux.
Implicit Arguments Wolog [goal].

(* LEGACY CODE
(* For 2 hyps *)
Tactic Notation "wolog" hyp(h1) hyp(h2) "/" constr(T) :=
(match goal with |- ?G =>
  let close := match eval pattern h1,h2 in (T -> G) with | ?F _ _ => F end
in let quant := match close with
  | (fun (a : ?A)(b : ?B) => ?t) => constr:(forall (a:A)(b:B), t)
end in refine (@Wolog quant G _ _);
[|clear h1 h2; intros h1 h2] end)
  || (revert h1 h2; fail "Unexpected error in 2-hyps wolog").
*)

Tactic Notation "wolog" ne_hyp_list(L) "/" constr(T) :=
match goal with |- ?G =>
  refine (_ (_: Tag T)); [|exact I];
  revert L; match goal with |- ?G' =>
    intros L; match G' with appcontext FTag [Tag] =>
      let fact := context FTag [Get] in
      intros _;
      refine (Wolog fact _ _);
      cbv delta[Get] beta;
      [|clear L; intros L]
    end
  end
end || (revert L; fail "Unexpected error in wolog (main form)").

(* REM: [intros L] does not fall back to [idtac] when hyp_list(L) is empty ! *)

(** Defective form of wolog (a suff-like tactic). *)
Tactic Notation "wolog" "/" constr(T) :=
match goal with |- ?G =>
  refine (_ (_: Tag T)); [|exact I];
  match goal with |- ?G' =>
    match G' with appcontext FTag [Tag] =>
      let fact := context FTag [Get] in
      intros _;
      refine (Wolog fact _ _);
      cbv delta[Get] beta
    end
  end
end || (fail "Unexpected error in wolog (defective form)").

(** ---- *)

Require Import Reals.

(** Define a tactic [simp2] to drop halves so that [psatzl R] can be invoked. *)

Lemma Rdiv1E : forall x : R, ( x / 1 = x )%R.
simpl; intros x.
now unfold Rdiv; rewrite Rinv_1, Rmult_1_r.
Qed.

Ltac simp2_hyp H :=
let H' := fresh H in
match type of H with
| ?x = ?y => pose proof @Rmult_eq_compat_r 2 _ _ H as H'
| Rlt ?x ?y => pose proof @Rmult_lt_compat_r 2 _ _ Rlt_R0_R2 H as H'
| Rle ?x ?y =>
  pose proof @Rmult_le_compat_r 2 _ _ (@Rlt_le _ _ Rlt_R0_R2) H as H'
| Rgt ?x ?y =>
  pose proof @Rmult_gt_compat_r 2 _ _ (@Rlt_gt _ _ Rlt_R0_R2) H as H'
| Rge ?x ?y =>
  pose proof @Rmult_ge_compat_r 2 _ _ (@Rgt_ge _ _ (@Rlt_gt _ _ Rlt_R0_R2)) H as H'
end; clear H; field_simplify in H'; rewrite ?Rdiv1E in H'; rename H' into H.

Ltac simp2_goal :=
match goal with
| |- ?x = ?y => (apply (@Rmult_eq_reg_r 2); discrR)
| |- Rlt ?x ?y => apply (@Rmult_lt_reg_r 2 _ _ Rlt_R0_R2)
| |- Rle ?x ?y => apply (@Rmult_le_reg_r 2 _ _ Rlt_R0_R2)
| |- Rgt ?x ?y => apply (@Rmult_lt_reg_r 2 _ _ Rlt_R0_R2)
| |- Rge ?x ?y => (apply Rle_ge; apply (@Rmult_le_reg_r 2 _ _ Rlt_R0_R2))
end; field_simplify; rewrite ?Rdiv1E.

Ltac simp2 :=
repeat match goal with
| [H : context[Rinv 2] |- _] => simp2_hyp H
| [H : context[Rdiv _ 2] |- _] => simp2_hyp H
| |- context[Rinv 2] => simp2_goal
| |- context[Rdiv _ 2] => simp2_goal
end.

(** Defined a generalized tactic of [simp2] called [simpdiv]. *)

Ltac simpdiv_hyp r Pr H :=
let H' := fresh H in
match type of H with
| ?x = ?y => pose proof @Rmult_eq_compat_r r _ _ H as H'
| Rlt ?x ?y => pose proof @Rmult_lt_compat_r r _ _ Pr H as H'
| Rle ?x ?y =>
  pose proof @Rmult_le_compat_r r _ _ (@Rlt_le _ _ Pr) H as H'
| Rgt ?x ?y =>
  pose proof @Rmult_gt_compat_r r _ _ (@Rlt_gt _ _ Pr) H as H'
| Rge ?x ?y =>
  pose proof @Rmult_ge_compat_r r _ _ (@Rgt_ge _ _ (@Rlt_gt _ _ Pr)) H as H'
end; clear H; field_simplify in H'; rewrite ?Rdiv1E in H'; rename H' into H.

Ltac simpdiv_goal r Pr :=
match goal with
| |- ?x = ?y => refine (@Rmult_eq_reg_r r _ _ _ (@Rgt_not_eq _ _ Pr))
| |- Rlt ?x ?y => apply (@Rmult_lt_reg_r r _ _ Pr)
| |- Rle ?x ?y => apply (@Rmult_le_reg_r r _ _ Pr)
| |- Rgt ?x ?y => apply (@Rmult_lt_reg_r r _ _ Pr)
| |- Rge ?x ?y => (apply Rle_ge; apply (@Rmult_le_reg_r r _ _ Pr))
end; field_simplify; rewrite ?Rdiv1E.

Ltac simpdiv r Pr :=
repeat match goal with
| [H : context[Rinv r] |- _] => simpdiv_hyp r Pr H
| [H : context[Rdiv _ r] |- _] => simpdiv_hyp r Pr H
| |- context[Rinv r] => simpdiv_goal r Pr
| |- context[Rdiv _ r] => simpdiv_goal r Pr
end.

Ltac simpdiv_cut r :=
  let Pr := fresh "Pr" in
  cut (0 < r)%R; [intro Pr; simpdiv r Pr|idtac].
