;;; May be loaded after ProofGeneral and before opening a Coq file

(progn
;; (add-to-list 'coq-user-solve-tactics-db '("easy" nil "easy" t "easy"))
;; (add-to-list 'coq-user-solve-tactics-db '("now" "now" "now #" t "now"))
(add-to-list 'coq-user-tactics-db '("auto'" nil "auto'" t "auto'"))
(add-to-list 'coq-user-solve-tactics-db '("easy'" nil "easy'" t "easy'"))
(add-to-list 'coq-user-solve-tactics-db '("now'" nil "now' #" t "now'"))
(add-to-list 'coq-user-solve-tactics-db '("psatzl" nil "psatzl #" t "psatzl"))
)
