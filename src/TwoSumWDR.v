Require Import MoreFlocq.
Require Import ROmega.
Require Import Psatz.
Require Import Flocq.Calc.Fcalc_digits.
Require Import Flocq.Prop.Fprop_div_sqrt_error.
Require Import Flocq.Calc.Fcalc_ops.
Require Import Flocq.Calc.Fcalc_bracket.
Require Import Flocq.Calc.Fcalc_round.
Require Import Flocq.Prop.Fprop_relative.
Require Import Flocq.Prop.Fprop_plus_error.
Require Import Flocq.Prop.Fprop_Sterbenz.
Require Import Remarks.

Local Notation two := radix2 (only parsing).
Local Notation pow e := (bpow two e).
Local Open Scope Z_scope.

Section Property_2_1.

Variables p p' : Z.
Local Notation fexp := (FLX_exp p).
Local Notation fexp2 := (FLX_exp (p + p')).

Local Notation format := (generic_format radix2 fexp).
Local Notation format2 := (generic_format radix2 fexp2).
Local Notation cexp := (canonic_exp two fexp).
Local Notation mant := (scaled_mantissa two fexp).
Local Notation cexp2 := (canonic_exp two fexp2).

Notation DN x := (round two fexp Zfloor x).
Notation UP x := (round two fexp Zceil x).

Variable choice : Z -> bool.
Local Notation rnd_p := (round two (FLX_exp p) (Znearest choice)).
Local Notation rnd_p2 := (round two (FLX_exp (p + p')) (Znearest choice)).

(*Hypothesis ZNE : choice = fun n => negb (Zeven n).*)
Hypothesis p_gt_0 : Prec_gt_0 p.
Hypothesis p'_gt_0 : Prec_gt_0 p'.
Let pp'_gt_0 : Prec_gt_0 (p + p').
Proof. now red in p_gt_0, p'_gt_0 |- *; auto with zarith. Qed.
Existing Instance pp'_gt_0.

Definition u' := (pow (- p) + pow (- p - p'))%R.

(** Here we focus on radix2, FLX, ZnearestE *)

Lemma DR_relative_error_N_FLX :
  forall x : R, (Rabs (rnd_p (rnd_p2 x) - x) <= u' * Rabs x)%R.
Proof.
intros x.
set (sigma := rnd_p (rnd_p2 x)).
destruct (Req_dec sigma (rnd_p x)) as [NoSlip|Slip].
(* *)
rewrite NoSlip.
pose proof relative_error_N_FLX two p p_gt_0 choice x as Hx.
apply Rle_trans with (1 := Hx).
rewrite bpow_plus.
clear; unfold u'; simpl; field_simplify; rewrite !Rdiv1E.
pose proof Rabs_pos x as Habs.
pose proof bpow_gt_0 two (- p - p') as Hpp'.
assert (0 <= Rabs x * pow (- p - p'))%R as H0.
  replace R0 with (Rabs x * 0)%R by psatzl R.
  apply Rmult_le_compat_l; psatzl R.
psatzl R.
(* *)
pose proof error_le_half_ulp_round two fexp2 choice x as h; rewrite Rabs_minus_sym in h.
assert (Rabs (x - rnd_p2 x) <= pow (- p' - 1) * ulp two fexp x)%R as H5.
2: clear h.
  admit.
pose proof error_le_half_ulp two fexp choice (rnd_p2 x) as H6.
rewrite Rabs_minus_sym in H6; fold sigma in H6.
pose proof Rem5 Slip as Hmidp.

assert (ulp two fexp (rnd_p2 x) = ulp two fexp x) as Hulp.
  admit.
assert (Rabs (x - sigma) <= (/2 + pow (-p' - 1)) * ulp radix2 fexp x)%R as H7.
  admit.
rewrite Hulp in H6.
admit.
Admitted.

Lemma DR_relative_error_N_FLX_ex :
  forall x : R,
  exists eps : R, (Rabs eps <= u' /\ rnd_p (rnd_p2 x) = x * (1 + eps))%R.
Proof with auto'.
(* Adapted from [relative_error_le_conversion]. *)
intros x.
destruct (Req_dec x 0) as [Hx0|Hx0].
(* *)
exists R0.
split.
rewrite Rabs_R0. unfold u'.
now pose proof bpow_gt_0 two(-p); pose proof bpow_gt_0 two(-p-p'); psatzl R.
rewrite Hx0, Rmult_0_l.
now' repeat rewrite round_0.
(* *)
exists ((rnd_p (rnd_p2 x) - x) / x)%R.
split. 2: now field.
unfold Rdiv.
rewrite Rabs_mult.
apply Rmult_le_reg_r with (Rabs x).
now apply Rabs_pos_lt.
rewrite Rmult_assoc, <- Rabs_mult.
rewrite Rinv_l with (1 := Hx0).
rewrite Rabs_R1, Rmult_1_r.
now apply DR_relative_error_N_FLX.
Qed.

Lemma DR_relative_error_N_FLX_round :
  forall x : R, (Rabs (rnd_p (rnd_p2 x) - x) <= u' * Rabs (rnd_p (rnd_p2 x)))%R.
Proof.
admit.
Admitted.

Lemma DR_relative_error_N_FLX_round_ex :
  forall x : R,
  exists eps : R, (Rabs eps <= u' /\ rnd_p (rnd_p2 x) = x / (1 + eps))%R.
Proof with auto'.
intros x.
destruct (Req_dec x 0) as [Hx0|Hx0].
(* *)
exists R0.
split.
rewrite Rabs_R0. unfold u'.
now pose proof bpow_gt_0 two(-p); pose proof bpow_gt_0 two(-p-p'); psatzl R.
rewrite Hx0; unfold Rdiv; rewrite Rmult_0_l.
now' repeat rewrite round_0.
(* *)
set (y := rnd_p (rnd_p2 x)).
assert (rnd_p (rnd_p2 x) <> 0)%R as Hrnd0.
  intro Kx; apply Hx0.
  apply FLX_round_N_eq0 with two (p + p') choice...
  now' apply FLX_round_N_eq0 with two p choice.
exists ((x - y) / y)%R.
split; [|field; unfold y; psatzl R].
pose proof DR_relative_error_N_FLX_round x as Hmain; fold y in Hmain.
unfold Rdiv.
rewrite Rabs_mult, Rabs_minus_sym, Rabs_Rinv; [|easy].
apply Rmult_le_reg_r with (Rabs y)%R.
  now apply Rabs_pos_lt. (* Check Rinv_0_lt_compat. *)
field_simplify; [rewrite !Rdiv1E; psatzl R|].
now apply Rabs_no_R0. (* Check (Rabs_no_R0, Rabs_pos_lt). *)
Qed.

End Property_2_1.

Section ProofFast2SumWDR.
Variables p p' emin : Z.
Hypothesis Hp : Zlt 2 p.
Hypothesis Hp' : Zlt 1 p'.

Local Instance p_gt_0 : Prec_gt_0 p.
now apply Zlt_trans with (2 := Hp).
Qed.

Local Instance p_p'_gt_0 : Prec_gt_0 (p + p').
apply Zlt_trans with (1 := refl_equal Lt : (0 < 2 + 1)%Z).
now apply Zplus_lt_compat.
Qed.

Local Instance p'_ge_0 : Prec'_ge_0 p'.
now apply Zlt_le_weak; apply Zlt_trans with (2 := Hp').
Qed.

Local Notation fexp := (FLX_exp p).
Local Notation fexp2 := (FLX_exp (p + p')).
Local Notation format := (generic_format two fexp).
Local Notation cexp := (canonic_exp two fexp).
Local Notation mant := (scaled_mantissa two fexp).
Local Notation format2 := (generic_format two fexp2).
Local Notation cexp2 := (canonic_exp two fexp2).

Lemma FLX_exp2_le : forall e, (e - fexp2 e <= p + p')%Z.
Proof.
unfold FLX_exp.
intros; omega.
Qed.

Local Notation rnd_p := (round two fexp ZnearestE).
Local Notation rnd_p2 := (round two fexp2 ZnearestE).

Section FastTwoSumInit.

Variables a b s : R.
Hypothesis Fa : format a.
Hypothesis Fb : format b.
Hypothesis sDef : s = rnd_p (a + b).
Hypothesis Hab : (cexp b <= cexp a)%Z.

Theorem FastTwoSumInit :
  format (s - a) /\ format (a + b - s).
Proof with auto'.
split.
apply sterbenz...
rewrite sDef.
apply generic_format_round...
refine (_ Hab).
admit.
replace (a + b - s)%R with (-(s - (a + b)))%R by ring.
rewrite sDef.
apply generic_format_opp.
apply plus_error...
Admitted.

End FastTwoSumInit.

Lemma generic_imp_any :
  forall x y,
  y = rnd_p2 x \/ y = rnd_p (rnd_p2 x) \/ y = rnd_p x ->
  generic_format two fexp x ->
  y = x.
Proof with auto'.
intros x y yDef Fx.
destruct yDef as [H|[H|H]] ; rewrite H.
apply round_generic...
apply formats...
rewrite round_generic with (x := x)...
apply round_generic...
apply formats...
apply round_generic...
Qed.

Section FastTwoSum.

Variables a b s z t : R.
Hypothesis Fa : format a.
Hypothesis Fb : format b.
Hypothesis sDef : s = rnd_p (rnd_p2 (a + b)) \/ s = rnd_p (a + b).
Hypothesis zDef : z = rnd_p2 (s - a) \/ z = rnd_p (rnd_p2 (s - a)) \/ z = rnd_p (s - a).
Hypothesis tDef : t = rnd_p (rnd_p2 (b - z)) \/ t = rnd_p (b - z).
Hypothesis Hab : (cexp b <= cexp a)%Z.

Theorem FastTwoSum :
  (z = s - a)%R /\
  ((s = rnd_p (a + b) -> (t = a + b - s)%R) /\
   (s <> rnd_p (a + b) -> t = rnd_p (a + b - s))).
Proof with auto'.
destruct (Req_dec s (rnd_p (a + b))) as [Heq|Hslip].
(* *)
destruct (FastTwoSumInit a b s Fa Fb Heq Hab) as (Fz,Ft).
cut (z = s - a)%R.
intros zDef'.
repeat split ; try easy.
intros _.
assert (Hz: (a + b - s = b - z)%R).
rewrite zDef'.
ring.
destruct tDef as [H|H] ; rewrite H, <- Hz.
rewrite round_generic with (x := (a + b - s)%R)...
apply round_generic...
apply formats...
apply round_generic...
now apply generic_imp_any.
(* *)
destruct sDef as [sDef'|Hs] ; try easy.
cut (z = s - a)%R.
intros zDef'.
repeat split ; try easy.
intros _.
destruct (Req_dec t (rnd_p (b - z))) as [tDef'|Hslip'].
rewrite tDef', zDef'.
apply f_equal.
ring.
destruct tDef as [tDef'|tDef'] ; try easy.
rewrite tDef'.
rewrite zDef'.
replace (b - (s - a))%R with (a + b - s)%R by ring.
apply Rem8_Eq6...
clear -Hp' ; omega.
clear -Hp ; omega.
assert (Fz: generic_format two fexp (s - a)).
apply sterbenz...
rewrite sDef'.
apply generic_format_round...
admit.
now apply generic_imp_any.
Admitted.

End FastTwoSum.

Variables a b s a' b' da db t : R.
Hypothesis Fa : format a.
Hypothesis Fb : format b.
Hypothesis sDef : s = rnd_p (rnd_p2 (a + b)) \/ s = rnd_p (a + b).
Hypothesis a'Def : a' = rnd_p (rnd_p2 (s - b)) \/ a' = rnd_p (s - b).
Hypothesis b'Def : b' = rnd_p2 (s - a') \/ b' = rnd_p (rnd_p2 (s - a')) \/ b' = rnd_p (s - a').
Hypothesis daDef : da = rnd_p (rnd_p2 (a - a')) \/ da = rnd_p (a - a').
Hypothesis dbDef : db = rnd_p (rnd_p2 (b - b')) \/ db = rnd_p (b - b').
Hypothesis tDef : t = rnd_p (rnd_p2 (da + db)) \/ t = rnd_p (da + db).

Theorem TwoSum :
  (s = rnd_p (a + b) -> (t = a + b - s)%R) /\ (s <> rnd_p (a + b) -> t = rnd_p (a + b - s)).
Proof with auto'.
destruct (Rle_or_lt (Rabs a) (Rabs b)) as [Hab|Hab].
(* *)
destruct (FastTwoSum b a s a' da Fb Fa) as (a'Def',H).
now rewrite Rplus_comm.
clear -a'Def ; tauto.
exact daDef.
apply canonic_exp_monotone_abs...
admit.
rewrite Rplus_comm in H.
replace t with da.
exact H.
assert (dbDef': db = R0).
clear H.
assert (Hb: (b = s - a')%R) by (rewrite a'Def' ; ring).
assert (Hb': b' = b).
rewrite Hb.
apply generic_imp_any.
exact b'Def.
now rewrite <- Hb.
replace R0 with (b - b')%R by (rewrite Hb' ; apply Rplus_opp_r).
apply generic_imp_any.
clear -dbDef ; tauto.
rewrite Hb'.
unfold Rminus.
rewrite Rplus_opp_r.
apply generic_format_0.
rewrite dbDef', Rplus_0_r in tDef.
apply sym_eq.
apply generic_imp_any.
clear -tDef ; tauto.
destruct (Req_dec s (rnd_p (a + b))) as [Heq|Hslip].
(* . *)
destruct H as (H,_).
specialize (H Heq).
rewrite H.
replace (a + b - s)%R with (- (s - (a + b)))%R by ring.
apply generic_format_opp.
rewrite Heq.
apply plus_error...
(* . *)
destruct H as (_,H).
specialize (H Hslip).
rewrite H.
apply generic_format_round...
(* *)
destruct (Rle_or_lt (Rabs b) (Rabs s)) as [Hbs|Hbs].
assert (Fda: format (a - a')).
apply sterbenz...
destruct a'Def as [H|H] ; rewrite H ; apply generic_format_round...
admit.
assert (daDef': da = (a - a')%R).
apply generic_imp_any.
clear -daDef ; tauto.
exact Fda.
destruct (FastTwoSum s (-b) a' (-b') (-db)) as (b'Def',H).
destruct sDef as [H|H] ; rewrite H ; apply generic_format_round...
now apply generic_format_opp.
exact a'Def.
rewrite <- Ropp_minus_distr'.
rewrite 3!round_NE_opp.
destruct b'Def as [H|[H|H]].
now left ; apply f_equal.
now right ; left ; apply f_equal.
now right ; right ; apply f_equal.
replace (- b - - b')%R with (-(b - b'))%R by ring.
rewrite 3!round_NE_opp.
destruct dbDef as [H|H].
now left ; apply f_equal.
now right ; apply f_equal.
rewrite canonic_exp_opp.
apply canonic_exp_monotone_abs...
admit.
destruct (Req_dec a' (rnd_p (s - b))) as [Heq|Hslip].
(* . *)
destruct H as (H,_).
specialize (H Heq).
rewrite daDef' in tDef.
rewrite <- (Ropp_involutive db) in tDef.
rewrite H in tDef.
replace (a - a' + - (s + - b - a'))%R with (a + b - s)%R in tDef by ring.
destruct (Req_dec s (rnd_p (a + b))) as [Heq'|Hslip'] ; split ; try easy ; intros _.
(* .. *)
apply generic_imp_any.
clear -tDef ; tauto.
replace (a + b - s)%R with (- (s - (a + b)))%R by ring.
apply generic_format_opp.
rewrite Heq'.
apply plus_error...
(* .. *)
destruct tDef as [tDef'|tDef'] ; try easy.
rewrite tDef'.
apply Rem8_Eq6...
clear -Hp' ; omega.
clear -Hp ; omega.
now case sDef.
(* . *)
destruct H as (_,H).
specialize (H Hslip).
