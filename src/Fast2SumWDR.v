(* Copyright (c) ENS de Lyon and Inria. All rights reserved. *)

Require Import MoreFlocq.
Require Import ROmega.
Require Import Psatz.
Require Import Flocq.Calc.Fcalc_digits.
Require Import Flocq.Prop.Fprop_div_sqrt_error.
Require Import Flocq.Calc.Fcalc_ops.
Require Import Flocq.Calc.Fcalc_bracket.
Require Import Flocq.Calc.Fcalc_round.
Require Import Flocq.Prop.Fprop_relative.
Require Import Flocq.Prop.Fprop_plus_error.
Require Import Flocq.Prop.Fprop_Sterbenz.
Require Import Remarks.

(******************************************************************************)
(* A formal proof of Fast2Sum with Double Roundings, using the Flocq library. *)
(*                                                                            *)
(******************************************************************************)

Set Implicit Arguments.

(* Definition two := radix2. Let two := radix2. Local Notation two := radix2. *)
Local Notation two := radix2 (only parsing).
Local Notation pow e := (bpow two e).
Local Open Scope Z_scope.

Section ProofFast2SumWDR.
Variables p p' emin : Z.
Hypothesis Hp : Zlt 2 p.
Hypothesis Hp' : Zlt 1 p'.

Local Instance p_gt_0 : Prec_gt_0 p.
now apply Zlt_trans with (2 := Hp).
Qed.

Local Instance p_p'_gt_0 : Prec_gt_0 (p + p').
apply Zlt_trans with (1 := refl_equal Lt : (0 < 2 + 1)%Z).
now apply Zplus_lt_compat.
Qed.

Local Instance p'_ge_0 : Prec'_ge_0 p'.
now apply Zlt_le_weak; apply Zlt_trans with (2 := Hp').
Qed.

Local Notation fexp := (FLX_exp p).
Local Notation fexp2 := (FLX_exp (p + p')).
Local Notation format := (generic_format two fexp).
Local Notation cexp := (canonic_exp two fexp).
Local Notation mant := (scaled_mantissa two fexp).
Local Notation format2 := (generic_format two fexp2).
Local Notation cexp2 := (canonic_exp two fexp2).

Lemma FLX_exp2_le : forall e, (e - fexp2 e <= p + p')%Z.
Proof.
unfold FLX_exp.
intros; omega.
Qed.

Variable choice : Z -> bool.

Local Notation rnd_p := (round two fexp (Znearest choice)).
Local Notation rnd_p2 := (round two fexp2 (Znearest choice)).

Variables a b s z t : R.
Hypothesis Fa : format a.
Hypothesis Fb : format b.
Hypothesis sDef : s = rnd_p (rnd_p2 (a + b)).
Hypothesis zDefOr :
  z = rnd_p2 (s - a) \/ z = rnd_p (rnd_p2 (s - a)) \/ z = rnd_p (s - a).
Hypothesis tDefOr : t = rnd_p (rnd_p2 (b - z)) \/ t = rnd_p (b - z).

Hypothesis cexp_le : (cexp b <= cexp a)%Z.

(** The non-overflow hypothesis is not needed in Flocq's formalism. *)

Lemma plus_cexp2 : forall u, (p' + cexp2 u = cexp u).
Proof.
clear; intros u.
unfold FLX_exp, canonic_exp; omega.
Qed.

Lemma cexp2_minus : forall u, (cexp2 u = cexp u - p').
Proof.
clear; intros u.
unfold FLX_exp, canonic_exp; omega.
Qed.


(* Specific to FLX ? *)
Theorem cexp_bpow : forall x e, x <> R0 -> cexp (x * pow e) = cexp x + e.
Proof.
clear; intros x e Zx.
unfold canonic_exp.
unfold FLX_exp.
rewrite ln_beta_mult_bpow with (1 := Zx).
ring.
Qed.

Theorem mant_bpow : forall x e, mant (x * pow e) = mant x.
Proof.
clear; intros x e.
destruct (Req_dec x 0) as [Zx|Zx].
now rewrite Zx, Rmult_0_l.
unfold scaled_mantissa.
rewrite Rmult_assoc, <- bpow_plus.
rewrite cexp_bpow with (1 := Zx).
now ring_simplify (e + - (cexp x + e)).
Qed.

Theorem round_bpow : forall x e, rnd_p (x * pow e) = (rnd_p x * pow e)%R.
Proof with auto with typeclass_instances.
clear; intros x e.
destruct (Req_dec x 0) as [Zx|Zx].
rewrite Zx, Rmult_0_l, round_0, Rmult_0_l...
unfold round.
unfold F2R; simpl.
rewrite cexp_bpow with (1 := Zx), bpow_plus.
rewrite mant_bpow.
now rewrite Rmult_assoc.
Qed.


Theorem mant_N' : forall x m, x <> R0 ->
  x = (rnd_p (pow (cexp x) * m)) -> mant x = Z2R (Znearest choice m).
Proof with auto with typeclass_instances.
intros x m Zx H.
destruct (Req_dec m 0) as [Zm|Zm].
elim Zx.
rewrite H, Zm, Rmult_0_r.
apply round_0...
rewrite Rmult_comm in H.
destruct (ln_beta_round two fexp (Znearest choice) (m * pow (cexp x))) as [Er|Er].
(* general case: x is not a power of the radix, and hence in the same slice as m * pow (cexp x) *)
now rewrite <- H.
rewrite <- H in Er.
unfold scaled_mantissa.
rewrite H at 1.
unfold round, F2R ; simpl.
unfold scaled_mantissa, canonic_exp at 2 4.
rewrite <- Er.
now rewrite 2!Rmult_assoc, <- 2! bpow_plus, 2!Zplus_opp_r, 2!Rmult_1_r.
(* special case: x is a power of the radix in another slice *)
rewrite <- H in Er.
destruct (Zle_or_lt (ln_beta radix2 (m * (pow (cexp x)))) (fexp (ln_beta radix2 (m * pow (cexp x))))) as [He|He].
(* - what about formats that can round to zero? TODO *)
elim Zle_not_lt with (1 := He).
unfold FLX_exp at 1.
clear -Hp ; omega.
(* - m * pow (cexp x) is in the representable range *)
revert Er.
rewrite Zmax_l with (1 := Zlt_le_weak _ _ He).
rewrite <- canonic_exp_abs.
intros Er.
generalize (f_equal (fun x => ln_beta_val _ x (ln_beta two x)) Er).
rewrite canonic_exp_abs in Er.
rewrite ln_beta_bpow, ln_beta_abs, canonic_exp_abs.
intros E'.
rewrite <- (cond_Ropp_Rlt_bool x) in Er.
apply (f_equal (cond_Ropp (Rlt_bool x 0))) in Er.
rewrite cond_Ropp_involutive in Er.
unfold scaled_mantissa.
assert (cond_Ropp (Rlt_bool x 0) (pow (ln_beta radix2 (m * pow (cexp x)) + - cexp x)) *
  pow (cexp x + - cexp (m * pow (cexp x))) =
  Z2R (Znearest choice (m * pow (cexp x + - cexp (m * pow (cexp x))))))%R as Hx.
apply Rmult_eq_reg_r with (pow (cexp (m * pow (cexp x)))).
rewrite <- 2!cond_Ropp_mult_l, <- 2!bpow_plus.
ring_simplify (ln_beta radix2 (m * pow (cexp x)) + - cexp x +
  (cexp x + - cexp (m * pow (cexp x))) + cexp (m * pow (cexp x))).
rewrite bpow_plus, <- Rmult_assoc.
now rewrite <- Er.
apply Rgt_not_eq.
now apply bpow_gt_0.
rewrite Er at 1.
rewrite <- cond_Ropp_mult_l, <- bpow_plus.
revert Hx.
rewrite <- Z2R_Zpower, <- Z2R_cond_Zopp.
intros Hx.
(* what about other rounding functions? TODO *)
rewrite Znearest_scale with (2 := sym_eq Hx).
apply refl_equal.
apply (bpow_le two 0).
apply Zle_minus_le_0.
unfold canonic_exp.
apply monotone_exp...
fold (cexp x).
rewrite E'.
apply Zle_succ.
unfold canonic_exp at 3.
generalize (proj1 (valid_exp _ _) He).
rewrite <- E'.
apply Zle_minus_le_0.
Qed.

Theorem mant_N : forall x m,
  x = (rnd_p (pow (cexp x) * m)) -> Ztrunc (mant x) = Znearest choice m.
Proof with auto with typeclass_instances.
intros x m H.
destruct (Req_dec x 0) as [Zx|Zx].
  destruct (Req_dec m 0) as [Zm|Zm].
    rewrite Zm, Zx, scaled_mantissa_0.
    change R0 with (Z2R 0).
    rewrite (Zrnd_Z2R (Znearest _)).
    now apply Ztrunc_Z2R.
  destruct (ln_beta two (pow (cexp x) * m)) as (em,Em).
  (* refine (let Em' := Em _ in _). *)
  assert (pow (cexp x) <> 0)%R as hpow  by now apply Rgt_not_eq; apply bpow_gt_0.
  pose proof Rmult_integral_contrapositive_currified _ _ hpow Zm as Em0.
  specialize (Em Em0).
  contradict Zm.
  apply Rmult_eq_reg_l with (pow (cexp x)); trivial.
  apply (round_bounded_large _ fexp (Znearest choice)) in Em.
    rewrite <- H, Zx in Em.
    elim Rle_not_lt with (1 := proj1 Em).
    rewrite Rabs_R0.
    now apply bpow_gt_0.
  unfold FLX_exp.
  now clear -Hp; omega.
apply eq_Z2R.
rewrite <- scaled_mantissa_generic.
  now apply mant_N'.
rewrite H.
now' apply generic_format_round.
Qed.

Theorem AuxThm : (s <> 0)%R -> (cexp s <= 1 + (cexp a))%Z.
Proof.
intros Zs.
apply Rem2 with (p' := p') (choice := choice) (b := b); trivial.
now auto with zarith.
now auto with zarith.
Qed.

Theorem Thm2a : (z = s - a)%R.
Proof with auto'.
pose (ea := cexp a).
pose (eb := cexp b).
pose (es := cexp s).
pose (ma := mant a).
pose (mb := mant b).
pose (ms := mant s).
pose (Ma := Ztrunc ma).
pose (Mb := Ztrunc mb).
pose (Ms := Ztrunc ms).
assert (Prec'_ge_0 p') as HTC by auto'.
assert (format (s - a) -> z = s - a)%R as HA. (* key idea *)
  intro Fsa.
  case zDefOr as [zDef|[zDef|zDef]].
  + rewrite ->zDef,-> !round_generic; auto'.
    now' apply formats.
  + now' rewrite zDef, (round_generic  _ _ _ _ (formats Fsa)), round_generic.
  + now' rewrite zDef, !round_generic; auto'.
  + apply HA; clear HA.
destruct (@Req_dec s R0) as [Zs|Zs].
  now rewrite Zs, Rminus_0_l; apply generic_format_opp.
assert (H : es = ea + 1 \/ (es <= ea)%Z).
  now pose proof (AuxThm Zs) as Haux; unfold es, ea; clear - Haux; omega.
(* s = _ *)
generalize sDef.
pose proof error_le_half_ulp_round as Hf.
pose proof error_le_half_ulp as L.
(* pose proof ulp_half_error_f as Hf.
pose proof ulp_half_error as L.*)
specialize (L two fexp2 (@FLX_exp_valid _ p_p'_gt_0) choice (a + b)%R).
set (eps' := (rnd_p2 (a + b) - (a + b))%R) in *.
replace (rnd_p2 (a + b)) with (a + b + eps')%R by (unfold eps'; ring).
rewrite ulp_neq_0 in L.
2: now' intros K; apply Zs; rewrite sDef, K, !round_0.
rewrite cexp2_minus in L.
(* unfold ulp in L; rewrite cexp2_minus in L.*)
rewrite Fa, Fb; fold ma mb Ma Mb ea eb.
(* rewrite Fa Fb at 1; fold ma mb Ma Mb ea eb.*)
unfold F2R; simpl.
assert (rnd_p2 (a + b) <> R0) as Zab.
  now' intro K; apply Zs; rewrite sDef, K, round_0.
(* pose proof FLX_round_N_eq0 Zab. *)
assert ((Rabs eps' <= pow (es) * pow (- p' - 1))%R) as Heps.
  unfold es; rewrite sDef.
  rewrite sDef in Zs.
  pose proof canonic_exp_round_ge two fexp (Znearest choice) _ Zs as HGe.
  pose proof canonic_exp_round_ge two fexp2 (Znearest choice) _ Zab as HGe2.
  assert (cexp (a + b) - p' <= cexp (rnd_p (rnd_p2 (a + b))) - p') as cle.
    now rewrite !cexp2_minus in HGe2; clear - HGe HGe2; omega.
  apply Rle_trans with (1 := L).
  unfold Zminus; pattern (pow (-p'+-(1))); rewrite bpow_plus; simpl.
rewrite <-Rmult_assoc, <-bpow_plus.
  unfold Zminus in cle.
  pose proof bpow_le two _ _ cle as Hle'.
  now simp2; clear - Hle'.
intro Hkey.
assert (0 <= p) as p_ge_0 by (clear - Hp; omega).
assert (format s) as Fs.
  now' rewrite sDef; apply (generic_format_round two fexp).
pose proof FLX_mant_le p_ge_0 Fa as Ha.
pose proof FLX_mant_le p_ge_0 Fb as Hb.
pose proof FLX_mant_le p_ge_0 Fs as Hs.
fold ma Ma in Ha.
fold mb Mb in Hb.
fold ms Ms in Hs.
(* no simpl yet *)
pose proof Z2R_le _ _ Ha as Ha'.
pose proof Z2R_le _ _ Hb as Hb'.
pose proof Z2R_le _ _ Hs as Hs'.
rewrite -> Z2R_abs in *.
rewrite -> Z2R_minus in *.
rewrite -> Z2R_Zpower in Ha', Hb', Hs'...
simpl in Ha', Hb', Hs'. (* now *)
pose proof Rabs_le_inv _ _ Ha' as [Ha1 Ha2].
pose proof Rabs_le_inv _ _ Hb' as [Hb1 Hb2].
pose proof Rabs_le_inv _ _ Hs' as [Hs1 Hs2].
pose proof bpow_ge_1 two p_ge_0 as Hp0.
destruct H as [H1|H2].
(* 1. *)
  pose (delta := Zminus ea eb).
(* pose (mu := 2 * Ms - Ma). simpl Zmult in mu. *)
  pose (mu := Ms * 2 - Ma).
  assert (s - a = F2R (Float two mu ea))%R as HA.
    unfold mu.
    rewrite Fs, Fa, <- Fminus_same_exp, F2R_minus.
    replace (Ms * 2) with (Ms * two ^ (es - ea)).
      rewrite <- F2R_change_exp.
        now unfold Ma, ma, ea, Ms, ms, es.
      rewrite H1.
      now apply Zle_succ.
    rewrite H1.
    fold (Zsucc ea).
    now rewrite <-Zminus_succ_l, Zminus_diag.
  replace (Z2R Ma * pow ea)%R with a by easy.
  rewrite HA.
  rename HA into H'.
  destruct (Rlt_or_le (Rabs (Z2R mu)) (pow p)) as [Hlt|Hle].
    now apply (FLX_format_Rabs_Fnum p (fx := Float two mu ea)).
  assert (Rabs (Z2R mu) = pow p) as Hpow.
     revert Hkey.
     replace (Z2R Ma * pow ea + Z2R Mb * pow eb + eps')%R
      with (pow es * ((Z2R Ma + Z2R Mb * pow (-delta) + 2*(eps'/pow es))/2))%R.
        2: unfold delta; rewrite H1.
        2: unfold Zminus; rewrite -> Zopp_plus_distr, ->Zopp_involutive,->!bpow_plus; simpl.
        2: now rewrite bpow_opp; field; apply Rgt_not_eq; apply bpow_gt_0.
    set (eps := (eps' / pow es)%R).
    intro H'0.
    pose proof (mant_N H'0) as H0.
    fold ms in H0.
    apply Rle_antisym; [|exact Hle].
    (* |- Rabs (Z2R mu) <= pow p *)
    apply Rabs_Z2R_succ...
    set (x := ((Z2R Ma + Z2R Mb * pow (- delta) + 2 * eps)/2)%R)in *.
    pose proof (Znearest_N choice x) as Habs.
    pose proof Rabs_le_inv _ _ Habs as [lb ub].
    rewrite <- H0 in lb, ub; clear Habs.
    fold Ms in lb, ub |- *.
    unfold mu, Ms.
    rewrite ->Z2R_minus, ->Z2R_mult; simpl.
    unfold x in lb, ub.
    assert (0 < pow es)%R as Ppes by apply bpow_gt_0.
    assert (Rabs (eps) <= / 2 * pow (-p'))%R as Heps1.
      clear - Ppes Heps.
      unfold eps.
      rewrite Rabs_Rdiv_pos...
      unfold Zminus in Heps; rewrite bpow_plus in Heps; simpl in Heps.
      simp2.
        2: now apply Rgt_not_eq.
      simpdiv (pow es) Ppes.
        2: now apply Rgt_not_eq.
      psatzl R.
    pose proof Rabs_le_inv _ _ Heps1 as [ll uu].
    clear Heps1.
    assert (0 < pow (- delta) <= 1)%R as [Hdelta0 Hdelta1].
      unfold delta.
      change 1%R with (pow 0).
      split; [|apply bpow_le; unfold ea, eb; clear - cexp_le; omega].
      now apply bpow_gt_0.
    set (D := pow (- delta)) in *.
    assert (2 <= pow p')%R as Hp'2.
      now change 2%R with (pow 1); apply bpow_le; clear - Hp'; omega.
    assert (0 < 2 * pow (- p') <= 1)%R as [_P' P'_].
      rewrite bpow_opp.
      now split; simpdiv_goal (pow p') (bpow_gt_0 two p'); psatzl R.
    set (P' := pow (- p')) in *.
    apply Rabs_lt; split; simp2.
    (* left *)
      assert (- (pow p - 1) <= - (pow p - 1) * D <= Z2R Mb * D)%R as HB.
      (* This cannot be straightforwardly proved by [psatzl R]. *)
        rewrite <- (Rmult_1_r (-(pow p - 1))) at 1.
        split; [|apply Rmult_le_compat_r; psatzl R].
        rewrite !Ropp_mult_distr_l_reverse.
        apply Ropp_le_contravar.
        now apply Rmult_le_compat; psatzl R.
      fold Ms.
      now psatzl R.
    (* right *)
    fold Ms.
    assert (Z2R Mb * D <= (pow p - 1) * D <= (pow p - 1))%R as HB.
      rewrite <- (Rmult_1_r (pow p - 1)) at 3.
      now split; [apply Rmult_le_compat_r|apply Rmult_le_compat]; psatzl R.
    now psatzl R.
  apply generic_format_abs_inv.
  rewrite <- F2R_Zabs.
  unfold F2R ; simpl.
  rewrite ->Z2R_abs, -> Hpow, <- bpow_plus.
  apply generic_format_bpow.
  unfold FLX_exp.
  now clear -Hp ; omega.
(* 2. *)
pose (d1 := ea - eb). (* delta_1 *)
assert (a + b = F2R (Float two (2^d1 * Ma + Mb) eb))%R as HA.
  rewrite ->Fa, ->Fb; fold ma; fold mb; fold Ma; fold Mb; fold ea; fold eb.
  rewrite (@F2R_change_exp two eb Ma ea); trivial.
  rewrite <- F2R_plus, Fplus_same_exp.
  now rewrite Zmult_comm.
destruct (Zle_or_lt es eb) as [Hle|Hlt].
(* Hle : es <= eb *)
  assert (s = a + b)%R as sExact.
    rewrite sDef.
    assert (format (a + b)) as Fab.
      apply generic_format_plus...
      replace (Zmin (ln_beta radix2 a) (ln_beta radix2 b)) with (Zmin ea eb + p)%Z.
        destruct (Req_dec (a + b) 0) as [Zab'|Zab'].
          rewrite ->Zab', ->Rabs_R0.
          now apply bpow_gt_0.
        apply Rlt_le_trans with (pow (ln_beta two (a + b))).
          destruct (ln_beta two (a + b)) as (eab,Eab).
          now apply Eab.
        apply bpow_le.
        apply Zplus_le_reg_r with (-p)%Z.
        ring_simplify.
        change (cexp (a + b) <= Zmin ea eb)%Z.
        apply Zle_trans with (cexp (rnd_p2 (a + b))).
          rewrite <- 2!plus_cexp2.
          apply Zplus_le_compat_l.
          apply canonic_exp_round_ge...
        apply Zle_trans with (cexp s).
          rewrite sDef.
          apply canonic_exp_round_ge...
          now rewrite <- sDef.
        now apply Zmin_glb.
      rewrite <- Zplus_min_distr_r.
      unfold ea, eb, canonic_exp, FLX_exp.
      now apply f_equal2 ; ring.
    rewrite -> round_generic with (x := (a + b)%R)...
      apply round_generic...
    apply formats...
  rewrite sExact.
  replace (Z2R Ma * pow ea)%R with a by easy.
  replace (a + b - a)%R with b by ring.
  exact Fb.
(* Hlt : eb < es *)
pose (d2 := es - eb).
replace eb with ((-d2) + es) in HA by (unfold d2; ring).
unfold F2R in HA; simpl in HA; rewrite bpow_plus in HA.
revert Hkey.
replace (Z2R Ma * pow ea + Z2R Mb * pow eb + eps')%R
  with (pow es * (Z2R Ma * pow(d1-d2) + Z2R Mb * pow(-d2) + (eps'/pow es)))%R.
  2: unfold d1, d2, Zminus.
  2: rewrite ->Zopp_plus_distr, ->Zopp_involutive, ->!bpow_plus; simpl.
  2: now rewrite !bpow_opp; field; split; apply Rgt_not_eq; apply bpow_gt_0.
set (eps := (eps' / pow es)%R).
intro H0s.
pose proof (mant_N H0s) as H1s.
set (x := (Z2R Ma * pow (d1 - d2) + Z2R Mb * pow (- d2) + eps)%R) in *.
pose proof (Znearest_N choice x) as Habs.
fold ms Ms in H1s.
replace (Z2R Ma * pow ea)%R with a by easy.
rewrite ->Fs, ->Fa; fold es ea.
replace ea with (d1 - d2 + es) by now unfold d1, d2; clear - H2; omega.
unfold F2R; simpl; fold ms ma Ms Ma.
rewrite ->bpow_plus, <- Rmult_assoc, <- Rmult_minus_distr_r.
rewrite <- Z2R_Zpower.
2: unfold d1, d2; clear - H2; omega.
rewrite <- Z2R_mult, <- Z2R_minus.
unfold es.
rewrite <- ulp_neq_0; trivial.
apply Midpoint.FLX_ulp_mult...
apply lt_Z2R; rewrite Z2R_abs, Z2R_Zpower...
rewrite Z2R_minus, Z2R_mult, Z2R_Zpower.
2: unfold d1, d2; clear - H2; omega.
pose proof Rabs_le_inv _ _ Habs as [_x x_]; clear Habs.
rewrite <- H1s in _x, x_.
unfold x in _x, x_.
pose proof bpow_gt_0 two (- d2) as PPmd2.
assert (0 < 2 * pow (- d2) <= 1)%R as [_D2 D2_].
  split; [psatzl R|].
  assert (1 <= d2) by (unfold d2; clear - Hlt; omega).
  rewrite bpow_opp.
  pose proof bpow_gt_0 two d2 as Ppd2.
  simpdiv_goal (pow d2) Ppd2.
  2: now apply Rgt_not_eq.
  change 2%R with (pow 1).
  now apply bpow_le.
assert (0 < pow es)%R as Ppes by apply bpow_gt_0.
assert (Rabs (eps) <= / 2 * pow (-p'))%R as Heps1.
  clear - Ppes Heps.
  unfold eps.
  rewrite Rabs_Rdiv_pos...
  unfold Zminus in Heps; rewrite bpow_plus in Heps; simpl in Heps.
  simp2.
  2: now apply Rgt_not_eq.
  simpdiv (pow es) Ppes.
  2: now apply Rgt_not_eq.
  psatzl R.
clear Heps L.
assert (2 * pow (- p') <= 1)%R as Hp'2.
  (* assert (2 <= p') by (clear - Hp'; omega). *)
  (* As in the paper, we only use: *) assert (1 <= p') by (clear - Hp'; omega).
  rewrite bpow_opp.
  pose proof bpow_gt_0 two (p') as Ppp'.
  simpdiv_goal (pow (p')) Ppp'.
  2: now apply Rgt_not_eq.
  change 2%R with (pow 1).
  now apply bpow_le.
set (Pd1m2 := pow (d1 - d2)) in *.
set (Pmp' := pow (-p')) in *.
set (Pmd2 := pow (-d2)) in *.
pose proof Rabs_le_inv _ _ Heps1 as [_eps eps_]; clear Heps1.
apply Rabs_lt; split; simp2.
(* left *)
assert (-(pow p - 1) * 1 <= -(pow p - 1) * (2 * Pmd2) <= Z2R Mb * (2 * Pmd2))%R.
  split; [|apply Rmult_le_compat_r; psatzl R].
  rewrite !Ropp_mult_distr_l_reverse.
  apply Ropp_le_contravar.
  now apply Rmult_le_compat; psatzl R.
psatzl R.
(* right *)
(* Remark: using tactic [psatz R] would require csdp *)
assert (Z2R Mb * (2 * Pmd2) <= (pow p - 1) * (2 * Pmd2) <= (pow p - 1) * 1)%R.
  now split; [apply Rmult_le_compat_r|apply Rmult_le_compat]; psatzl R.
psatzl R.
Qed.

(********************************************************************)
Variable s0 : R.
Hypothesis s0Def : s0 = rnd_p (a + b).

Theorem Thm2b : s = s0 -> t = (a + b - s)%R.
Proof with auto'.
intro NoDblRndSlip.
generalize (tDefOr).
rewrite ->Thm2a, ->NoDblRndSlip, ->s0Def.
replace (b - (rnd_p (a + b) - a))%R with (-(rnd_p (a + b) - (a + b)))%R by ring.
assert (H: format (- (rnd_p (a + b) - (a + b)))).
  apply generic_format_opp.
  now' apply plus_error.
assert (H2: format2 (- (rnd_p (a + b) - (a + b)))).
  now' apply formats.
rewrite -> round_generic with (2 := H2)...
rewrite -> round_generic with (2 := H)...
replace (a + b - rnd_p (a + b))%R with (-(rnd_p (a + b) - (a + b)))%R by ring.
now intros [|].
Qed.

(********************************************************************)
Hypothesis ZNE : choice = fun n => negb (Zeven n).

Theorem Thm2c : s <> s0 -> t = rnd_p (a + b - s).
Proof with auto'.
intro DblRndSlip.
generalize (tDefOr).
rewrite Thm2a.
replace (b - (s - a))%R with ((a + b) - s)%R by ring.
rewrite Rem8_Eq6...
+ tauto.
+ now auto with zarith.
+ now auto with zarith.
Qed.

End ProofFast2SumWDR.
