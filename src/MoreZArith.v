(* Copyright (c) ENS de Lyon and Inria. All rights reserved. *)

Require Import ZArith.
Require Import Flocq.Core.Fcore_Zaux.
Require Import Tactics.

Set Implicit Arguments.

Open Scope Z_scope.

Theorem Zabs_lt : forall m n : Z, (- m < n < m) -> (Zabs n < m).
Proof.
intros m n H; destruct (Zabs_spec n) as [[I J]|[I J]]; rewrite J; omega.
Qed.

Theorem Zabs_lt_inv : forall m n : Z, (Zabs n < m) -> (- m < n < m).
Proof.
intros m n H.
destruct (Zabs_spec n) as [[I J]|[I J]]; rewrite J in H; auto with zarith.
Qed.

Theorem Zabs_le : forall m n : Z, (- m <= n <= m) -> (Zabs n <= m).
Proof.
intros m n H.
destruct (Zabs_spec n) as [[I J]|[I J]]; rewrite J; auto with zarith.
Qed.

Theorem Zabs_le_inv : forall m n : Z, (Zabs n <= m) -> (- m <= n <= m).
Proof.
intros m n H.
destruct (Zabs_spec n) as [[I J]|[I J]]; rewrite J in H; auto with zarith.
Qed.

Theorem Zlt_le_neq : forall m n, m <= n -> m <> n -> m < n.
Proof.
intros; omega.
Qed.

Theorem Zlt_le_pred : forall n m : Z, n < m -> n <= Zpred m.
Proof.
intros n m H.
apply Zlt_succ_r.
now rewrite <-Zsucc_pred.
Qed.

Theorem Zle_pred_r : forall n m : Z, n <= Zpred m -> n < m.
Proof.
intros n m H.
rewrite Zsucc_pred.
now apply Zle_lt_succ.
Qed.

Theorem Zgt_le_pred : forall n m : Z, m > n -> n <= Zpred m.
Proof.
intros; apply Zlt_le_pred.
now apply Zgt_lt.
Qed.

Theorem Zplus_permute_alt : forall n m p : Z, n + m + p = n + p + m.
Proof.
intros n m p.
repeat rewrite <- Zplus_assoc.
now f_equal; rewrite Zplus_comm.
Qed.

Theorem Zle_ex_plus :
  forall m n : Z, m <= n -> exists2 p : Z, 0 <= p & n = m + p.
Proof.
intros m n Hmn.
exists (n - m).
  now apply Zle_minus_le_0.
  now rewrite Zplus_minus.
Qed.

Theorem Zpower_1 : forall n : Z, n ^ 1 = n.
Proof.
intros; simpl.
unfold Zpower_pos; simpl.
now rewrite Zmult_comm; destruct n.
Qed.

Theorem Zmult_div_two : forall n : Z, (2 * n) / 2 = n.
Proof.
intros n.
rewrite Zmult_comm; apply Z_div_mult_full.
discriminate.
Qed.

Theorem Zdiv2_lt : forall b : Z, 0 < b -> 0 <= (b / 2) < b.
Proof.
split.
apply Z_div_pos; auto with zarith.
apply Z_div_lt; auto with zarith.
Qed.

(** Some results on parity *)

Theorem Zeven_double : forall n, Zeven (Zdouble n) = true.
Proof.
intros n.
now destruct n.
Qed.

Theorem discrZ_2xp1 : forall m n, 2 * m = 2 * n + 1 -> False.
Proof.
intros m n H.
generalize (refl_equal true).
rewrite <- (Zeven_double m) at 1.
rewrite Zdouble_mult, H.
rewrite Zeven_2xp1.
discriminate.
Qed.

Theorem discrZ_2xm1 : forall m n, 2 * m = 2 * n - 1 -> False.
Proof.
intros m n H.
generalize (refl_equal true).
rewrite <- (Zeven_double m) at 1.
rewrite Zdouble_mult, H.
replace (2 * n - 1) with (2 * (n - 1) + 1) by ring.
rewrite Zeven_2xp1.
discriminate.
Qed.
