(* Copyright (c) ENS de Lyon and Inria. All rights reserved. *)

(* File created by Erik Martin-Dorel on 2011-10-28 *)

Require Import MoreFlocq.
Require Import Psatz.
Require Import Flocq.Core.Fcore.
Require Import Flocq.Calc.Fcalc_digits.
Require Import Flocq.Calc.Fcalc_bracket.

Set Implicit Arguments.

(*
Check Rcompare_middle. Check Rcompare_Eq. Check Rcompare_Eq_inv.
Check round_N_middle.
Check Zeven_ex. Check Zeven_2xp1.
Check Zeven_Zpower. Check Zeven_mult. Check Zeven_plus.
(* Check ulp_DN_UP. *)
Check generic_format_discrete.

Check ulp_le_abs. (* Check succ_le_bpow. *)
Check FLX_format_Rabs_Fnum. Check FLX_format_Rabs_Fexp.
Check ln_beta_mult_bpow. Check ln_beta_F2R_bounds. Check ln_beta_le_Zpower.
SearchAbout [ln_beta bpow Rabs].
Check FLX_mant_le. Check FLX_Rabs_le. Check format_plus_cexp.

Check ulp_le. (* Check pred_plus_ulp. *) Check pred_ge_0. Check le_pred_lt.

(* Check round_DN_succ. Check round_UP_succ. *)

Print Rnd_NE_pt.
Print Rnd_NG_pt.
Print Rnd_N_pt.
Check round_N_pt.
Check round_NE_pt.
*)

Open Scope R_scope.
Open Scope Z_scope.

Definition eqpred (P Q : R -> Prop) := forall x, P x <-> Q x.
Definition impred (P Q : R -> Prop) := forall x, P x -> Q x.

Ltac setx beta fexp x :=
  let xd := fresh x "d" in
  let xu := fresh x "u" in
  let u_x := fresh "u_" x in
  let p_x := fresh "p_" x in
  set (xd := round beta fexp Zfloor x) in *;
  set (xu := round beta fexp Zceil x) in *;
  set (u_x := ulp beta fexp x) in *.
  (* ; set (p_x := pred beta fexp x) in *. *)

Section Midpoint.
(** Give some definitions for the midpoint predicate in any radix *)
Variables (beta : radix) (fexp : Z -> Z).
(* Variable choice : Z -> bool. *)
Definition midpoint (x : R) : Prop :=
  let xd := round beta fexp Zfloor x in
  ( x = xd + (ulp beta fexp x) / 2 )%R /\ x <> R0.
Definition midpoint2 (x : R) : Prop :=
  let xd := round beta fexp Zfloor x in
  ( x = (xd + (xd + ulp beta fexp x)) / 2 )%R /\ x <> R0.
Definition midpoint3 (x : R) : Prop :=
  (* without ulp *)
  let xd := round beta fexp Zfloor x in
  let xu := round beta fexp Zceil x in
  x = ((xd + xu) / 2)%R /\ ~ generic_format beta fexp x.
Definition midpoint4 (x : R) : Prop :=
  (* without ulp nor generic_format *)
  let xd := round beta fexp Zfloor x in
  let xu := round beta fexp Zceil x in
  x = ((xd + xu) / 2)%R /\ xd <> xu.
Definition midpoint5 (x : R) : Prop :=
  (* without Rdiv *)
  let xd := round beta fexp Zfloor x in
  let xu := round beta fexp Zceil x in
  (x - xd = xu - x)%R /\ ~ generic_format beta fexp x.
Definition midpoint6 (x : R) : Prop :=
  (* without Rdiv nor generic_format *)
  let xd := round beta fexp Zfloor x in
  let xu := round beta fexp Zceil x in
  (x - xd = xu - x)%R /\ xd <> xu.

Lemma m1_imp2 : impred midpoint midpoint2.
Proof.
red; intros x H; destruct H.
red; split; setx beta fexp x.
2: easy.
rewrite H; field.
Qed.
Lemma m2_imp3 : impred midpoint2 midpoint3.
Proof.
red; intros x H; destruct H.
assert (~ generic_format beta fexp x) as Hlet.
intro Fx.
pose proof @ulp_pos beta fexp x H0 as Hu.
pose proof round_generic beta fexp Zfloor x Fx as Heq.
setx beta fexp x; assert (x <> xd) as HA.
rewrite H.
apply Rgt_not_eq.
replace ((xd + (xd + u_x)) / 2)%R with (xd + u_x / 2)%R by now simp2; psatzl R.
assert ((0 < u_x / 2)%R) as Hu2 by now simp2; psatzl R.
set (ux2 := (u_x / 2)%R) in *.
psatzl R.
now apply HA.
red; split; trivial.
rewrite <- round_UP_DN_ulp in H; trivial.
Qed.
Lemma m3_imp4 : impred midpoint3 midpoint4.
Proof.
red; intros x H; destruct H.
pose proof round_UP_DN_ulp beta fexp x H0 as HD.
assert (neq0 : x <> 0%R) by (intro K; rewrite K in H0; apply H0; apply generic_format_0).
pose proof @ulp_pos beta fexp x neq0 as Hu.
red; split; setx beta fexp x.
rewrite H, HD; field.
apply Rlt_not_eq; rewrite HD.
psatzl R.
Qed.
Lemma m4_imp5 : impred midpoint4 midpoint5.
Proof.
red; intros x H; destruct H.
red; split.
setx beta fexp x; rewrite H; field.
intro Fx.
pose proof round_DN_UP_generic Fx.
setx beta fexp x; contradiction.
Qed.
Lemma m5_imp6 : impred midpoint5 midpoint6.
Proof with auto'.
red; intros x H; destruct H as [H H'].
pose proof generic_format_0 beta fexp as H0.
assert (x <> R0) as Zx by (intro K; rewrite K in H'; contradiction).
pose proof round_UP_DN_ulp beta fexp x H' as J.
pose proof @ulp_pos beta fexp x Zx as J'.
red; split.
assumption.
apply Rlt_not_eq.
rewrite J.
psatzl R.
Qed.
Lemma m6_imp1 : impred midpoint6 midpoint.
Proof with auto'.
red; intros x H; destruct H as [I J].
red; split.
assert (~ generic_format beta fexp x) as J'.
  intro Fx.
  pose proof (round_DN_UP_generic Fx) as Hround.
  contradiction.
pose proof round_UP_DN_ulp _ _ _ J' as K.
apply Rplus_eq_reg_r with (r := (- round beta fexp Zfloor x)%R).
setx beta fexp x.
replace (x + - xd)%R with (x - xd)%R by auto with zarith.
assert (u_x = (xu - xd)%R) as HA by (rewrite K; ring).
field_simplify.
apply Rmult_eq_reg_r with (r := 2%R).
unfold Rdiv.
replace (/ 1)%R with R1 by field.
symmetry; rewrite Rmult_assoc.
replace (/ 2 * 2)%R with R1 by field.
rewrite !Rmult_1_r.
rewrite Rmult_comm, double.
rewrite I at 1.
ring_simplify; assumption.
discrR.
intro Zx.
rewrite Zx, !round_0 in J...
Qed.

(* OLD PROOF
Context { valid_exp : Valid_exp fexp }.

Lemma m5_imp6 : impred midpoint5 midpoint6.
Proof with auto'.
red; intros x H; destruct H.
red; split.
assumption.
contradict H0.
now' apply <- generic_format_iff_Zfloor_Zceil.
Qed.
*)

(** Hence the following equivalences: *)

Theorem midpoint12 : eqpred midpoint midpoint2.
Proof.
split.
now apply m1_imp2.
intro H.
now apply m6_imp1; apply m5_imp6; apply m4_imp5; apply m3_imp4; apply m2_imp3.
Qed.
Theorem midpoint23 : eqpred midpoint2 midpoint3.
Proof.
split.
now apply m2_imp3.
intro H.
now apply m1_imp2; apply m6_imp1; apply m5_imp6; apply m4_imp5; apply m3_imp4.
Qed.
Theorem midpoint34 : eqpred midpoint3 midpoint4.
Proof.
split.
now apply m3_imp4.
intro H.
now apply m2_imp3; apply m1_imp2; apply m6_imp1; apply m5_imp6; apply m4_imp5.
Qed.
Theorem midpoint45 : eqpred midpoint4 midpoint5.
Proof.
split.
now apply m4_imp5.
intro H.
now apply m3_imp4; apply m2_imp3; apply m1_imp2; apply m6_imp1; apply m5_imp6.
Qed.
Theorem midpoint56 : eqpred midpoint5 midpoint6.
Proof.
split.
now apply m5_imp6.
intro H.
now apply m4_imp5; apply m3_imp4; apply m2_imp3; apply m1_imp2; apply m6_imp1.
Qed.

Theorem midpoint_opp : forall x, midpoint x -> midpoint (- x).
intros x.
rewrite !(midpoint12 _), !(midpoint23 _), !(midpoint34 _).
intros [Hm Zm]; red.
rewrite !round_DN_opp, !round_UP_opp.
split; setx beta fexp x.
rewrite Hm; field.
intro K; apply Zm.
rewrite <- (Ropp_involutive xu), K.
now rewrite Ropp_involutive.
Qed.

End Midpoint.


Section MidpointFLX.
(** Give some results that are typical for the FLX format *)
Variable beta : radix.
(* Variable p : Z.
Notation fexp := (FLX_exp p). *)
Let FLX p := (generic_format beta (FLX_exp p)).
Let MID p := (midpoint beta (FLX_exp p)).

Theorem ulp_FLX_p1 :
  forall beta p x, Prec_gt_0 p ->
  (ulp beta (FLX_exp p) x = Z2R beta * (ulp beta (FLX_exp (p+1)) x))%R.
Proof.
clear; intros beta p x Hp.
destruct (Req_bool_spec x 0) as [Zx|Zx].
{ rewrite Zx; unfold ulp; simpl; rewrite Req_bool_true; trivial.
  destruct (negligible_exp_spec (FLX_exp p)) as [H1|n1 H1];
    destruct (negligible_exp_spec (FLX_exp (p + 1))) as [H2|n2 H2];
    try auto with real; unfold FLX_exp in H1, H2.
- exfalso; specialize (H1 n2); omega.
- exfalso; specialize (H2 n1); red in Hp; omega.
- exfalso; red in Hp; omega. }
unfold ulp, FLX_exp, canonic_exp; rewrite Req_bool_false; trivial.
unfold Zminus.
replace (-p) with (- (p+1) + 1) by omega.
rewrite Zplus_assoc, bpow_plus, bpow_1.
field.
Qed.

Theorem ulp_FLX_p1_inv :
  forall beta p x, Prec_gt_0 p ->
  (ulp beta (FLX_exp (p + 1)) x = (ulp beta (FLX_exp p) x) / Z2R beta)%R.
Proof.
clear; intros beta p x Hp; symmetry; rewrite ulp_FLX_p1; trivial.
field.
pose proof (radix_pos beta).
now apply Rgt_not_eq; auto with real.
Qed.

Corollary radix2_ulp_FLX_p1 :
  forall p x, Prec_gt_0 p ->
  (ulp radix2 (FLX_exp p) x = (2 * ulp radix2 (FLX_exp (p+1)) x))%R.
Proof.
intros p x Hp.
now rewrite ulp_FLX_p1.
Qed.

Theorem FLX_bpow_mult :
  forall p m x, Prec'_ge_0 p -> x <> 0%R ->
  (Zabs m < beta ^ p)%Z ->
  generic_format beta (FLX_exp p) (Z2R m * bpow beta (canonic_exp beta (FLX_exp p) x))%R.
Proof with auto'.
clear; intros p m x Hp Hx0 Hmp.
apply generic_format_FLX.
exists (Float beta m (canonic_exp beta (FLX_exp p) x)).
now split.
Qed.

Theorem FLX_ulp_mult :
  forall p m x, Prec'_ge_0 p -> x <> 0%R ->
  (Zabs m < beta ^ p)%Z ->
  generic_format beta (FLX_exp p) (Z2R m * ulp beta (FLX_exp p) x)%R.
Proof with auto'.
clear; intros p m x Hp Hx0 Hmp.
rewrite ulp_neq_0; trivial.
now apply FLX_bpow_mult.
Qed.

Theorem MID_FLX_pos :
  Zeven beta = true ->
  forall p, Prec_gt_0 p -> forall x, (x > 0)%R -> MID p x -> FLX (p+1) x.
Proof with auto'.
intros E p i x x_0 H.
assert (p1_gt_0 : Prec_gt_0 (p+1)) by (red in i |- *; auto with zarith).
red.
destruct H as [H N0]; rewrite H.
set (r := round beta (FLX_exp p) Zfloor x).
destruct (Rtotal_order r 0) as [Hno|[h0|h0]].
(* < *)
contradict Hno.
apply RIneq.Rle_not_lt.
rewrite <- (round_0 beta (FLX_exp p) Zfloor).
apply round_le...
now apply Rlt_le.
(* = *)
rewrite h0, Rplus_0_l.
rewrite ulp_FLX_p1...
set (u := ulp beta (FLX_exp (p + 1)) x).
replace (Z2R beta * u / 2)%R with (Z2R beta / 2 * u)%R by field.
rewrite Z2R_beta_even_div_two with (1 := E).
apply FLX_ulp_mult; trivial.
red in i |- *; omega.
unfold Zdiv2.
(* thus *) apply Zabs_lt; split.
  apply Zlt_le_trans with (m := Z0).
  pose proof Zpower_gt_0 beta (p + 1) as hp.
  assert (0 <= p + 1) as hp' by (red in i; auto with zarith).
  specialize (hp hp').
  now auto with zarith.
  pose proof radix_gt_0 beta.
  apply Zge_le.
  apply Z_div_ge0; now auto with zarith.
  apply Zlt_le_trans with (m := beta).
  apply Z_div_lt; auto with zarith.
  now apply Zlt_gt; apply radix_gt_0.
  rewrite <- (Zpower_1 beta) at 1.
  apply Zpower_le with (r := beta).
  now red in i; auto with zarith.
(* > *)
rewrite <- ulp_DN... fold r.
rewrite ulp_FLX_p1...
(*replace (ulp beta (FLX_exp p) r /2)%R with (ulp beta (FLX_exp (p+1)) r).*)
generalize (refl_equal r).
unfold r at 2; rewrite generic_format_round with (x := x) at 1...
unfold F2R; simpl.
fold r.
pose (cr := canonic_exp beta (FLX_exp p) r).
(* fold (ulp beta (FLX_exp p) r). *)
rewrite <- ulp_neq_0; auto with real.
rewrite ulp_FLX_p1...
intros Hr; rewrite Hr at 1; clear Hr.
set (u := ulp beta (FLX_exp (p+1)) r).
set (mr := scaled_mantissa beta (FLX_exp p) r).
replace (Z2R (Ztrunc mr) * (Z2R beta * u) + Z2R beta * u / 2)%R with
  (Z2R (Ztrunc mr * beta + (beta / 2)) * u)%R.
2: rewrite <- Rmult_assoc, <- Z2R_mult, Z2R_plus.
2: rewrite <- Z2R_beta_even_div_two with (1 := E); field.
apply FLX_ulp_mult; auto with real.
red in i |- *; omega.
assert (0 <= p)%Z as hp0 by (red in i; omega).
pose proof (@FLX_mant_le beta _ hp0 r) as Hm.
assert (K:generic_format beta (FLX_exp p) r) by now' apply generic_format_round.
specialize (Hm K); clear K.
fold mr in Hm.
(* And finally some ZArith bookkeeping: *)
apply Zabs_lt.
apply Zabs_le_inv in Hm.
assert (0 < beta) as b0 by apply radix_gt_0.
pose proof Zdiv2_lt b0 as h2.
split; rewrite Zpower_plus, Zpower_1; auto with zarith;
  set (B := beta ^ p) in *; set (b2 := beta / 2) in *;
  set (Mr := Ztrunc mr) in *.
destruct Hm as [Hm' _].
2: destruct Hm as [_ Hm'].
clear - Hm' b0 b2 h2.
(* apply Zle_pred_r; unfold Zpred. *)
(* apply le_Z2R; rewrite !Z2R_plus, !Z2R_opp, !Z2R_mult; simpl. *)
pose proof Zmult_le_compat_r _ _ _ Hm' (Zlt_le_weak _ _ b0) as HK.
psatzl Z.
clear - Hm' b0 b2 h2.
pose proof Zmult_le_compat_r _ _ _ Hm' (Zlt_le_weak _ _ b0) as HK.
psatzl Z.
Qed.

Theorem MID_FLX :
  Zeven beta = true ->
  forall p, Prec_gt_0 p -> forall x, MID p x -> FLX (p+1) x.
Proof with auto'.
intros E p i x H.
red.
destruct (Rtotal_order x 0) as [H0|[H0|H0]].
(* = *)
2: rewrite H0; apply generic_format_0.
(* < *)
rewrite <- (Ropp_involutive x) at 1.
apply generic_format_opp.
apply MID_FLX_pos...
auto with real.
apply midpoint_opp...
(* > *)
now apply MID_FLX_pos.
Qed.

Theorem MID_FLX_Zeven :
  forall p x,
  MID p x -> FLX (p+1) x ->
  Zeven beta = true.
Proof.
intros p x HM HF.
generalize (refl_equal x).
destruct HM as [HM N0].
rewrite HM at 2.
rewrite HF at 1.
unfold round; unfold F2R; simpl.
rewrite ulp_neq_0; trivial.
unfold ulp.
set (c := canonic_exp beta (FLX_exp p) x).
set (P := bpow beta c).
set (c1 := canonic_exp beta (FLX_exp (p+1)) x).
set (P1 := bpow beta c1).
set (m := scaled_mantissa beta (FLX_exp p) x).
set (m1 := scaled_mantissa beta (FLX_exp (p+1)) x).
(*unfold Rdiv; rewrite Rmult_comm with (r2 := (/ 2)%R), <- Rmult_plus_distr_r.*)
intro HH.
apply Z2R_beta_even.
(* unfold canonic_exp, FLX_exp in c, c1. *)
(* HH : Zm1 * beta^(c-1) = Zm * beta^c + beta^c / 2 *)
(* |- beta / c = Zm1 - beta * Zm *)
exists (Ztrunc m1 - beta * Zfloor m).
apply Rmult_eq_reg_l with (r := P1).
rewrite Z2R_minus, Z2R_mult.
rewrite Rmult_minus_distr_l.
rewrite Rmult_comm.
rewrite HH.
rewrite <- Rmult_assoc.
unfold Rdiv.
rewrite <- Rmult_assoc.
replace (P1 * Z2R beta)%R with P.
field.
unfold P, P1, c, c1.
unfold canonic_exp, FLX_exp; simpl.
rewrite <- bpow_1.
rewrite <- bpow_plus.
f_equal.
omega.
apply Rgt_not_eq.
now apply bpow_gt_0.
Qed.

End MidpointFLX.

Section MidpointFLT.
(* And (almost) idem for FLT. *)
End MidpointFLT.

Inductive surround_org (M : R -> Prop) (x : R) (a b : R) :=
  Surround_org : M a -> M b -> (a <= x <= b)%R ->
  (forall c, (a < c < b)%R -> ~ M c) -> surround_org M x a b.
(** We may require (a <= x < b) to ensure uniqueness of {a,b} if (M x). *)
Inductive surround (M : R -> Prop) (x : R) (a b : R) : Type :=
  Surround : M a -> M b -> (a <= x < b)%R ->
  (forall c, (a < c < b)%R -> ~ M c) -> surround M x a b.

(* (* First specification based on the paper: radix2, FLX *)
Section SurroundFLX_Radix2.
Notation beta := radix2 (only parsing).
Variable p : Z.
Notation fexp := (FLX_exp p).
Variable rnd : R -> Z.
(* Variable choice : Z -> bool. *)
Notation format := (generic_format beta fexp).
Notation midp := (midpoint beta fexp).
Notation u := (ulp beta fexp).

(* Let notpow_mid_dn x := (x - u x / 2)%R. *)
(* Let notpow_mid_up x := (x + u x / 2)%R. *)
(* Let POSpow_mid_dn x := (x - u x / 4)%R. *)
(* Let POSpow_mid_up x := (x + u x / 2)%R. *)
(* Let NEGpow_mid_dn x := (x - u x / 2)%R. *)
(* Let NEGpow_mid_up x := (x + u x / 4)%R. *)
Notation notpow_mid_dn x := (x - u x / 2)%R (only parsing).
Notation notpow_mid_up x := (x + u x / 2)%R (only parsing).
Notation POSpow_mid_dn x := (x - u x / 4)%R (only parsing).
Notation POSpow_mid_up x := (x + u x / 2)%R (only parsing).
Notation NEGpow_mid_dn x := (x - u x / 2)%R (only parsing).
Notation NEGpow_mid_up x := (x + u x / 4)%R (only parsing).
End SurroundFLX_Radix2.

(* We prove below a generic version instead,
with different values for POSpow_mid_dn and NEGpow_mid_up. *) *)

Section Generic.
(** Develop some generic results for any radix and any floating-point format *)
Variable rnd : R -> Z.
Variable beta : radix.
Variable fexp : Z -> Z.
Variable choice : Z -> bool.
Notation format := (generic_format beta fexp).
Notation midp := (midpoint beta fexp).
(* Notation u := (ulp beta fexp). *)

Theorem surround_left_DN :
  forall M x a b, surround M x a b -> Rnd_DN_pt M x a.
Proof.
intros M x a b [S1 S2 S3 S4].
repeat (split ; try easy).
intros g Mg Hgx.
apply Rnot_lt_le.
intros Hag.
apply S4 with (2 := Mg).
refine (conj Hag _).
now apply Rle_lt_trans with x.
Qed.

Theorem surround_unique :
  forall M x a b a' b',
    surround M x a b -> surround M x a' b' ->
    a = a' /\ b = b'.
Proof.
intros M x a b a' b' S S'.
assert (Ha : a = a').
eapply Rnd_DN_pt_unicity.
apply surround_left_DN with (1 := S).
apply surround_left_DN with (1 := S').
refine (conj Ha _).
rewrite <- Ha in S'.
destruct S as [S1 S2 S3 S4].
destruct S' as [S1' S2' S3' S4'].
apply Rle_antisym ; apply Rnot_lt_le ; intros Hb.
apply S4 with (2 := S2').
refine (conj _ Hb).
now apply Rle_lt_trans with x.
apply S4' with (2 := S2).
refine (conj _ Hb).
now apply Rle_lt_trans with x.
Qed.

Theorem midpoint_no_format :
  forall x, midpoint beta fexp x -> ~ generic_format beta fexp x.
Proof.
intros x Mx.
rewrite (midpoint12 _ _ _), (midpoint23 _ _ _) in Mx.
now destruct Mx as [_ Mx'].
Qed.

Context { valid_exp : Valid_exp fexp  }.

(** A precision-p midpoint is a number exactly halfway beetween two
consecutive precision-p FP numbers: *)
Theorem midpoint_surround_middle :
  forall z : R, (midpoint beta fexp) z ->
    forall x y : R, surround format z x y ->
      (z = (x + y) / 2)%R.
Proof with auto'.
intros z Mz x y Hzxy.
rewrite (midpoint12 _ _ _), (midpoint23 _ _ _) in Mz.
destruct Mz as [Mz Nz].
destruct Hzxy as [Fx Fy H H'].
cut (x = round beta fexp Zfloor z /\ y = round beta fexp Zceil z).
now intros [A B]; rewrite Mz,A,B.
split.
(* x *)
apply Rle_antisym.
  destruct round_DN_pt with (beta := beta) (fexp := fexp) (x := z) as [I I']...
  now' apply I'.
apply Rnot_lt_le; intro Klt.
destruct (Rle_or_lt y (round beta fexp Zfloor z)) as [Hle|Hlt].
  assert ((round beta fexp Zfloor z <= y)%R) as HA.
  apply round_le_generic...
  now apply Rlt_le; apply (proj2 H).
  assert (y = round beta fexp Zfloor z) as Ha.
  apply Rle_antisym; trivial.
  clear Hle HA.
  destruct round_DN_pt with (beta := beta) (fexp := fexp) (x := z) as [I I']...
  destruct I' as [I' I''].
  assert (Hh : (y <= z)%R).
  apply Rle_trans with (2 := I').
  rewrite Ha.
  apply Rle_refl.
  assert (y = z) as HB.
  exfalso; apply Rlt_not_le with (1 := proj2 H) (2 := Hh).
  rewrite HB in Fy; contradiction. (* for instance *)
assert (x < round beta fexp Zfloor z < y)%R as Hc by (split; trivial).
apply (H' _ Hc).
now' apply generic_format_round.
(* y => very similar *)
symmetry; apply Rle_antisym.
  destruct round_UP_pt with (beta := beta) (fexp := fexp) (x := z) as [I I']...
  apply I'...
  now apply Rlt_le.
apply Rnot_lt_le; intro Klt.
destruct (Rlt_or_le x (round beta fexp Zceil z)) as [Hle|Hlt].
  set (zu := round beta fexp Zceil z) in *.
  specialize (H' zu (conj Hle Klt)).
  apply H'.
  now' apply generic_format_round.
  assert (x = z)%R as HE.
  destruct H as [_z z_].
  apply Rle_antisym; trivial.
  apply Rle_trans with (2 := Hlt).
  now apply (proj2 (round_DN_UP_le beta fexp z)).
rewrite HE in Fx.
contradiction.
Qed.

Context { valid_rnd : Valid_rnd rnd }.

Notation u := (ulp beta fexp).
Notation cexp := (canonic_exp beta fexp).
Notation pow e := (bpow beta e).

(** Note that the following values are applicable to any radix and format *)
Notation notpow_mid_dn x := (x - u x / 2)%R (only parsing).
Notation notpow_mid_up x := (x + u x / 2)%R (only parsing).
Notation POSpow_mid_dn x := (x - u (x / Z2R beta) / 2)%R (only parsing).
Notation POSpow_mid_up x := (x + u x / 2)%R (only parsing).
Notation NEGpow_mid_dn x := (x - u x / 2)%R (only parsing).
Notation NEGpow_mid_up x := (x + u (x / Z2R beta) / 2)%R (only parsing).

Notation DN x := (round beta fexp Zfloor x).
Notation UP x := (round beta fexp Zceil x).
Notation succ' f := (f + ulp beta fexp f)%R (only parsing).

Theorem POSpow_DN_halfulp_pred_plus :
  forall (x : R), format x -> is_pow beta x -> (0 < x)%R ->
  ( x - ulp beta fexp (x / Z2R beta) / 2 =
    let pr := pred beta fexp x in pr + (x - pr) / 2 )%R.
Proof with auto'.
intros x Fx Bx Px.
rewrite POSpow_pred...
set (b := Z2R beta); set (uxb := u (x / b)).
simpl.
field.
Qed.

Lemma round_DN_pred_alt :
  forall x : R, format x -> x <> 0%R ->
    (0 < pred beta fexp x)%R ->
  forall y, (pred beta fexp x <= y < x)%R ->
  round beta fexp Zfloor y = pred beta fexp x.
Proof with auto'.
intros x Fx Zx Pp y [_y y_].
assert (0 < x)%R as Px by
  now apply Rlt_trans with (2 := pred_lt_id beta fexp x Zx).
replace y with (x - (x - y))%R by ring.
rewrite round_DN_minus_eps_pos...
split; try psatzl R.
assert (pred beta fexp x <> R0) as NZp by (apply Rgt_not_eq; auto with real).
pose proof pred_pos_plus_ulp beta fexp x Px Fx as HK.
rewrite <- HK at 1.
rewrite <-!pred_eq_pos; auto with real.
psatzl R.
Qed.

Lemma round_UP_pred_alt :
  forall x : R, format x -> x <> 0%R ->
    (0 < pred beta fexp x)%R ->
  forall y, (pred beta fexp x < y <= x)%R ->
  round beta fexp Zceil y = x.
Proof with auto'.
intros x Fx Zx Pp y [_y y_].
assert (0 < x)%R as Px by
  now apply Rlt_trans with (2 := pred_lt_id beta fexp x Zx).
set (p := pred beta fexp x) in *.
replace y with (p + (y - p))%R by ring.
unfold p; rewrite round_UP_pred_plus_eps_pos...
split; try psatzl R.
fold p; psatzl R.
assert (pred beta fexp x <> R0) as NZp by (apply Rgt_not_eq; auto with real).
pose proof pred_pos_plus_ulp beta fexp x Px Fx as HK.
rewrite <-!pred_eq_pos in HK; auto with real.
assert (u p = x - pred beta fexp x)%R as Hu.
rewrite <- HK at 1.
unfold p; field.
psatzl R.
Qed.

Lemma round_DN_pred_0 :
  forall (x : R), format x -> pred beta fexp x = R0 ->
  forall y, (0 <= y < x)%R ->
  round beta fexp Zfloor y = R0.
Proof with auto'.
intros x Fx Zp y [_y y_].
rewrite <- round_0 with beta fexp Zfloor...
destruct Rle_lt_or_eq_dec with (1 := _y) as [h0|h0].
2: now' rewrite <- h0; rewrite round_0.
apply Rle_antisym.
rewrite round_0...
apply Rnot_lt_le; intro Ky.
(* 0 < DN y <= y < x *)
assert (0 < x)%R as Px by now apply Rlt_trans with (2 := y_).
pose proof le_pred_lt beta fexp (DN y) x (generic_format_round _ _ _ _) as HP.
assert (DN y < x)%R as HA.
apply Rle_lt_trans with (2 := y_)...
now apply (proj1 (round_DN_UP_le _ _ _)).
specialize (HP Fx (*Px*) HA).
apply Rlt_not_le with (1 := Ky).
now rewrite Zp in HP; apply HP.
now' apply round_le.
Qed.

Lemma round_UP_pred_0 :
  forall (x : R), format x -> pred beta fexp x = R0 ->
  forall y, (0 < y <= x)%R ->
  round beta fexp Zceil y = x.
Proof with auto'.
intros x Fx Zp y [_y y_].
destruct Rle_lt_or_eq_dec with (1 := y_) as [h|h].
2: now' rewrite h; rewrite round_generic.
apply Rle_antisym.
now' apply round_le_generic.
assert (0 < x)%R as Px by now apply Rlt_trans with (1 := _y).
pose proof le_pred_lt beta fexp (UP y) x (generic_format_round _ _ _ _) as HP.
apply Rnot_lt_le; intro K.
specialize (HP Fx (*Px*) K).
rewrite Zp in HP.
(* 0 < y <= UP y < x *)
assert (UP y > 0)%R as HA.
apply Rlt_le_trans with (r2 := y)...
now apply (proj2 (round_DN_UP_le _ _ _)).
now apply RIneq.Rle_not_lt with (1 := HP).
Qed.

(* Check (round_UP_pred_alt, round_UP_pred_0). *)

Lemma round_UP_pred_full :
  forall (x : R), format x -> x <> 0%R -> (0 <= pred beta fexp x)%R ->
  forall y, (pred beta fexp x < y <= x)%R ->
  round beta fexp Zceil y = x.
Proof with auto'.
intros x Fx Zx Pp y [_y y_].
destruct (Rle_lt_or_eq_dec _ _ Pp) as [Pp'|Pp'].
now' apply round_UP_pred_alt.
apply round_UP_pred_0...
now split; psatzl R.
Qed.

(* Check (round_DN_pred_alt, round_DN_pred_0). *)

Lemma round_DN_pred_full :
  forall x : R, format x -> x <> 0%R ->
    (0 <= pred beta fexp x)%R ->
    forall y : R,
      (pred beta fexp x <= y < x)%R -> DN y = pred beta fexp x.
Proof with auto'.
intros x Fx Zx Pp y [_y y_].
destruct (Rle_lt_or_eq_dec _ _ Pp) as [Pp'|Pp'].
now' apply round_DN_pred_alt.
rewrite <- Pp'.
apply round_DN_pred_0 with (1 := Fx)...
now split; psatzl R.
Qed.

(* Check (round_UP_succ, round_UP_pred_full). *)

Lemma round_UP_succ_full :
  forall x : R, format x ->
    (0 < x)%R ->
  forall y : R,
    (x < y <= succ' x)%R -> UP y = (x + ulp beta fexp x)%R.
Proof with auto'.
intros x Fx Px y [_y y_].
replace y with (x + (y - x))%R by ring.
rewrite round_UP_plus_eps...
rewrite succ_eq_pos; auto with real...
rewrite Rle_bool_true; auto with real.
now psatzl R.
Qed.

(* Check (round_DN_succ, round_DN_pred_full). *)

Lemma round_DN_succ_full :
  forall x : R, format x ->
    (0 < x)%R ->
  forall y : R,
    (x <= y < succ' x)%R -> DN y = x.
Proof with auto'.
intros x Fx Px y [_y y_].
replace y with (x + (y - x))%R by ring.
rewrite round_DN_plus_eps...
rewrite Rle_bool_true; auto with real.
now psatzl R.
Qed.

Theorem POSpow_ulp_le :
( forall x, format x -> is_pow beta x -> 0 < x -> u (x / Z2R beta) <= x )%R.
Proof with auto'.
intros x Fx Bx Px.
(* OLD PROOF
destruct (generic_format_EM beta fexp (x / Z2R beta)) as [h|h].
apply Rle_trans with (r2 := (x / Z2R beta)%R).
apply ulp_le_id...
apply Rdiv_pos...
now apply radix_pos.
set (b := Z2R beta) in *.
(* thus *) apply Rlt_le.
  now apply Rdiv_radix_lt.
pose proof ulp_DN_UP beta fexp _ h as Hu.
set (b := Z2R beta) in *.
assert (u (x / b) = UP (x / b) - DN (x / b))%R as Hu' by (rewrite Hu; field).
rewrite Hu'.
destruct (Req_dec (pred beta fexp x) R0) as [h0|h0].
rewrite round_UP_pred_0 with (x := x) (y := (x / b)%R)...
rewrite round_DN_pred_0 with (x := x) (y := (x / b)%R)...
psatzl R.
split.
apply Rlt_le; apply Rdiv_pos...
now apply radix_pos.
(* thus *) now apply Rdiv_radix_lt.
split.
apply Rdiv_pos...
now apply radix_pos.
(* thus *) apply Rlt_le.
  now apply Rdiv_radix_lt.
assert (0 < pred beta fexp x)%R as HP.
apply Rnot_le_lt; intro K.
apply h0.
apply Rle_antisym...
now apply pred_ge_0.
destruct (Rge_or_gt (pred beta fexp x) (x / b)).
(* >= *)
rewrite <- Hu'.
unfold ulp.
revert H.
unfold pred.
rewrite (@POSpow_ln_beta beta x) at 1...
rewrite Req_bool_true; trivial.
unfold canonic_exp.
rewrite ln_beta_div_beta.
set (B := bpow beta (fexp (ln_beta beta x - 1))) in *.
intros K.
pose proof radix_pos beta as Pbeta.
pose proof Rdiv_pos Px Pbeta as Hdiv.
cut (x - B >= 0)%R; [intro;psatzl R|idtac].
apply Rge_trans with (x / b)%R...
now apply Rgt_ge; apply Rlt_gt.
now apply Rgt_not_eq.
(* < *)
rewrite round_UP_pred_alt with (x := x) (y := (x / b)%R)...
rewrite round_DN_pred_alt with (x := x) (y := (x / b)%R)...
psatzl R.
split; trivial.
now apply Rlt_le; apply Rgt_lt.
(* thus *) now apply Rdiv_radix_lt.
split; trivial.
(* thus *) apply Rlt_le.
  now apply Rdiv_radix_lt.
*)
pose proof POSpow_pred Fx Bx Px as Hpred0.
pose proof pred_ge_0 beta fexp x Px Fx as Hpred1.
psatzl R.
Qed.

Theorem surround_notpow_pos :
  forall (x : R), format x -> (0 < x)%R -> ~ is_pow beta x ->
  surround midp x (notpow_mid_dn x) (notpow_mid_up x).
Proof with auto'.
intros x Fx Px NBx.
assert (NZx : x <> 0%R) by auto with real.
(* Cut *)
assert (0 <= x - u x)%R as Hge.
pose proof ulp_le_id beta fexp x Px Fx as HL; psatzl R.
constructor.
(* 1 *)
{ rewrite (midpoint12 _ _ _), (midpoint23 _ _ _), (midpoint34 _ _ _).
  red.
  case (Rle_lt_or_eq_dec _ _ Hge); intros h0.
  (* > 0 *)
  { set (y := (x - u x / 2)%R) in *.
    rewrite round_DN_pred_alt with x y...
    rewrite round_UP_pred_alt with x y...
    unfold y; rewrite notpow_pred...
    split; try field.
    pose proof ulp_pos beta fexp NZx.
    now apply Rlt_not_eq; psatzl R.
    now rewrite notpow_pred.
    pose proof ulp_pos beta fexp NZx; unfold y.
    split.
    rewrite notpow_pred...
    simp2; psatzl R.
    simp2; psatzl R.
    now' rewrite notpow_pred.
    pose proof ulp_pos beta fexp NZx; unfold y.
    split.
    rewrite notpow_pred...
    simp2; psatzl R.
    simp2; psatzl R. }
  (* = 0 *)
  { assert (u x = x)%R as HK.
    apply Rplus_eq_reg_r with (r := Ropp (u x)).
    unfold Rminus in h0; rewrite <- h0; ring.
    rewrite HK.
    rewrite (round_DN_pred_0 Fx).
    rewrite (round_UP_pred_0 Fx).
    split; try field.
    now apply Rlt_not_eq.
    now rewrite notpow_pred.
    split; simp2; psatzl R.
    now rewrite notpow_pred.
    split; simp2; psatzl R. } }
(* 2 *)
{ rewrite (midpoint12 _ _ _), (midpoint23 _ _ _), (midpoint34 _ _ _).
  red.
  pose proof ulp_pos beta fexp NZx.
  rewrite round_DN_plus_eps...
  2: rewrite Rle_bool_true; auto with real; split; simp2; psatzl R.
  rewrite round_UP_plus_eps...
  2: rewrite Rle_bool_true; auto with real; split; simp2; psatzl R.
  rewrite succ_eq_pos; auto with real.
  split; try field.
  apply Rlt_not_eq; psatzl R. }
(* 3 *)
{ pose proof ulp_pos beta fexp NZx.
  split; simp2; psatzl R. }
(* 4 *)
{ intros c [_c c_].
  rewrite (midpoint12 _ _ _), (midpoint23 _ _ _), (midpoint34 _ _ _).
  intros [Mc Mc'].
  generalize Mc. (* generalize Mc'. *)
  (* pose proof pred_plus_ulp beta fexp c as Hc. *)
  generalize Hge; rewrite <- notpow_pred; auto'; intros Hge'.
  destruct (Rtotal_order c x) as [LT|[EQ|GT]].
  (* LT *)
  { pose proof notpow_pred Fx NBx as Hpred.
    rewrite round_UP_pred_full with (x := x) (y := c)...
    rewrite round_DN_pred_full with (x := x) (y := c)...
    rewrite Hpred.
    intros HH.
    rewrite HH in _c.
    field_simplify in _c.
    now apply Rlt_irrefl with (1 := _c).
    rewrite Hpred.
    pose proof ulp_pos beta fexp NZx as Hu.
    split; trivial.
    now simp2; psatzl R.
    pose proof ulp_pos beta fexp NZx as Hu.
    split.
    now simp2; psatzl R.
    now apply Rlt_le. }
  (* EQ *)
  { rewrite EQ in Mc'.
    now' rewrite !round_generic in Mc'. }
  (* GT *)
  { rewrite round_UP_succ_full with (x := x)...
    rewrite round_DN_succ_full with (x := x)...
    intros HH; rewrite HH in c_; field_simplify in c_.
    now apply Rlt_irrefl with (1 := c_).
    pose proof ulp_pos beta fexp NZx as Hu.
    simp2; psatzl R.
    simp2; psatzl R. } }
Qed.

Theorem surround_notpow :
  forall (x : R), format x -> (x <> 0)%R -> ~ is_pow beta x ->
  surround midp x (notpow_mid_dn x) (notpow_mid_up x).
Proof.
intros x Fx NZx NBx.
destruct (Rneq_lt NZx) as [Nx|Px].
(* Nx *)
destruct (@surround_notpow_pos (Ropp x)) as [S1 S2 S3 S4].
now apply generic_format_opp.
now auto with real.
now intro K; apply NBx; rewrite <- Ropp_involutive; apply is_pow_opp.
constructor.
(* 1 *)
replace (x - u x / 2)%R with (- (- x + u (- x) / 2))%R.
2: rewrite ulp_opp; ring.
now apply midpoint_opp.
(* 2 *)
replace (x + u x / 2)%R with (- (-x - u (-x) / 2))%R.
2: rewrite ulp_opp; ring.
now apply midpoint_opp.
(* 3 *)
destruct S3 as [S3 S3'].
rewrite ulp_opp in S3, S3'.
split; psatzl R.
(* 4 *)
intros c [_c c_].
intro NMc.
pose proof midpoint_opp NMc.
apply S4 with (c := Ropp c).
2: easy.
rewrite ulp_opp.
split; psatzl R.
(* Px *)
now apply surround_notpow_pos.
Qed.

(* Context { monotone_exp : Monotone_exp fexp }. *)

Theorem surround_POSpow :
  forall (x : R), format x -> is_pow beta x -> (0 < x)%R ->
  surround midp x (POSpow_mid_dn x) (POSpow_mid_up x).
Proof with auto'.
pose proof radix_pos beta.
intros x Fx Bx Px.
assert (NZx : x <> 0%R) by auto with real.
pose proof ulp_pos beta fexp NZx as Hulp.
set (b := Z2R beta) in *.
(* Cut *)
assert (0 <= x - u (x / b))%R as Hge.
pose proof POSpow_ulp_le Fx Bx Px as HL.
fold b in HL.
psatzl R.
constructor; fold b.
(* 1 *)
rewrite (midpoint12 _ _ _), (midpoint23 _ _ _), (midpoint34 _ _ _).
red.
case (Rle_lt_or_eq_dec _ _ Hge); intros h0.
(* > 0 *)
assert (NZxb : (x / b <> 0)%R) by now apply Rgt_not_eq, Rdiv_pos.
set (y := (x - u (x / b) / 2)%R) in *.
rewrite round_DN_pred_alt with x y...
rewrite round_UP_pred_alt with x y...
unfold y; rewrite POSpow_DN_halfulp_pred_plus...
simpl.
split; try field.
now apply Rlt_not_eq; apply pred_lt_id.
now rewrite POSpow_pred.
unfold y; split.
rewrite POSpow_pred...
fold b.
pose proof ulp_pos beta fexp NZxb; simp2; psatzl R.
pose proof ulp_pos beta fexp NZxb; simp2; psatzl R.
now rewrite POSpow_pred.
unfold y; split.
rewrite POSpow_pred...
fold b.
pose proof ulp_pos beta fexp NZxb; simp2; psatzl R.
pose proof ulp_pos beta fexp NZxb; simp2; psatzl R.
(* = 0 *)
set (uxb := u (x / b)) in *.
assert (uxb = x)%R as HK.
apply Rplus_eq_reg_l with (r := Ropp uxb).
ring_simplify; rewrite h0; ring.
rewrite HK.
rewrite (round_DN_pred_0 Fx).
rewrite (round_UP_pred_0 Fx).
split; try field.
now apply Rlt_not_eq.
now rewrite POSpow_pred.
split; simp2; psatzl R.
now rewrite POSpow_pred.
split; simp2; psatzl R.
(* 2 *)
rewrite (midpoint12 _ _ _), (midpoint23 _ _ _), (midpoint34 _ _ _).
red.
rewrite round_DN_plus_eps...
2: rewrite Rle_bool_true; auto with real; split; simp2; psatzl R.
rewrite round_UP_plus_eps...
2: rewrite Rle_bool_true; auto with real; split; simp2; psatzl R.
rewrite succ_eq_pos; auto with real.
split; try field.
apply Rlt_not_eq; psatzl R.
(* 3 *)
assert (NZxb : (x / b <> 0)%R) by now apply Rgt_not_eq, Rdiv_pos.
assert (0 < u x / b / 2)%R as HB.
  now repeat apply Rdiv_pos; auto with real.
set (uu := (u x / b)%R) in *.
pose proof ulp_pos beta fexp NZxb.
split; simp2; psatzl R.
(* 4 *)
intros c [_c c_].
rewrite (midpoint12 _ _ _), (midpoint23 _ _ _), (midpoint34 _ _ _).
intros [Mc Mc'].
set (uxb := (u (x / b)%R)) in *.
generalize Mc. (* generalize Mc'. *)
generalize Hge; unfold uxb; rewrite <- POSpow_pred; auto'; intros Hge'.
destruct (Rtotal_order c x) as [LT|[EQ|GT]].
(* LT *)
pose proof POSpow_pred Fx Bx Px as Hpred.
rewrite round_UP_pred_full with (x := x) (y := c)...
rewrite round_DN_pred_full with (x := x) (y := c)...
rewrite Hpred.
intros HH.
rewrite HH in _c.
field_simplify in _c.
now apply Rlt_irrefl with (1 := _c).
rewrite Hpred.
fold b uxb.
split; trivial.
now simp2; psatzl R.
rewrite Hpred.
fold b uxb.
split.
now simp2; psatzl R.
now apply Rlt_le.
(* EQ *)
rewrite EQ in Mc'.
now' rewrite !round_generic in Mc'.
(* GT *)
rewrite round_UP_succ_full with (x := x)...
rewrite round_DN_succ_full with (x := x)...
intros HH; rewrite HH in c_; field_simplify in c_.
now apply Rlt_irrefl with (1 := c_).
pose proof ulp_pos beta fexp NZx as Hu.
simp2; psatzl R.
simp2; psatzl R.
Qed.

Theorem surround_NEGpow :
  forall (x : R), format x -> is_pow beta x -> (x < 0)%R ->
  surround midp x (NEGpow_mid_dn x) (NEGpow_mid_up x).
Proof with auto'.
assert (0 < Z2R beta)%R as Pb.
  now change R0 with (Z2R 0); apply Z2R_lt; apply radix_gt_0. (* TODO: refactor *)
assert (Z2R beta <> 0%R) as NZb by auto with real.
intros x Fx Bx Nx.
destruct (@surround_POSpow (Ropp x)) as [S1 S2 S3 S4].
now apply generic_format_opp.
now apply is_pow_opp.
psatzl R.
constructor.
(* 1 *)
replace (x - u x / 2)%R with (- (- x + u (- x) / 2))%R.
2: rewrite ulp_opp; ring.
now apply midpoint_opp.
(* 2 *)
replace (x + u (x / Z2R beta) / 2)%R with (- (-x - u (-x / Z2R beta) / 2))%R.
2: rewrite Rdiv_opp_l, ulp_opp; ring || easy.
now apply midpoint_opp.
(* 3 *)
destruct S3 as [S3 S3'].
rewrite Rdiv_opp_l in S3...
rewrite ulp_opp in S3, S3'.
split; try psatzl R.
(* Here the asymmetry of [surround] does not help, we need to call [ulp_pos] *)
assert (NZxb : (x / Z2R beta <> 0)%R).
{ (* TODO: refactor *)
  apply Rlt_not_eq; unfold Rdiv.
  rewrite <-(Rmult_0_l (/ Z2R beta)).
  apply Rmult_lt_compat_r...
  now apply Rinv_0_lt_compat. }
pose proof ulp_pos beta fexp NZxb.
simp2; psatzl R.
(* 4 *)
intros c [_c c_].
intro NMc.
pose proof midpoint_opp NMc.
apply S4 with (c := Ropp c).
2: easy.
rewrite Rdiv_opp_l...
rewrite !ulp_opp.
set (uu := (u (x / Z2R beta) / 2)%R) in *.
split; psatzl R.
Qed.

Ltac discuss_pow x :=
let h:= fresh "HZ" x in
let i := fresh "HB" x in
let j := fresh "HP" x in
let k := fresh "HN" x in
destruct (Req_EM_T x R0) as [h|h];
  [|destruct (is_pow_dec beta x) as [i|i];
    [destruct (R0_lt_dec h) as [j|k]|]].

Inductive surround_spec (x : R) : Type :=
| Surround_notpow : ~ is_pow beta x -> x <> R0 ->
  surround midp x (notpow_mid_dn x) (notpow_mid_up x) ->
  surround_spec x
| Surround_pospow : is_pow beta x -> (0 < x)%R ->
  surround midp x (POSpow_mid_dn x) (POSpow_mid_up x) ->
  surround_spec x
| Surround_negpow : is_pow beta x -> (x < 0)%R ->
  surround midp x (NEGpow_mid_dn x) (NEGpow_mid_up x) ->
  surround_spec x.

Theorem surroundP :
  forall (x : R) (Fx : format x), x <> R0 -> surround_spec x.
Proof.
intros x Fx Hx.
discuss_pow x.
contradiction.
constructor 2; trivial.
now apply surround_POSpow.
constructor 3; trivial.
now apply surround_NEGpow.
constructor 1; trivial.
now apply surround_notpow.
Qed.

(* Problem met with first attempt:
When (x < 0) and (generic_format beta fexp x), we cannot prove that
(round beta fexp Zceil x = round beta fexp Zfloor x + ulp beta fexp x)
*)

(* We first prove the result with succ, before considering opposite numbers. *)
Lemma round_lt_exists_midpoint_pos :
  forall x y : R,
    (0 < round beta fexp Zceil x < round beta fexp Zfloor y)%R ->
    exists2 mu, midp mu & (x <= mu <= y)%R.
Proof with auto'.
intros x y [_x xy].
case (@surroundP (UP x)); try intros B H [M1 M2 M3 M4]...
apply round_UP_pt...
now apply Rgt_not_eq.
(* C1 *)
exists (UP x + u (UP x) / 2)%R; trivial.
pose proof @ulp_pos beta fexp (UP x) H.
split.
pose proof (proj2 (round_DN_UP_le beta fexp x)).
simp2; psatzl R.
apply Rle_trans with (r2 := (UP x + u (UP x))%R).
simp2; psatzl R.
apply Rle_trans with (r2 := DN y).
rewrite <-succ_eq_pos; auto with real.
apply succ_le_lt...
apply round_UP_pt...
apply round_DN_pt...
now apply (round_DN_UP_le).
(* C2 *)
exists (UP x + u (UP x) / 2)%R; trivial.
assert (NZx' : UP x <> 0%R) by auto with real.
pose proof @ulp_pos beta fexp (UP x) NZx'.
split.
pose proof (proj2 (round_DN_UP_le beta fexp x)).
simp2; psatzl R.
apply Rle_trans with (r2 := (UP x + u (UP x))%R).
simp2; psatzl R.
apply Rle_trans with (r2 := DN y).
rewrite <-succ_eq_pos; auto with real.
apply succ_le_lt...
apply round_UP_pt...
apply round_DN_pt...
now apply (round_DN_UP_le).
(* C3 *)
exfalso.
apply (Rlt_irrefl R0).
now apply Rlt_trans with (1 := _x) (2 := H).
Qed.

Lemma round_lt_exists_midpoint_neg :
  forall x y : R,
    (round beta fexp Zceil x < round beta fexp Zfloor y < 0)%R ->
    exists2 mu, midp mu & (x <= mu <= y)%R.
Proof with auto'.
intros x y [xy y_].
destruct (@round_lt_exists_midpoint_pos (Ropp y) (Ropp x)) as [mu M U].
rewrite round_DN_opp, round_UP_opp.
split; psatzl R.
exists (Ropp mu).
now apply midpoint_opp.
destruct U; split; psatzl R.
Qed.

Lemma round_lt_exists_midpoint_0_DN :
  forall x y : R,
    (round beta fexp Zceil x <= 0 < round beta fexp Zfloor y)%R ->
    exists2 mu, midp mu & (x <= mu <= y)%R.
Proof with auto'.
intros x y [Nx Py].
case (@surroundP (DN y)); try intros B H [M1 M2 M3 M4]...
apply round_DN_pt...
now apply Rgt_not_eq.
(* C1 *)
exists (DN y - u (DN y) / 2)%R; trivial.
split.
apply Rle_trans with (r2 := pred beta fexp (DN y)).
apply Rle_trans with (r2 := UP x).
now apply (round_DN_UP_le).
apply le_pred_lt...
apply round_UP_pt...
apply round_DN_pt...
now apply Rle_lt_trans with (1 := Nx) (2 := Py).
rewrite notpow_pred...
pose proof @ulp_pos beta fexp (DN y) (Rgt_not_eq _ _ Py).
simp2; psatzl R.
apply round_DN_pt...
pose proof @ulp_pos beta fexp (DN y) (Rgt_not_eq _ _ Py).
apply Rle_trans with (2 := proj1 (round_DN_UP_le beta fexp y)).
simp2; psatzl R.
(* C2 *)
exists (DN y - u (DN y / Z2R beta) / 2)%R; trivial.
assert (NZyd_b : (DN y / Z2R beta <> 0)%R).
{ (* TODO: refactor *)
  apply Rgt_not_eq; unfold Rdiv.
  rewrite <-(Rmult_0_l (/ Z2R beta)).
  apply Rmult_lt_compat_r...
  apply Rinv_0_lt_compat.
  now change R0 with (Z2R 0); apply Z2R_lt; apply radix_gt_0. }
split.
apply Rle_trans with (r2 := pred beta fexp (DN y)).
apply Rle_trans with (r2 := UP x).
now apply (round_DN_UP_le).
apply le_pred_lt...
apply round_UP_pt...
apply round_DN_pt...
now apply Rle_lt_trans with (1 := Nx) (2 := Py).
rewrite POSpow_pred...
set (yd_b := (DN y / Z2R beta)%R) in *.
pose proof ulp_pos beta fexp NZyd_b.
simp2; psatzl R.
apply round_DN_pt...
set (yd_b := (DN y / Z2R beta)%R) in *.
pose proof ulp_pos beta fexp NZyd_b.
apply Rle_trans with (2 := proj1 (round_DN_UP_le beta fexp y)).
simp2; psatzl R.
(* C3 *)
exfalso.
apply (Rlt_irrefl R0).
now apply Rlt_trans with (1 := Py) (2 := H).
Qed.

Lemma round_lt_exists_midpoint_0_UP :
  forall x y : R,
    (round beta fexp Zceil x < 0 <= round beta fexp Zfloor y)%R ->
    exists2 mu, midp mu & (x <= mu <= y)%R.
Proof with auto'.
intros x y [Nx Py].
destruct (@round_lt_exists_midpoint_0_DN (Ropp y) (Ropp x)) as [mu M M'].
rewrite round_UP_opp, round_DN_opp; split; psatzl R.
exists (Ropp mu).
now apply midpoint_opp.
destruct M'; split; psatzl R.
Qed.

Lemma round_lt_exists_midpoint_UP_DN :
  forall x y : R,
    (round beta fexp Zceil x < round beta fexp Zfloor y)%R ->
    exists2 mu, midp mu & (x <= mu <= y)%R.
Proof with auto'.
intros x y H.
assert (x < y)%R as Hxy.
  pose proof round_DN_UP_le beta fexp y as Hy.
  assert (UP x < UP y)%R as HA by psatzl R.
  apply Rnot_le_lt; intro K; apply (Rlt_not_le _ _ HA).
  now' apply round_le.
assert (0 < UP x < DN y \/
  UP x < DN y < 0 \/
  UP x < 0 <= DN y \/
  UP x <= 0 < DN y)%R as HA.
destruct (Rlt_le_dec R0 (UP x)) as [I|I].
now left; split.
destruct (Rlt_le_dec (DN y) R0) as [J|J].
now right; left; split.
let lem := Rle_lt_or_eq_dec in
  destruct (lem _ _ I) as [K|K]; destruct (lem _ _ J) as [L|L].
now right; right; left; split.
now right; right; left; split.
now right; right; right; split.
now exfalso; rewrite K, <- L in H; apply Rlt_irrefl with (1 := H).
destruct HA as [HA1|[HA2|[HA3|HA4]]].
now apply round_lt_exists_midpoint_pos; split; auto with real.
now apply round_lt_exists_midpoint_neg; split; auto with real.
now apply round_lt_exists_midpoint_0_UP.
now apply round_lt_exists_midpoint_0_DN.
Qed.

Theorem surround_format :
  forall z : R, ~ generic_format beta fexp z ->
    surround format z (DN z) (UP z).
Proof with auto'.
intros z Fz.
constructor.
now' apply round_DN_pt.
now' apply round_UP_pt.
pose proof round_DN_UP_lt Fz.
psatzl R.
intros c [_c c_].
now' apply float_discr_DN_UP with z.
Qed.

Definition Feven x :=
   exists f : float beta,
   x = F2R f /\ canonic beta fexp f /\ Zeven (Fnum f) = true.

Theorem Feven_DN_UP_incompat :
Monotone_exp fexp ->
  forall mu, ~ format mu -> DN mu <> R0 -> UP mu <> R0 ->
    Feven (DN mu) -> Feven (UP mu) -> False.
Proof with auto'.
intros Monot mu Fmu ZD ZU (f,(d1,(d2,d3))) (g,(u1,(u2,u3))).
unfold canonic in d2, u2.
pose proof round_UP_DN_ulp beta fexp _ Fmu as Hulp.
generalize (refl_equal (UP mu)).
rewrite u1 at 1.
rewrite Hulp, d1.
assert (NZmu : mu <> 0%R) by
  now intro K; rewrite K in Fmu; apply Fmu; apply generic_format_0.
rewrite ulp_neq_0; trivial.
unfold F2R.
(* rewrite d2, u2. *)
(* unfold canonic_exp.
destruct f as (m,e).
destruct g as (m',e').
simpl in *.
rewrite !ln_beta_F2R. *)
assert (Fexp f <= Fexp g /\ cexp mu = cexp (DN mu) \/
Fexp g <= Fexp f /\ cexp mu = cexp (UP mu)) as Hmain.
destruct (Rlt_or_le R0 (DN mu)) as [LT_D|LE_D].
pose proof canonic_exp_DN beta fexp mu LT_D as HD.
left.
split; [|now symmetry].
rewrite d2, u2.
apply canonic_exp_monotone_abs...
now rewrite <- d1.
rewrite <- d1, <- u1.
pose proof round_DN_UP_le beta fexp mu.
rewrite !Rabs_pos_eq; auto with real; psatzl R...
destruct (Rlt_or_le R0 (DN (-mu))) as [LT_D'|LE_D'].
pose proof canonic_exp_DN beta fexp _ LT_D' as HD'.
rewrite <- canonic_exp_opp.
rewrite <- HD'.
right.
split; [|now rewrite round_DN_opp, canonic_exp_opp].
rewrite d2, u2.
rewrite <- d1, <- u1.
apply canonic_exp_monotone_abs...
pose proof round_DN_UP_le beta fexp (Ropp mu).
(* [Rabs_left] has a too strong hypothesis. *)
rewrite <- (Ropp_involutive mu).
rewrite round_UP_opp, Rabs_Ropp, Rabs_pos_eq.
pattern (DN (- - mu)).
rewrite round_DN_opp, Rabs_Ropp, Rabs_pos_eq.
psatzl R.
rewrite round_UP_opp; psatzl R.
now apply Rlt_le.
rewrite round_DN_opp in LE_D'.
assert (DN mu < 0 < UP mu)%R as (Z, Z') by psatzl R.
exfalso.
apply float_discr_DN_UP with (2 := Z) (3 := Z')...
apply generic_format_0.
(* set (F := Fnum f).
set (G := Fnum g). *)
set (PF := pow (Fexp f)).
set (PG := pow (Fexp g)).
pose proof Zeven_ex (Fnum f) as [m Hm]. rewrite d3, Zplus_0_r in Hm.
pose proof Zeven_ex (Fnum g) as [n Hn]. rewrite u3, Zplus_0_r in Hn.
assert (0 < PF)%R as PPF by apply bpow_gt_0.
assert (0 < PG)%R as PPG by apply bpow_gt_0.
destruct Hmain as [[Hfg Hleft]|[Hgf Hright]].
(* Hleft *)
rewrite Hleft, d1, <- d2.
fold PF.
rewrite Hm, Hn.
rewrite !Z2R_mult; simpl.
intros H.
assert (2 * (Z2R n * (PG / PF)) = 2 * Z2R m + 1)%R as H'.
simpdiv_goal PF PPF; psatzl R.
(* assert (Fexp f <= Fexp g) as Hfg. [proved above] *)
unfold Rdiv, PG, PF in H'.
rewrite <- bpow_opp, <- bpow_plus in H'.
rewrite <- Z2R_Zpower in H'; [|clear -Hfg; omega].
simpl in H'.
assert (2 * (n * beta ^ (Fexp g - Fexp f)) = 2 * m + 1) as HZ.
apply eq_Z2R; rewrite !Z2R_plus, !Z2R_mult.
simpl; unfold Zminus.
now rewrite H'.
apply discrZ_2xp1 with (1 := HZ).
(* Hright *)
rewrite Hright, u1, <- u2.
fold PG.
rewrite Hm, Hn.
rewrite !Z2R_mult; simpl.
intros H.
assert (2 * (Z2R m * (PF / PG)) = 2 * Z2R n - 1)%R as H'.
simpdiv_goal PG PPG; psatzl R.
(* assert (Fexp g <= Fexp f) as Hgf. [proved above] *)
unfold Rdiv, PG, PF in H'.
rewrite <- bpow_opp, <- bpow_plus in H'.
rewrite <- Z2R_Zpower in H'; [|clear -Hgf; omega].
simpl in H'.
assert (2 * (m * beta ^ (Fexp f - Fexp g)) = 2 * n - 1) as HZ.
apply eq_Z2R; rewrite !Z2R_minus, !Z2R_mult.
simpl; unfold Zminus.
now rewrite H'.
apply discrZ_2xm1 with (1 := HZ).
Qed.
End Generic.


Section Nearest.
Variable beta : radix.
Variable fexp : Z -> Z.
Context { valid_exp : Valid_exp fexp }.
Variable choice : Z -> bool.
Notation RN := (round beta fexp (Znearest choice)).
Notation format := (generic_format beta fexp).
Notation midp := (midpoint beta fexp).
Notation u := (ulp beta fexp).
Notation DN x := (round beta fexp Zfloor x).
Notation UP x := (round beta fexp Zceil x).
Notation succ' f := (f + ulp beta fexp f)%R (only parsing).

Theorem round_DN_midp_pos :
  forall x, (0 < DN x)%R -> DN ((DN x + UP x) / 2) = DN x.
Proof with auto'.
intros x Pxd.
assert (NZx : x <> 0%R) by
  now intros K; rewrite K, round_0 in Pxd; auto';
  exfalso; apply (Rlt_irrefl _ Pxd).
rewrite round_DN_succ_full with (x := DN x)...
apply round_DN_pt...
split; simp2.
pose proof round_DN_UP_le beta fexp x as Hineq.
now psatzl R.
rewrite ulp_DN...
pose proof ulp_pos beta fexp NZx.
destruct (generic_format_EM beta fexp x) as [Fx|Fx].
(* format x *)
rewrite !round_generic...
now psatzl R.
(* ~ format x *)
rewrite round_UP_DN_ulp...
now psatzl R.
Qed.

(* Backup old lemma [lt_UP_le_DN_pos] on which [lt_UP_le_DN] does not depend *)
Lemma lt_UP_le_DN_pos :
  forall x y, (0 < DN x)%R -> format y -> (y < UP x -> y <= DN x)%R.
Proof with auto'.
intros x y Pxd Fy .
destruct (generic_format_EM beta fexp x) as [Fx|Fx].
rewrite !round_generic...
now apply Rlt_le.
intros Hlt.
rewrite round_UP_DN_ulp in Hlt...
apply Rnot_lt_le; intro HN.
pose proof succ_le_lt beta fexp (DN x) y as HK.
rewrite succ_eq_pos in HK; auto with real.
rewrite ulp_DN in HK...
contradict Hlt.
apply Rge_not_lt; apply Rle_ge.
apply HK...
now' apply round_DN_pt.
Qed.

Theorem lt_UP_le_DN :
  forall x y, format y -> (y < UP x -> y <= DN x)%R.
Proof with auto'.
intros x y Fy Hlt.
apply round_DN_pt...
apply Rnot_lt_le.
contradict Hlt.
apply RIneq.Rle_not_lt.
apply round_UP_pt...
now apply Rlt_le.
Qed.

Lemma pred_UP_le_DN :
  forall x, (0 < UP x)%R -> (pred beta fexp (UP x) <= DN x)%R.
Proof with auto'.
intros x Pxu.
destruct (generic_format_EM beta fexp x) as [Fx|Fx].
rewrite !round_generic...
now apply pred_le_id.
assert (pred beta fexp (UP x) < UP x)%R as Hup.
  now apply pred_lt_id; auto with real.
apply lt_UP_le_DN...
apply generic_format_pred...
now apply round_UP_pt.
Qed.

Lemma pred_UP_eq_DN :
  forall x, (0 < UP x)%R -> ~ format x -> (pred beta fexp (UP x) = DN x)%R.
Proof with auto'.
intros x Px Fx.
apply Rle_antisym.
now apply pred_UP_le_DN.
apply le_pred_lt; try apply generic_format_round...
pose proof round_DN_UP_lt Fx as HE.
psatzl R.
Qed.

Theorem Pred_UP_eq_DN :
  forall x : R, UP x <> R0 -> ~ format x -> Pred beta fexp (UP x) = DN x.
Proof with auto'.
intros x Zx Fx.
unfold Pred,Succ in *; destruct (Rcompare_spec (UP x) 0) as [LT|EQ|GT].
(* LT *)
pose proof round_UP_DN_ulp beta fexp x Fx.
rewrite <-round_DN_opp.
rewrite ulp_DN...
rewrite ulp_opp.
psatzl R.
rewrite round_DN_opp; psatzl R.
(* EQ *)
easy.
(* GT *)
now' rewrite pred_UP_eq_DN.
Qed.

Theorem Pred_UP_le_DN :
  forall x, UP x <> R0 -> (Pred beta fexp (UP x) <= DN x)%R.
Proof with auto'.
intros x Zx. simpl.
destruct (generic_format_EM beta fexp x) as [Fx|Fx].
rewrite !round_generic...
exact (proj1 (Pred_le_Succ beta fexp x)).
assert (Pred beta fexp (UP x) < UP x)%R as Hup.
  exact (proj1 (Pred_lt_Succ beta fexp Zx)).
apply lt_UP_le_DN...
apply generic_format_Pred...
now apply round_UP_pt.
Qed.

Theorem Succ_DN_eq_UP :
  forall x : R, DN x <> R0 -> ~ format x -> Succ beta fexp (DN x) = UP x.
Proof with auto'.
intros x Zx Fx.
unfold Pred,Succ in *; destruct (Rcompare_spec (DN x) 0) as [LT|EQ|GT].
(* LT *)
rewrite <-round_UP_opp.
rewrite pred_UP_eq_DN.
rewrite round_DN_opp.
now auto with real.
rewrite round_UP_opp.
now psatzl R.
contradict Fx.
now rewrite <-(Ropp_involutive x); apply generic_format_opp.
(* EQ *)
easy.
(* GT *)
rewrite round_UP_DN_ulp...
f_equal.
now' apply ulp_DN.
Qed.

Theorem Succ_DN_ge_UP :
  forall x, DN x <> R0 -> (UP x <= Succ beta fexp (DN x))%R.
Proof with auto'.
intros x Zx.
destruct (generic_format_EM beta fexp x) as [Fx|Fx].
rewrite !round_generic...
exact (proj2 (Pred_le_Succ beta fexp x)).
rewrite <-(Ropp_involutive x).
rewrite round_DN_opp, Succ_opp, round_UP_opp.
assert (UP (- x) <> R0) as Zx' by now rewrite round_UP_opp; auto with real.
pose proof Pred_UP_le_DN Zx'.
now psatzl R.
Qed.

Lemma round_UP_midp_pos :
  forall x, (0 < UP x)%R -> UP ((DN x + UP x) / 2) = UP x.
Proof with auto'.
intros x Pxu.
rewrite round_UP_pred_full with (x := UP x)...
now' apply round_UP_pt.
now apply Rgt_not_eq.
apply pred_ge_0...
now' apply round_UP_pt.
split; simp2.
2: pose proof round_DN_UP_le beta fexp x as Hineq.
2: now psatzl R.
pose proof pred_lt_id beta fexp (UP x).
assert (pred beta fexp (UP x) <= DN x)%R as HA.
2: now psatzl R. (* suff *)
now apply pred_UP_le_DN.
Qed.

(* With a weaker hypothesis than round_DN_midp_pos *)
Lemma round_DN_midp_pos' :
  forall x, (0 < UP x)%R -> DN ((DN x + UP x) / 2) = DN x.
Proof with auto'.
intros x Px.
destruct (generic_format_EM beta fexp x) as [Fx|Fx].
rewrite !(round_generic beta fexp _ x)...
replace ((x + x) / 2)%R with x by field.
now' rewrite round_generic.
pose proof round_DN_UP_lt Fx as Hlt.
rewrite round_DN_pred_full with (x := UP x)...
now apply pred_UP_eq_DN.
now' apply generic_format_round.
now apply Rgt_not_eq.
apply pred_ge_0...
now' apply generic_format_round.
split; simp2.
2: pose proof round_DN_UP_le beta fexp x as Hineq.
2: try now psatzl R.
pose proof pred_lt_id beta fexp (UP x).
assert (pred beta fexp (UP x) <= DN x)%R as HA.
2: now psatzl R. (* suff *)
now apply pred_UP_le_DN.
Qed.

(* May be proved as a corollary of round_UP_midp_pos *)
Lemma round_UP_midp_pos' :
  forall x, (0 < DN x)%R -> UP ((DN x + UP x) / 2) = UP x.
Proof with auto'.
intros x Px.
destruct (generic_format_EM beta fexp x) as [Fx|Fx].
rewrite !(round_generic beta fexp _ x)...
replace ((x + x) / 2)%R with x by field.
now' rewrite round_generic.
pose proof round_DN_UP_lt Fx as Hlt.
rewrite round_UP_succ_full with (x := DN x)...
rewrite round_UP_DN_ulp...
now rewrite ulp_DN.
now' apply generic_format_round.
split; simp2.
pose proof round_DN_UP_le beta fexp x as Hineq.
now psatzl R.
rewrite !ulp_DN, round_UP_DN_ulp...
assert (NZx : x <> 0%R) by
  now intro K; rewrite K in Fx; apply Fx; apply generic_format_0.
pose proof ulp_pos beta fexp NZx.
now psatzl R.
Qed.

Theorem round_DN_midp :
  forall x, DN ((DN x + UP x) / 2) = DN x.
Proof with auto'.
intros x.
destruct (Rtotal_order (DN x) R0) as [LT|[EQ|GT]].
3: now apply round_DN_midp_pos.
rewrite <- (Ropp_involutive x).
rewrite round_UP_opp, round_DN_opp.
replace ((-UP (-x) + - DN (-x))/2)%R with (-((UP (-x) + DN (-x))/2))%R by field.
rewrite round_DN_opp; f_equal.
rewrite Rplus_comm, round_UP_midp_pos...
now rewrite round_UP_opp; auto with real.
(* EQ : DN x = 0 *)
destruct (generic_format_EM beta fexp x) as [Fx|Fx].
pose proof round_generic beta fexp Zfloor x Fx as Hx.
rewrite Hx in EQ; rewrite !EQ, !round_0...
replace ((0 + 0)/2)%R with R0 by field.
now' rewrite round_0.
destruct (Rle_lt_dec (UP x) R0) as [Hx|Hx].
assert (UP x = 0)%R as h0.
  apply Rle_antisym...
  apply Rle_trans with (r2 := DN x); [now right|idtac].
  pose proof round_DN_UP_le beta fexp x.
  now psatzl R.
rewrite !EQ, h0.
replace ((0 + 0) / 2)%R with R0 by field.
now' rewrite round_0.
rewrite round_DN_pred_full with (x := UP x)...
now' rewrite pred_UP_eq_DN.
now' apply generic_format_round.
now apply Rgt_not_eq.
apply le_pred_lt...
now apply generic_format_0.
now' apply generic_format_round.
rewrite pred_UP_eq_DN...
rewrite !EQ.
now split; simp2; psatzl R.
Qed.

Theorem round_UP_midp :
  forall x, UP ((DN x + UP x) / 2) = UP x.
Proof with auto'.
intros x.
destruct (Rtotal_order (DN x) R0) as [LT|[EQ|GT]].
3: now apply round_UP_midp_pos'.
rewrite <- (Ropp_involutive x).
rewrite round_UP_opp, round_DN_opp.
replace ((-UP (-x) + - DN (-x))/2)%R with (-((UP (-x) + DN (-x))/2))%R by field.
rewrite round_UP_opp; f_equal.
rewrite Rplus_comm, round_DN_midp_pos'...
now rewrite round_UP_opp; auto with real.
(* EQ : DN x = 0 *)
destruct (generic_format_EM beta fexp x) as [Fx|Fx].
pose proof round_generic beta fexp Zfloor x Fx as Hx.
rewrite Hx in EQ; rewrite !EQ, !round_0...
replace ((0 + 0)/2)%R with R0 by field.
now' rewrite round_0.
destruct (Rle_lt_dec (UP x) R0) as [Hx|Hx].
assert (UP x = 0)%R as h0.
  apply Rle_antisym...
  apply Rle_trans with (r2 := DN x); [now right|idtac].
  pose proof round_DN_UP_le beta fexp x.
  now psatzl R.
rewrite !EQ, h0.
replace ((0 + 0) / 2)%R with R0 by field.
now' rewrite round_0.
rewrite round_UP_pred_full with (x := UP x)...
now' apply generic_format_round.
now apply Rgt_not_eq.
rewrite pred_UP_eq_DN...
apply lt_UP_le_DN...
now apply generic_format_0.
rewrite pred_UP_eq_DN...
rewrite !EQ.
now split; simp2; psatzl R.
Qed.

Theorem round_DN_Pred :
  forall x : R,
  generic_format beta fexp x ->
  forall y : R,
  (Pred beta fexp x <= y < x)%R ->
  round beta fexp Zfloor y = Pred beta fexp x.
Proof with auto'.
intros x Fx y Hineq.
unfold Pred,Succ in *; destruct (Rcompare_spec x 0) as [LT|EQ|GT]; try psatzl R.
rewrite <-(Ropp_involutive y), round_DN_opp.
rewrite round_UP_succ_full with (x := Ropp x); try psatzl R...
now apply generic_format_opp.
rewrite round_DN_pred_full with (x := x)...
now apply Rgt_not_eq.
now apply pred_ge_0.
Qed.

Theorem round_UP_Succ :
  forall x : R,
  generic_format beta fexp x ->
  forall y : R,
  (x < y <= Succ beta fexp x)%R ->
  round beta fexp Zceil y = Succ beta fexp x.
Proof with auto'.
intros x Fx y Hineq.
unfold Pred,Succ in *; destruct (Rcompare_spec x 0) as [LT|EQ|GT]; try psatzl R.
rewrite <-(Ropp_involutive y), round_UP_opp.
rewrite round_DN_pred_full with (x := Ropp x); try psatzl R...
now apply generic_format_opp.
apply pred_ge_0; try psatzl R...
now apply generic_format_opp.
now rewrite round_UP_succ_full with (x := x)...
Qed.

Theorem round_DN_Succ :
  forall x : R,
  generic_format beta fexp x ->
  forall y : R,
  (x <= y < Succ beta fexp x)%R ->
  round beta fexp Zfloor y = x.
Proof with auto'.
intros x Fx y Hineq.
unfold Pred,Succ in *; destruct (Rcompare_spec x 0) as [LT|EQ|GT]; try psatzl R.
rewrite <-(Ropp_involutive y), round_DN_opp.
rewrite round_UP_pred_full with (x := Ropp x); try psatzl R...
now apply generic_format_opp.
apply pred_ge_0; try psatzl R...
now apply generic_format_opp.
now rewrite round_DN_succ_full with (x := x)...
Qed.

Theorem round_UP_Pred :
  forall x : R,
  generic_format beta fexp x ->
  forall y : R,
  (Pred beta fexp x < y <= x)%R ->
  round beta fexp Zceil y = x.
Proof with auto'.
intros x Fx y Hineq.
unfold Pred,Succ in *; destruct (Rcompare_spec x 0) as [LT|EQ|GT]; try psatzl R.
rewrite <-(Ropp_involutive y), round_UP_opp.
rewrite round_DN_succ_full with (x := Ropp x); try psatzl R...
now apply generic_format_opp.
rewrite round_UP_pred_full with (x := x)...
now apply Rgt_not_eq.
now apply pred_ge_0; try psatzl R.
Qed.

Lemma cross_spec :
  forall x y,
    (x < y)%R ->
    (RN x < RN y)%R ->
    (DN y < UP x)%R ->
    RN x = DN x /\ DN x = DN y /\
    RN y = UP y /\ UP x = UP y.
Proof with auto'.
intros x y hxy Hxy Hyx.
repeat split.
(* 1 *)
destruct (round_DN_or_UP beta fexp(Znearest choice)x)as[Hx|Hx]; [easy|exfalso].
rewrite Hx in *.
destruct (round_DN_or_UP beta fexp(Znearest choice)y)as[Hy|Hy].
rewrite Hy in *.
now apply (Rlt_irrefl _ (Rlt_trans _ _ _ Hxy Hyx)).
rewrite Hy in *.
now' apply float_discr_DN_UP with beta fexp y (UP x); try apply round_UP_pt.
(* 2 *)
apply Rle_antisym.
apply round_le...
now apply Rlt_le.
apply Rnot_lt_le; intro K.
clear Hxy.
now' apply float_discr_DN_UP with beta fexp x (DN y); try apply round_DN_pt.
(* 3 *)
destruct (round_DN_or_UP beta fexp(Znearest choice)y)as[Hy|Hy]; [exfalso|easy].
rewrite Hy in *.
destruct (round_DN_or_UP beta fexp(Znearest choice)x)as[Hx|Hx].
rewrite Hx in *.
now' apply float_discr_DN_UP with beta fexp x (DN y); try apply round_DN_pt.
rewrite Hx in *.
now apply (Rlt_irrefl _ (Rlt_trans _ _ _ Hxy Hyx)).
(* 4 *)
apply Rle_antisym.
apply round_le...
now apply Rlt_le.
apply Rnot_lt_le; intro K.
clear Hxy.
now' apply float_discr_DN_UP with beta fexp y (UP x); try apply round_UP_pt.
Qed.

Theorem round_UP_cross :
  forall x y,
    (x < y)%R -> (RN x < RN y)%R -> (DN y < UP x)%R ->
    UP ((DN y + UP x) / 2) = UP x.
Proof with auto'.
intros x y hxy Hxy Hyx.
destruct (Req_dec (UP x) R0) as [Zx|Zx].
(* UP x = 0 *)
rewrite Zx.
assert (Succ beta fexp (DN y) = R0) as HA.
assert (DN y < 0)%R as Ny.
now rewrite <-Zx.
apply Rle_antisym.
apply Succ_le_lt...
now' apply generic_format_round.
now apply generic_format_0.
assert (DN y <> R0) as Zy by psatzl R.
pose proof Succ_DN_ge_UP Zy.
destruct (cross_spec hxy Hxy Hyx) as [I[J[K L]]].
now rewrite <- Zx, L.
rewrite round_UP_Succ with (x := DN y)...
now' apply generic_format_round.
rewrite HA.
now split; simp2; psatzl R.
(* UP x <> 0 *)
rewrite round_UP_Pred with (x := UP x)...
now' apply generic_format_round.
pose proof Pred_UP_le_DN Zx as HP.
pose proof round_le beta fexp Zfloor x y (Rlt_le _ _ hxy) as HD.
now split; simp2; psatzl R.
Qed.

Theorem round_DN_cross :
  forall x y, (x < y)%R -> (RN x < RN y)%R -> (DN y < UP x)%R ->
    DN ((DN y + UP x) / 2) = DN y.
Proof with auto'.
intros x y hxy Hxy Hyx.
destruct (Req_dec (DN y) R0) as [Zy|Zy].
(* DN y = 0 *)
destruct (cross_spec hxy Hxy Hyx) as [I[J[K L]]].
rewrite Zy.
assert (Pred beta fexp (UP y) = R0) as HA.
assert (0 < UP x)%R as Nx.
now rewrite <-Zy.
apply Rle_antisym.
assert (UP x <> R0) as Zx by psatzl R.
pose proof Pred_UP_le_DN Zx.
now rewrite <- Zy, <- L, <- J.
apply le_Pred_lt...
now apply generic_format_0.
now' apply generic_format_round.
now rewrite <- L.
rewrite round_DN_Pred with (x := UP y)...
now' apply generic_format_round.
rewrite HA.
now split; simp2; psatzl R.
(* DN y <> 0 *)
rewrite round_DN_Succ with (x := DN y)...
now' apply generic_format_round.
pose proof Succ_DN_ge_UP Zy as HS.
pose proof round_le beta fexp Zceil x y (Rlt_le _ _ hxy) as HU.
now split; simp2; psatzl R.
Qed.

(** An existence lemma for midpoints *)
Theorem midpoint_intro :
  forall x, (DN x < UP x)%R -> midp ((DN x + UP x) / 2)%R.
Proof.
intros x Fx.
rewrite (midpoint12 _ _ _), (midpoint23 _ _ _), (midpoint34 _ _ _).
red.
rewrite round_DN_midp.
rewrite round_UP_midp.
now psatzl R.
Qed.

Theorem lt_midp_RN_DN :
( forall x, x < (DN x + UP x) / 2 -> RN x = DN x )%R.
Proof with auto'.
intros x Hx.
simp2.
assert (x - DN x < UP x - x)%R by psatzl R.
eapply (Rnd_N_pt_unicity _ x (DN x) (UP x)).
apply round_DN_pt...
apply round_UP_pt...
now apply Rlt_not_eq.
apply round_N_pt...
apply Rnd_DN_pt_N with (u := UP x).
apply round_DN_pt...
apply round_UP_pt...
now apply Rlt_le.
Qed.

Theorem gt_midp_RN_UP :
( forall x, (DN x + UP x) / 2 < x -> RN x = UP x )%R.
Proof with auto'.
intros x Hx.
simp2.
assert (UP x - x < x - DN x)%R by psatzl R.
eapply (Rnd_N_pt_unicity _ x (DN x) (UP x)).
apply round_DN_pt...
apply round_UP_pt...
now apply Rgt_not_eq.
apply round_N_pt...
apply Rnd_UP_pt_N with (d := DN x).
apply round_DN_pt...
apply round_UP_pt...
now apply Rlt_le.
Qed.

(** This main theorem makes a heavy use of all the earlier lemmas *)
Theorem RN_lt_exists_midpoint :
  forall x y : R,
    (RN x < RN y)%R ->
    exists2 mu, midp mu & (x <= mu <= y)%R.
Proof with auto'.
intros x y Hxy.
assert (x < y)%R as hxy.
  now' apply Rnot_le_lt; intro K; apply (Rlt_not_le _ _ Hxy); apply round_le.
pose proof round_DN_UP_le beta fexp x as Hx.
pose proof round_DN_UP_le beta fexp y as Hy.
destruct (Rle_lt_dec (UP x) y) as [LE|GT].
(* LE *)
(* |- There does exist one FP number between x and y *)
assert (UP x <= DN y)%R as HUD.
  pose proof round_DN_pt beta fexp y as (h1,(h2,h3)).
  now' apply h3; try apply generic_format_round.
destruct (Rle_lt_or_eq_dec _ _ HUD) as [LT|EQ].
(* LT *)
now' apply round_lt_exists_midpoint_UP_DN.
(* EQ *)
pose (g1 := ((DN x + UP x) / 2)%R).
pose (g2 := ((DN y + UP y) / 2)%R).
(* Note that the gi's are not necessarily a midpoint at this moment *)
destruct (generic_format_EM beta fexp x) as [Fx|Fx];
destruct (generic_format_EM beta fexp y) as [Fy|Fy].
rewrite !round_generic in EQ...
now contradict EQ; apply Rlt_not_eq.
(* F x \ F y *)
pose proof round_DN_UP_lt Fy as HHy.
exists g2; unfold g2.
apply midpoint_intro; psatzl R.
split; [simp2; psatzl R|].
fold g2.
apply Rnot_lt_le; intro K.
contradict Hxy.
rewrite round_generic with (2 := Fx)...
rewrite lt_midp_RN_DN...
rewrite round_generic with (2 := Fx) in EQ...
rewrite EQ.
now apply Rlt_irrefl.
(* F y \ F x *)
pose proof round_DN_UP_lt Fx as HHx.
exists g1; unfold g1.
apply midpoint_intro; psatzl R.
split; [|simp2; psatzl R].
fold g1.
apply Rnot_lt_le; intro K.
contradict Hxy.
rewrite round_generic with (2 := Fy)...
rewrite gt_midp_RN_UP...
rewrite round_generic with (2 := Fy) in EQ...
rewrite EQ.
now apply Rlt_irrefl.
(* ~ F x & ~ F y *)
assert (x <= g1 \/ g2 <= y)%R as Hg.
destruct (Rle_lt_dec x g1) as [HA|HA].
now left.
destruct (Rle_lt_dec g2 y) as [HB|HB].
now right.
absurd (RN x = RN y)%R. now apply Rlt_not_eq.
rewrite gt_midp_RN_UP at 1...
now rewrite lt_midp_RN_DN.
destruct Hg as [Hg1|Hg2].
(* EQ; Hg1 *)
pose proof round_DN_UP_lt Fx as HHx.
exists g1; unfold g1.
apply midpoint_intro; psatzl R.
unfold g1 in *.
split; simp2; psatzl R.
(* EQ; Hg2 *)
pose proof round_DN_UP_lt Fy as HHy.
exists g2; unfold g1.
apply midpoint_intro; psatzl R.
unfold g2 in *.
split; simp2; psatzl R.
(* GT *)
(* |- There does not exist any FP number between x and y *)
assert (DN y < UP x)%R as HDU.
  now apply Rle_lt_trans with (1 := proj1 (round_DN_UP_le beta fexp y)).
pose proof cross_spec hxy Hxy HDU as (C1,(C2,(C3,C4))).
pose (g := ((DN y + UP x) / 2)%R).
exists g.
unfold g.
rewrite (midpoint12 _ _ _), (midpoint23 _ _ _), (midpoint34 _ _ _); red.
rewrite round_DN_cross...
rewrite round_UP_cross...
now split; simp2; psatzl R.
(* Last but not least *)
split; apply Rnot_lt_le; intro K.
contradict Hxy. apply RIneq.Rle_not_lt.
assert (RN x = UP x)%R as HLx.
rewrite gt_midp_RN_UP...
now rewrite C2.
right. (* trick *)
psatzl R.
contradict Hxy. apply RIneq.Rle_not_lt.
assert (RN y = DN y)%R as HLy.
rewrite lt_midp_RN_DN...
now rewrite <- C4.
right.
psatzl R.
Qed.

(* Another result, with a slight variation in its statement w.r.t. the paper *)
Theorem no_midpoint_same_round :
  forall x y : R,
    (x <= y)%R /\ (forall z, (x <= z <= y)%R -> ~ midp z) ->
    RN x = RN y.
Proof with auto'.
intros x y [Hle Hm].
apply Rle_antisym.
apply round_le...
destruct (Rlt_or_le (RN x) (RN y)) as [H|H].
destruct (RN_lt_exists_midpoint H) as [m I J].
exfalso. now apply (Hm _ J).
assumption.
Qed.

Theorem Rle_imp_Rabs_ineq :
  forall x y mu : R, (x <= mu <= y)%R ->
    (Rabs (mu - x) <= Rabs (y - x))%R /\
    (Rabs (y - mu) <= Rabs (y - x))%R /\
    (Rabs (mu - x) = mu - x)%R /\
    (Rabs (y - mu) = y - mu)%R /\
    (Rabs (y - x) = y - x)%R.
Proof.
intros x y mu [Hx Hy].
assert (HH : (x <= y)%R) by (apply Rle_trans with (r2 := mu); trivial).
repeat split; repeat rewrite Rabs_pos_eq; trivial; psatzl R.
Qed.

Context { monotone_exp : Monotone_exp fexp }.

Theorem RN_lt_pow_midp :
  forall x y, format x -> x <> R0 ->
  (x - u (x / Z2R beta) / 2 < y < x + u (x / Z2R beta) / 2)%R ->
  RN y = x.
Proof with auto'.
intros x y Fx Zx [_y y_].
assert (0 < Z2R beta)%R as Pb.
  pose proof radix_gt_0 beta as Zb.
  now apply Rlt_gt; change R0 with (Z2R Z0); apply Z2R_lt.
assert (Z2R beta <> R0) as Zb.
  now apply Rgt_not_eq.
assert (u (x / Z2R beta) <= u x)%R as Hle.
destruct (Rtotal_order x R0) as [LT|[EQ|GT]].
rewrite <- (Ropp_involutive x).
replace (- - x / Z2R beta)%R with (- ((- x) / Z2R beta))%R by now field.
rewrite ulp_opp, ulp_opp.
apply ulp_le ...
pattern (-x)%R at 1.
replace (-x)%R with (-x * 1)%R by field.
unfold Rdiv.
rewrite Rabs_mult.
rewrite (Rabs_pos_eq (/ Z2R beta)).
simpdiv_goal (Z2R beta) Pb...
pose proof radix_gt_1 beta as hb1.
pose proof Z2R_lt _ _ hb1 as hb1'.
simpl in hb1'.
rewrite Rmult_comm, Rabs_mult.
rewrite Rabs_R1.
apply Rmult_le_compat_r.
apply Rabs_pos.
now apply Rlt_le.
now apply Rlt_le, Rinv_0_lt_compat.
(* EQ *)
easy.
(* GT *)
apply ulp_le ...
unfold Rdiv.
rewrite Rabs_mult.
rewrite (Rabs_pos_eq (/ Z2R beta)).
simpdiv_goal (Z2R beta) Pb...
pose proof radix_gt_1 beta as hb1.
pose proof Z2R_lt _ _ hb1 as hb1'.
simpl in hb1'.
pattern (x) at 1.
replace (x) with (x * 1)%R by field.
rewrite Rabs_mult, Rabs_R1.
apply Rmult_le_compat_l.
eapply Rabs_pos.
now auto with real.
now auto with real.
destruct (Rtotal_order y x) as [LT|[EQ|GT]].
(* LT *)
rewrite -> no_midpoint_same_round with (y := x)...
now' rewrite round_generic.
split; [auto with real|].
intros z [_z z_].
set (b := Z2R beta) in *.
pose proof surroundP Fx Zx as
  [I _ (K,L,M,N)|I J (K,L,M,N)|I J (K,L,M,N)].
apply N.
split; simp2; psatzl R.
apply N. fold b.
split; simp2; psatzl R.
apply N. fold b.
split; simp2; psatzl R.
(* EQ *)
rewrite EQ.
now' rewrite round_generic.
(* GT *)
rewrite <- no_midpoint_same_round with (x := x)...
now' rewrite round_generic.
split; [auto with real|].
intros z [_z z_].
set (b := Z2R beta) in *.
pose proof surroundP Fx Zx as
  [I _ (K,L,M,N)|I J (K,L,M,N)|I J (K,L,M,N)].
apply N.
split; simp2; psatzl R.
apply N. fold b.
split; simp2; psatzl R.
apply N. fold b.
split; simp2; psatzl R.
Qed.

(** Now a parity result *)

Hypothesis ZNE : choice = fun n => negb (Zeven n).

Context { HNE : Exists_NE beta fexp }.

Theorem midp_even : forall m, midp m -> Feven beta fexp (RN m).
Proof with auto'.
intros m Mm.
red.
pose proof round_NE_pt beta fexp m as HR.
rewrite <- ZNE in HR.
destruct HR as (I,J); destruct J as [(x,(J,(K,L)))|J].
now exists x; repeat split.
pose (RN' := round beta fexp (Znearest Zeven)).
assert (Rnd_N_pt format m (RN' m)).
now' apply round_N_pt.
exfalso.
pose proof midpoint_no_format Mm as Fm.
assert (DN m <> UP m) by now pose proof round_DN_UP_lt Fm; psatzl R.
assert (m - DN m = UP m - m)%R.
  revert Mm.
  rewrite
    (midpoint12 _ _ _), (midpoint23 _ _ _),
    (midpoint34 _ _ _), (midpoint45 _ _ _).
  unfold midpoint5; tauto.
absurd (RN m = RN' m); [|now rewrite (J (RN' m) H)].
rewrite round_N_middle.
unfold RN'; rewrite round_N_middle.
rewrite ZNE.
destruct (Zeven (Zfloor (scaled_mantissa beta fexp m))).
simpl; easy.
simpl; psatzl R.
easy.
easy.
Qed.

(* The informal proof of Rem5 suggests a variant of Rnd_NG_pt with midpoints. *)

End Nearest.

(*
midpoint_strictly_between xu yd [SUBSUMED_WITH surround_format]
surround with _ < _ < _ ? [USELESS]
midpoint <=> point of discontinuity
 *)
