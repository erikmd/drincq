(* Copyright (c) ENS de Lyon and Inria. All rights reserved. *)

Require Import MoreFlocq.
Require Import Tactics.
Require Import Psatz.
Require Import Flocq.Calc.Fcalc_digits.
Require Import Flocq.Prop.Fprop_div_sqrt_error.
Require Import Flocq.Calc.Fcalc_ops.
Require Import Flocq.Calc.Fcalc_bracket.
Require Import Flocq.Calc.Fcalc_round.
Require Import Flocq.Prop.Fprop_relative.
Require Import Flocq.Prop.Fprop_plus_error.
Require Import Midpoint.

Set Implicit Arguments.

(* Definition two := radix2. Let two := radix2. Local Notation two := radix2. *)
Local Notation two := radix2 (only parsing).
Local Notation pow e := (bpow two e).

Section Remarks.
Local Open Scope Z_scope.
Variables p p' : Z.
Local Notation fexp := (FLX_exp p).
Local Notation fexp2 := (FLX_exp (p + p')).

Local Notation format := (generic_format radix2 fexp).
Local Notation format2 := (generic_format radix2 fexp2).
Local Notation cexp := (canonic_exp two fexp).
Local Notation mant := (scaled_mantissa two fexp).
Local Notation cexp2 := (canonic_exp two fexp2).

Notation DN x := (round two fexp Zfloor x).
Notation UP x := (round two fexp Zceil x).

Lemma FLX_exp2_le : forall e, (e - fexp2 e <= p + p')%Z.
Proof. clear; intros; unfold FLX_exp; omega. Qed.

Variable choice : Z -> bool.

Local Notation rnd_p := (round two (FLX_exp p) (Znearest choice)).
Local Notation rnd_p2 := (round two (FLX_exp (p + p')) (Znearest choice)).

Section Remarks_5_6.
Context { p_gt_0 : Prec_gt_0 p }. (* p >= 1 *)
Context { p'_gt_0 : Prec_gt_0 p' }. (* p' >= 1 *)
Let p_p'_gt_0 : Prec_gt_0 (p + p').
Proof. clear - p_gt_0 p'_gt_0; red in p_gt_0, p'_gt_0 |- *; omega. Qed.
Variable op : R -> R -> R.
Variable x : R.
Hypothesis Slip : rnd_p (rnd_p2 x) <> rnd_p x.

Theorem Rem5 : midpoint radix2 fexp (rnd_p2 x).
Proof with auto'.
pose proof (@FLX_exp_valid (p+p')  p_p'_gt_0) as exp2_valid.
destruct (Rneq_lt Slip) as [Hlt|Hlt].
(* < *)
  destruct RN_lt_exists_midpoint with (2 := Hlt) as [mu M U]...
  destruct (Rle_imp_Rabs_ineq U) as [U1 [U2 [U3 [U4 U5]]]].
  assert (Zeven radix2 = true) as E2 by trivial. (* Need for an even radix *)
  pose proof MID_FLX E2 p_gt_0 M as Fmu.
  simpl in Fmu.
  assert (format2 mu) as F2mu.
    apply generic_inclusion_ln_beta with (fexp1 := FLX_exp (p + 1)); trivial.
    now clear - p'_gt_0; intros; unfold FLX_exp; red in p'_gt_0; omega.
  destruct (Req_dec (rnd_p2 x) mu) as [Heq|Hneq].
(* Heq *)
    now' rewrite Heq.
(* Hneq *)
  destruct (round_N_pt radix2 (FLX_exp (p+p')) choice x) as [I J].
  specialize (J mu F2mu).
  rewrite -> Rabs_minus_sym with (y := x), -> Rabs_minus_sym with (x := mu) in J.
  rewrite ->U3, ->U4, ->U5 in *.
  assert (rnd_p2 x = mu) as Hmu.
    now apply Rle_antisym; psatzl R.
(* HAPPILY *)
  rewrite Hmu.
  exfalso.
  contradiction.
(* > *)
(* LIKEWISE *)
destruct RN_lt_exists_midpoint with (2 := Hlt) as [mu M U]...
destruct (Rle_imp_Rabs_ineq U) as [U1 [U2 [U3 [U4 U5]]]].
assert (Zeven radix2 = true) as E2 by trivial. (* Need for an even radix *)
pose proof MID_FLX E2 p_gt_0 M as Fmu.
simpl in Fmu.
assert (format2 mu) as F2mu.
  apply generic_inclusion_ln_beta with (fexp1 := FLX_exp (p + 1)); trivial.
  now clear - p'_gt_0; intros; unfold FLX_exp; red in p'_gt_0; omega.
destruct (Req_dec (rnd_p2 x) mu) as [Heq|Hneq].
(* Heq *)
now' rewrite Heq.
(* Hneq *)
destruct (round_N_pt radix2 (FLX_exp (p+p')) choice x) as [I J].
specialize (J mu F2mu).
(* DONT
rewrite Rabs_minus_sym with (y := op a b), Rabs_minus_sym with (x := mu) in J.
*)
rewrite ->U3,-> U4, ->U5 in *.
assert (rnd_p2 x = mu) as Hmu by now apply Rle_antisym; psatzl R.
(* Happily : *)
rewrite Hmu.
exfalso.
contradiction.
Qed.

Hypothesis ZNE : choice = fun n => negb (Zeven n).

Hypothesis p_gt_1 : (1 < p)%Z. (* in order to have [Exists_NE two fexp] *)

Theorem Rem6 :
  let y := (rnd_p2 x) in
  exists f : float two,
    rnd_p y = F2R f /\
    canonic two fexp f /\
    Zeven (Fnum f) = true.
Proof.
intros y.
apply midp_even; auto'.
now apply Rem5.
Qed.

End Remarks_5_6.

Section Remark3.
Variables u v : R.
Hypothesis Fu : format u.
Hypothesis Fv : format v.
Hypothesis Zu : u <> R0.
Let w := rnd_p (rnd_p2 (u + v)).
Hypothesis cexp_le : cexp v <= cexp u.
Hypothesis Hp' : 2 <= p'.
Hypothesis Slip : w <> rnd_p (u + v).

Context { p_gt_0 : Prec_gt_0 p }.

Local Instance pp'_gt_0 : Prec_gt_0 (p + p').
Proof.
unfold Prec_gt_0 in p_gt_0 |- *.
clear -Hp' p_gt_0 ; omega.
Qed.
Local Instance p2_gt_0 : Prec_gt_0 (p + 2).
Proof.
red in p_gt_0 |- *.
clear -p_gt_0; omega.
Qed.
Local Instance p'_ge_0 : Prec'_ge_0 p'.
now apply Zle_trans with (2 := Hp').
Qed.

Theorem Rem3_Eq5 : cexp w >= cexp v + p' + 1.
Proof with auto with typeclass_instances.
destruct (Req_dec (u + v) 0) as [Zw|Zw].
elim Slip.
unfold w.
rewrite ->Zw, round_0...
destruct (ln_beta two (u + v)) as (km1,Hk).
specialize (Hk Zw).
assert (pow (km1 - 1) <= Rabs (rnd_p2 (u + v)) <= pow km1)%R as Hk1.
apply round_bounded_large...
unfold Prec_gt_0 in p_gt_0.
unfold FLX_exp.
clear -Hp' p_gt_0 ; omega.
assert (pow (km1 - 1) <= Rabs w)%R as Hk2.
apply abs_round_ge_generic...
apply generic_format_bpow.
unfold Prec_gt_0 in p_gt_0.
unfold FLX_exp.
clear -p_gt_0 ; omega.
apply Hk1.
destruct (generic_format_EM radix2 (FLX_exp (p + p')) (u + v)) as [Hs|Hs].
elim Slip.
unfold w.
rewrite ->round_generic with (2 := Hs)...
generalize (refl_equal (v + u)%R).
rewrite ->Fu at 2.
rewrite ->Fv at 2.
rewrite <- F2R_plus.
unfold Fplus, Falign.
rewrite ->Zle_bool_true with (1 := cexp_le).
rewrite Rplus_comm.
intros Huv.
generalize (generic_format_F2R radix2 (FLX_exp (p + p')) (Ztrunc (mant v) + Ztrunc (mant u) * radix2^(cexp u - cexp v)) (cexp v)).
rewrite <- Huv.
intros Hs'.
generalize (fun H => Hs (Hs' H)).
clear Hs Hs'. intros Hs.
refine (_ (proj1 (Decidable.not_imp_iff _ _ _) Hs)).
intros (Hs1,Hs2). clear Hs.
unfold canonic_exp at 1 in Hs2.
rewrite ->ln_beta_unique with (1 := Hk) in Hs2.
unfold FLX_exp at 1 in Hs2.
generalize (ln_beta_gt_bpow _ _ _ Hk2).
unfold canonic_exp at 1, FLX_exp at 1.
clear - Hs2; omega.
destruct (Z_eq_dec (Ztrunc (mant v) + Ztrunc (mant u) * radix2^(cexp u - cexp v)) 0) as [Zuv|Zuv].
right.
rewrite Zuv.
intros H.
now elim H.
now left.
Qed.

Theorem Rem3_Eq4b : cexp v <= cexp u - p'.
Proof with auto with typeclass_instances.
apply Znot_gt_le.
contradict Slip.
apply f_equal.
apply round_generic...
apply generic_format_FLX.
rewrite ->Fu, Fv.
exists (Float two (Ztrunc (mant v) + Ztrunc (mant u) * radix2 ^ (cexp u - cexp v)) (cexp v)).
unfold Fnum.
split.
rewrite ->Rplus_comm, <- F2R_plus.
unfold Fplus, Falign.
now rewrite Zle_bool_true.
apply Zle_lt_trans with (1 := Zabs_triangle _ _).
rewrite Zabs_Zmult.
apply lt_Z2R.
rewrite ->Z2R_plus, Z2R_mult, 3!Z2R_abs.
rewrite ->Z2R_Zpower by now apply Zle_minus_le_0.
rewrite Z2R_Zpower.
apply Rlt_le_trans with (pow p + (pow p * pow (p' - 1)))%R.
rewrite <- 2!scaled_mantissa_generic by assumption.
apply Rplus_lt_compat.
pattern p at 2 ; replace p with (ln_beta two v - (ln_beta two v - p))%Z by ring.
apply abs_scaled_mantissa_lt_bpow.
rewrite (Rabs_pos_eq (pow (cexp u - cexp v))).
apply Rlt_le_trans with (pow p * pow (cexp u - cexp v))%R.
apply Rmult_lt_compat_r.
apply bpow_gt_0.
pattern p at 2 ; replace p with (ln_beta two u - (ln_beta two u - p))%Z by ring.
apply abs_scaled_mantissa_lt_bpow.
apply Rmult_le_compat_l.
apply bpow_ge_0.
apply bpow_le.
clear -Slip ; omega.
apply bpow_ge_0.
replace (pow p + pow p * pow (p' - 1))%R with ((pow (p' - 1) + 1) * pow p)%R by ring.
assert (Z2R (Zpower two (p' - 1)) = pow (p' - 1)).
apply Z2R_Zpower.
clear -Hp' ; omega.
rewrite <- H, <- (Z2R_plus _ 1).
apply F2R_p1_le_bpow.
apply Zpower_gt_0.
clear -Hp' ; omega.
unfold F2R, Fnum.
rewrite ->H, <- bpow_plus.
apply bpow_lt.
simpl ; clear ; omega.
unfold Prec_gt_0 in p_gt_0 ; clear - Hp' p_gt_0 ; omega.
Qed.

Hypothesis ZNE : choice = fun n => negb (Zeven n).

(* Local Instance HNE : Exists_NE two fexp2.
apply exists_NE_FLX. right. red in p_gt_0. omega.
Qed. *)

Hypothesis p_gt_1 : (1 < p)%Z. (* in order to have [Exists_NE two fexp] *)

Local Instance HNE : Exists_NE two fexp.
now auto'.
Qed.

Theorem Feven_pow :
  forall x,
    is_pow two x ->
    Feven two fexp x.
Proof.
clear - p_gt_0 p_gt_1; intros x Bx.
red.
unfold is_pow in Bx.
destruct Bx as [e He].
destruct HNE as [H|H]; [discriminate|].
pose proof generic_format_bpow' two fexp e as Hb.
assert (fexp e <= e) as Hok. (* OK in FLX *)
  clear - p_gt_1; unfold FLX_exp; omega.
specialize (Hb Hok); clear Hok.
(* destruct (Rtotal_order x R0).
rewrite Rabs_left in He.
pose proof Ropp_eq_compat _ _ He as He'; clear He; rename He' into He.
rewrite Ropp_involutive in He. *)
pose proof generic_format_Rsign x Hb as Hbx.
exists (Float two (Ztrunc (scaled_mantissa two fexp (Rsign x * pow e)))
  (canonic_exp two fexp (Rsign x * pow e))).
repeat split.
rewrite <- Hbx.
now rewrite <- He, <- Rmult_Rsign_Rabs.
red; simpl.
now rewrite <- Hbx.
simpl.
(* Wlog *)
assert (Zeven (Ztrunc (mant (pow e))) = true).
unfold scaled_mantissa.
unfold canonic_exp.
rewrite ln_beta_bpow.
unfold FLX_exp.
replace (pow e * _)%R with (pow (p - 1)).
rewrite <- Z2R_Zpower.
2: clear -p_gt_1; omega.
rewrite Ztrunc_Z2R. simpl.
rewrite Zeven_Zpower. trivial.
clear -p_gt_1; omega.
unfold Zminus. rewrite !Zopp_plus_distr. rewrite Zopp_involutive.
rewrite !bpow_plus.
rewrite !bpow_opp. simpl.
field.
now apply Rgt_not_eq; apply bpow_gt_0.
(* Main *)
unfold Rsign; destruct (Rcompare_spec x R0) as [LT|EQ|GT].
(* LT x 0 *)
rewrite ->Ropp_mult_distr_l_reverse, Rmult_1_l.
now rewrite ->scaled_mantissa_opp, Ztrunc_opp, Zeven_opp.
(* EQ x 0 *)
rewrite ->Rmult_0_l, scaled_mantissa_0.
change R0 with (Z2R Z0).
now rewrite Ztrunc_Z2R.
(* GT x 0 *)
now rewrite Rmult_1_l.
Qed.

Lemma Rdiv_neq0 x y : x <> 0%R -> y <> 0%R -> (x / y <> 0)%R.
Proof.
intros NZx NZy; unfold Rdiv.
pose proof Rinv_neq_0_compat y NZy as NZy'.
rewrite Rmult_comm, <-(Rmult_0_l x).
now apply Rmult_neq_compat_r.
Qed.

Theorem Rem3_Eq4a : cexp u - p - 1 <= cexp v.
Proof with auto'.
apply Znot_gt_le; intro Hgt.
apply Zgt_le_pred in Hgt.
pose proof abs_lt_bpow_prec two _ _ (FLX_exp_ge p) v as Habs.
assert (Rabs v < pow (cexp u - 2))%R as HA.
  apply Rlt_le_trans with (1 := Habs).
  apply bpow_le.
  unfold Zpred in Hgt; clear -Hgt; omega.
assert (Rabs v < ulp two fexp u / 4)%R as Hulp.
  rewrite ulp_neq_0; trivial.
  replace (cexp u) with (cexp u - 2 + 2) by (clear; omega).
  rewrite bpow_plus.
  rewrite ->Rmult_comm, <- Z2R_Zpower, Rmult_comm.
    simpl; unfold Rdiv; rewrite ->Rmult_assoc, Rinv_r, Rmult_1_r; trivial.
    now discrR.
  easy.
set (U := ulp radix2 fexp u) in *.
pose (U2 := ulp radix2 (FLX_exp (p + 2)) u).
assert (U / 4 = U2)%R as HU.
  unfold U, U2.
  rewrite 2!radix2_ulp_FLX_p1...
  2: red; omega.
  replace (p + 1 + 1) with (p + 2) by (clear; omega).
  field.
assert (format2 (u - U / 4)) as HF2sub.
  rewrite HU.
  apply generic_inclusion_ln_beta with (fexp1 := FLX_exp (p + 2)).
  unfold FLX_exp; intros; clear -Hp'; omega.
  apply generic_format_minus_ulp ...
  apply formats...
  red; auto with zarith.
assert (format2 (u + U / 4)) as HF2add.
  rewrite HU.
  apply generic_inclusion_ln_beta with (fexp1 := FLX_exp (p + 2)).
  unfold FLX_exp; intros; clear -Hp'; omega.
  apply generic_format_plus_ulp...
  apply formats...
  red; auto with zarith.
assert (u - U / 4 <= rnd_p2 (u + v) <= u + U / 4)%R as Hineq.
  split.
  (* left *)
  apply round_ge_generic...
  pose proof Rabs_lt_inv _ _ Hulp as [I J].
  now psatzl R.
  (* right *)
  apply round_le_generic...
  pose proof Rabs_lt_inv _ _ Hulp as [I J].
  now psatzl R.
assert (ulp two fexp (u / Z2R two) / 2 = U / 4)%R as HU2.
  unfold U.
  rewrite !ulp_neq_0; trivial; [|now apply Rdiv_neq0; simpl; discrR].
  unfold FLX_exp, canonic_exp.
  rewrite ln_beta_div_beta; trivial.
  unfold Zminus.
  rewrite !bpow_plus; simpl; field.
case (surroundP Fu Zu); intros I J [K L M N].
(* ~ is_pow two u *)
assert (Prec_gt_0 p') as Hp'0 by (red; clear -Hp'; omega).
pose proof @Rem5 p_gt_0 Hp'0 (u + v)%R Slip as h5.
contradict h5; apply N.
rewrite 2!ulp_FLX_p1...
2: now red; omega.
replace (p + 1 + 1) with (p + 2) by ring; simpl.
pose proof @ulp_pos two (FLX_exp(p+2)) u J as Hu.
fold U2 in Hu |- *.
rewrite HU in Hineq.
split; simp2; psatzl R.
(* POWpos *)
fold U in K,L,M,N.
rewrite HU2 in *.
set (mu := (u - U / 4)%R) in *.
set (mu' := (u + U / 4)%R) in *.
assert (rnd_p (rnd_p2 mu) = u) as Hmu.
  pose proof midp_even ZNE K as ER. (* (f,(h1,(h2,h3))) *)
  assert (Zeven two = true) as E2 by easy.
  pose proof MID_FLX E2 p_gt_0 K as F1mu; simpl in F1mu.
  assert (format2 mu) as F2mu.
  apply generic_inclusion_ln_beta with (fexp1 := FLX_exp (p + 1))...
  now intros _; unfold FLX_exp; clear -Hp'; omega.
  rewrite round_generic with (2 := F2mu)...
  assert (~ format mu) as Fmu by now apply midpoint_no_format.
  pose proof surround_format Fmu.
  (* apply Rle_antisym.
  now' apply round_le_generic; try psatzl R. *)
  assert (UP mu = u) as Hu.
  unfold mu.
  (* rewrite round_UP_pred_full with (x := u)...
  now' apply pred_ge_0. *)
  rewrite round_UP_Pred with (x := u)...
  unfold Pred; rewrite Rcompare_Gt...
  rewrite POSpow_pred...
  replace (ulp two fexp (u / Z2R radix2))%R with (2 * (U / 4))%R.
  pose proof @ulp_pos two fexp u Zu as U0... fold U in U0.
  split; simpdiv_cut 4%R; psatzl R.
  rewrite <- HU2; field.
  destruct (round_DN_or_UP two fexp (Znearest choice) mu) as [HD|HUP].
  (* HD *)
  exfalso.
  pose proof Feven_pow I as Eu.
  apply Feven_DN_UP_incompat with (beta := radix2) (fexp := fexp) (mu := mu)...
  rewrite <- HD; intro Zmu.
  pose proof FLX_round_N_eq0 Zmu.
  apply midpoint_no_format with (1 := K).
  now rewrite H0; apply generic_format_0.
  now rewrite Hu.
  now rewrite <- HD.
  now rewrite Hu.
  (* HUP *)
  now rewrite HUP, Hu.
assert (rnd_p (rnd_p2 mu') = u) as Hmu'.
  unfold mu'.
  rewrite HU.
  assert (format2 (u + U2)) as H2.
  apply generic_inclusion_ln_beta with (fexp1 := FLX_exp (p + 2)).
  intros _; unfold FLX_exp; clear -Hp'; omega.
  unfold U2.
  rewrite <- succ_eq_pos; [|now apply Rlt_le].
  apply generic_format_succ ...
  now' apply formats.
  rewrite round_generic with (2 := H2)...
  (* rnd_p (u + U2) = u *)
  rewrite <- no_midpoint_same_round with (x := u)...
  now' rewrite round_generic.
  pose proof @ulp_pos two (FLX_exp (p + 2)) u (Rgt_not_eq _ _ J).
  split; [fold U2 in H; psatzl R|].
  intros z [_z z_].
  destruct (Rle_lt_or_eq_dec _ _ _z) as [LT|EQ].
  (* LT *)
  apply N.
  rewrite <- HU in z_.
  simpdiv_cut 4%R. 2: now apply (@Z2R_lt 0 4); auto with zarith.
  split; simp2; psatzl R.
  (* EQ *)
  rewrite <- EQ.
  now intro Mu; apply midpoint_no_format with (1 := Mu).
assert (rnd_p (u + v) = u) as Huv.
  rewrite RN_lt_pow_midp with (x := u)...
  rewrite HU2.
  pose proof Rabs_lt_inv _ _ Hulp as (Hulp1,Hulp2).
  split; simpdiv_cut 4%R; psatzl R.
contradict Slip.
unfold w.
rewrite Huv.
apply Rle_antisym.
rewrite <- Hmu' at 2.
apply round_le...
apply round_ge_generic...
now' apply generic_format_round.
exact (proj2 Hineq).
rewrite <- Hmu at 1.
apply round_le...
apply round_le_generic...
now' apply generic_format_round.
exact (proj1 Hineq).
(* NEGpos *)
fold U in K,L,M,N.
rewrite HU2 in *.
set (mu := (u + U / 4)%R) in *.
set (mu' := (u - U / 4)%R) in *.
assert (rnd_p (rnd_p2 mu) = u) as Hmu.
  pose proof midp_even ZNE L as ER. (* (f,(h1,(h2,h3))) *)
  assert (Zeven two = true) as E2 by easy.
  pose proof MID_FLX E2 p_gt_0 L as F1mu; simpl in F1mu.
  assert (format2 mu) as F2mu.
  apply generic_inclusion_ln_beta with (fexp1 := FLX_exp (p + 1))...
  now intros _; unfold FLX_exp; clear -Hp'; omega.
  rewrite round_generic with (2 := F2mu)...
  assert (~ format mu) as Fmu by now apply midpoint_no_format.
  pose proof surround_format Fmu.
  assert (DN mu = u) as Hu.
  unfold mu.
  rewrite round_DN_Succ with (x := u)...
  unfold Succ; rewrite Rcompare_Lt...
  rewrite POSpow_pred.
  (* remember (-u)%R as u'. *)
  replace (ulp two fexp (-u / Z2R radix2))%R with (2 * (U / 4))%R.
  pose proof @ulp_pos two fexp u Zu as U0... fold U in U0.
  split; simpdiv_cut 4%R; psatzl R.
  rewrite Rdiv_opp_l, ulp_opp.
  rewrite <- HU2; field.
  now simpl; discrR.
  now apply generic_format_opp.
  now apply is_pow_opp.
  now auto with real.
  destruct (round_DN_or_UP two fexp (Znearest choice) mu) as [HD|HUP].
  (* HD *)
  now rewrite HD, Hu.
  (* HUP *)
  exfalso.
  pose proof Feven_pow I as Eu.
  apply Feven_DN_UP_incompat with (beta := radix2) (fexp := fexp) (mu := mu)...
  now rewrite Hu.
  rewrite <- HUP; intro Zmu.
  pose proof FLX_round_N_eq0 Zmu.
  apply midpoint_no_format with (1 := L).
  now rewrite H0; apply generic_format_0.
  now rewrite Hu.
  now rewrite <- HUP.
assert (rnd_p (rnd_p2 mu') = u) as Hmu'.
  unfold mu'.
  rewrite HU.
  assert (format2 (u - U2)) as H2.
  apply generic_inclusion_ln_beta with (fexp1 := FLX_exp (p + 2)).
  intros _; unfold FLX_exp; clear -Hp'; omega.
  apply generic_format_minus_ulp...
  now' apply formats.
  rewrite round_generic with (2 := H2)...
  rewrite no_midpoint_same_round with (y := u)...
  now' rewrite round_generic.
  pose proof ulp_pos two (FLX_exp (p + 2)) Zu.
  split; [fold U2 in H; psatzl R|].
  intros z [_z z_].
  (* destruct (Rle_lt_or_eq_dec _ _ z_) as [LT|EQ].
     is not needed, given that surround is asymmetric *)
  apply N.
  rewrite <- HU in _z.
  fold U2 in H; rewrite <- HU in H.
  simpdiv_cut 4%R. 2: now apply (@Z2R_lt 0 4); auto with zarith.
  split; simp2; try psatzl R.
assert (rnd_p (u + v) = u) as Huv.
  rewrite RN_lt_pow_midp with (x := u)...
  rewrite HU2.
  pose proof Rabs_lt_inv _ _ Hulp as (Hulp1,Hulp2).
  split; simpdiv_cut 4%R; psatzl R.
contradict Slip.
unfold w.
rewrite Huv.
apply Rle_antisym.
rewrite <- Hmu at 2.
apply round_le...
apply round_ge_generic...
now' apply generic_format_round.
exact (proj2 Hineq).
rewrite <- Hmu' at 1.
apply round_le...
apply round_le_generic...
now' apply generic_format_round.
exact (proj1 Hineq).
Qed.

End Remark3.

Section Remark4.

Hypothesis Hp'p : p + 1 <= p'.
Context { p_gt_0 : Prec_gt_0 p }.
Lemma Hp'2 : 2 <= p'. Proof. clear -p_gt_0 Hp'p; red in p_gt_0; omega. Qed.

Local Instance p'_ge_0_again : Prec'_ge_0 p'.
exact (p'_ge_0 Hp'2).
Qed.

Hypothesis ZNE : choice = fun n => negb (Zeven n).
Hypothesis p_gt_1 : (1 < p)%Z.

Theorem Rem4 a b : format a -> format b -> rnd_p (rnd_p2 (a + b)) = rnd_p (a + b).
Proof with auto'.
wolog a b / (Rabs b <= Rabs a)%R.
(* reduction *)
intros HW Fa Fb; destruct (Rle_or_lt (Rabs b) (Rabs a)) as [h1|h2].
  now apply (HW a b h1).
rewrite Rplus_comm.
apply Rlt_le in h2.
now apply (HW b a h2).
(* main goal *)
intros Habs Fa Fb.
(* Now let's prove [cexp b <= cexp a] *)
destruct (Req_dec b 0) as [Zb|Zb].
  rewrite ->Zb, Rplus_0_r.
  repeat rewrite round_generic...
    now' apply formats.
  now' apply formats.
assert (a <> R0) as Za.
  intro K; rewrite ->K in Habs.
  rewrite Rabs_R0 in Habs.
  pose proof Rabs_pos_lt b Zb as HK.
  now apply (Rlt_not_le _ _ HK).
assert (cexp b <= cexp a) as Hc.
now' apply canonic_exp_monotone_abs.
(* now let us go *)
destruct (Zle_or_lt (cexp a) (cexp b + p + 1)) as [Hle|Hlt].
  rewrite ->round_generic with (fexp := fexp2)...
  assert (0 <= p) as Pp by (clear -p_gt_0; red in p_gt_0; omega).
  rewrite <- Zplus_assoc in Hle.
  pose proof (@format_plus_cexp _ _ (p + 1) a b Fa Fb Pp (Zle_succ _) Hc Hle) as Fab.
  apply generic_inclusion_ln_beta with (2 := Fab); intros _.
  unfold FLX_exp; clear -p_gt_0 Hp'p; omega.
pose proof (@Rem3_Eq4a a b Fa (*Fb*) Za (*Hc*) Hp'2) as Hineq. (* here we need [2 <= p']. *)
match goal with |- ?a = ?b => destruct (Req_dec a b) as [Heq|Hneq] end; trivial.
assert (0 < p) as Pp by (clear -p_gt_0; red in p_gt_0; omega).
specialize (Hineq Hneq Pp ZNE p_gt_1).
destruct (Zle_not_lt(cexp a)(cexp b+p+1));trivial;clear -p_gt_1 Hlt Hineq;omega.
Qed.

End Remark4.

(*
SearchAbout Rnd_NG_pt.
Check satisfies_any_imp_NG.
Print satisfies_any.
Print NG_existence_prop.
Print round_pred_total.
*)

Theorem bpow_ge_1 : forall beta n, 0 <= n -> (1 <= bpow beta n)%R.
clear; intros beta n Hn.
change R1 with (bpow beta 0).
now apply bpow_le.
Qed.

Section Remark_cexp_s.
Variables a b s : R.
Hypothesis Fa : format a.
Hypothesis Fb : format b.
Hypothesis sDef : s = rnd_p (rnd_p2 (a + b)).
Let r := (a + b - s)%R.
Hypothesis p_gt_0 : 0 < p.
Hypothesis p'_ge_0 : 0 <= p'.
Let p_ge_0 : 0 <= p. Proof Zlt_le_weak _ _ p_gt_0.
Local Instance p_p'_gt_0 : Prec_gt_0 (p + p').
Proof. now red; auto with zarith. Qed.

Theorem Rem2 :
  cexp b <= cexp a ->
  s <> R0 ->
  cexp s <= 1 + (cexp a).
Proof with auto'.
intros Hc NZs.
assert (Rabs (a + b) <= (pow p - 1) * pow (1 + cexp a))%R as Habs.
  pose proof (FLX_Rabs_le p_ge_0 Fa) as Ha.
  pose proof (FLX_Rabs_le p_ge_0 Fb) as Hb.
  apply Rle_trans with (1 := Rabs_triang _ _).
  rewrite bpow_plus; simpl.
  rewrite double.
  rewrite Rmult_plus_distr_l.
  apply Rplus_le_compat; trivial.
  pose proof (bpow_le two _ _ Hc) as Hpc.
  apply Rle_trans with (1 := Hb).
  apply Rmult_le_compat_l; trivial.
  pose proof (bpow_ge_1 two p_ge_0) as Hpow.
  psatzl R.
assert (format ((pow p - 1) * pow (1 + cexp a))) as Fpow.
  rewrite Zplus_comm. (* useful for simpl *)
  apply FLX_format_Rabs_Fnum with (fx := Float _ (two^p-1) (cexp a + 1)).
    unfold F2R; simpl; f_equal; rewrite Z2R_minus; f_equal.
    now change (2^p) with (two^p); rewrite Z2R_Zpower.
  simpl; rewrite ->Rabs_pos_eq (* + subgoal *), Z2R_minus; simpl.
    change (2^p) with (two^p); rewrite Z2R_Zpower; [psatzl R|assumption].
  pose proof (bpow_ge_1 two p_ge_0) as Hpow.
  rewrite Z2R_minus.
  change (2^p) with (two^p); rewrite Z2R_Zpower; simpl; [psatzl R|easy].
assert (Rabs s <= (pow p - 1) * pow (1 + cexp a))%R as Hle.
  rewrite sDef.
  apply abs_round_le_generic...
  apply abs_round_le_generic...
  now' apply formats.
assert (Rabs s < pow (p + (1 + cexp a)))%R as Hlt.
  apply Rle_lt_trans with (1 := Hle).
  rewrite ->bpow_plus with (e1 := p).
  apply Rmult_lt_compat_r.
  now apply bpow_gt_0.
  psatzl R.
clear Hle.
unfold canonic_exp at 1, FLX_exp at 1.
apply Zplus_le_reg_l with p.
ring_simplify (p + (ln_beta radix2 s - p))%Z.
now apply ln_beta_le_bpow.
Qed.

End Remark_cexp_s.

Section Remark8.
Context {p_gt_0 : Prec_gt_0 p}.
Hypothesis p'_ge_2 : 2 <= p'.

Let p_ge_0 : 0 <= p. Proof Zlt_le_weak _ _ p_gt_0.
Local Instance pp'_gt_0' : Prec_gt_0 (p + p') := pp'_gt_0 p'_ge_2.
Let p'_ge_0 : 0 <= p'. Proof. now auto with zarith. Qed.

Let cexp2_minus : forall u, (cexp2 u = cexp u - p').
Proof.
clear; intros u.
unfold FLX_exp, canonic_exp; auto with zarith.
Qed.

Hypothesis ZNE : choice = fun n => negb (Zeven n).
Hypothesis p_gt_1 : (1 < p)%Z.

Theorem Rem8 a b s (Fa : format a) ( Fb : format b) (sDef : s = rnd_p (rnd_p2 (a + b))):
   generic_format two (FLX_exp (p + 2)) (a + b - s)%R .
Proof with auto'.
wolog a b s Fa Fb sDef / (cexp b <= cexp a); [|intros Hc].
(* reduction *)
  intro HW.
  destruct (Zle_or_lt (cexp b) (cexp a)) as [Hle|Hlt].
    now apply HW.
  simpl in HW |- *.
  rewrite sDef; rewrite Rplus_comm.
  now apply HW; auto with zarith.
(* main case *)
destruct (Req_dec a R0) as [Za|Za].
  rewrite ->sDef, Za.
  rewrite !Rplus_0_l.
  rewrite ->round_generic with (fexp := fexp2)...
  apply formats...
  now red; clear; omega.
  rewrite round_generic...
  rewrite Rminus_diag_eq...
  now apply generic_format_0.
  apply formats...
destruct (Req_dec (rnd_p (rnd_p2 (a + b))) (rnd_p (a + b))) as [Heq|Hneq].
  assert (format (a+b -s)%R) as Fr.
    rewrite Heq in sDef.
    rewrite sDef.
    replace (a + b - _)%R with (- (rnd_p (a + b) - (a + b)))%R by ring.
    apply generic_format_opp.
    apply plus_error...
  now apply formats.
pose proof @Rem3_Eq4a a b Fa Za p'_ge_2 Hneq _ ZNE p_gt_1 as H4a.
pose proof Rem3_Eq5 Fa Fb Hc p'_ge_2 Hneq as H5.
rewrite <- sDef in H5.
(* Let us take into account the case [s = 0] *)
case (Req_dec s R0) as [Zs|Zs].
  exfalso.
  rewrite Zs in sDef.
  symmetry in sDef.
  pose proof FLX_round_N_eq0 sDef as HK...
  rewrite sDef in Hneq.
  pose proof FLX_round_N_eq0 HK as HK2.
  now' rewrite ->HK2, round_0 in Hneq.
(* Let us reuse [Rem2]. *)
pose (s' := rnd_p2 (a + b)).
destruct (Req_dec s' R0) as [Zs'|Zs'].
  now' unfold s' in *; rewrite ->Zs', round_0 in sDef.
pose proof Rem2 Fa Fb sDef p_gt_0 p'_ge_0 Hc Zs as Hs.
assert (Fs : format s) by (now' rewrite sDef; apply generic_format_round).
rewrite Rplus_comm.
generalize (eq_refl (b + a - s)%R).
rewrite ->Fa, Fb, Fs at -1.
rewrite <- F2R_plus.
unfold Fplus, Falign.
rewrite ->Zle_bool_true with (1 := Hc).
assert (cexp b <= cexp s) as Hbs by (clear -H5 p'_ge_0; omega).
rewrite <- F2R_minus.
unfold Fminus, Fopp.
unfold Fplus, Falign.
rewrite ->Zle_bool_true with (1 := Hbs).
intro Heq.
eapply FLX_format_Rabs_Fexp; [reflexivity|simpl].
simpl in Heq; rewrite <- Heq.
replace (b + a - s)%R with (- (s' - (a + b)) + (s' - s))%R by ring.
eapply Rle_lt_trans; [apply Rabs_triang|].
rewrite Rabs_Ropp.
assert (F2a : format2 a) by now apply formats.
assert (F2b : format2 b) by now apply formats.
pose proof error_le_half_ulp_round two fexp2 choice (a+b) as Hs'.
pose proof error_le_half_ulp_round two fexp choice s' as Hss'.
rewrite ulp_neq_0 in Hs'; trivial.
rewrite cexp2_minus in Hs'.
rewrite ulp_neq_0 in Hss'; trivial.
fold s' in Hs'.
assert (-1 + cexp s <= 1 + p + cexp b)%Z as Hc1 by (clear -H4a Hs; omega).
assert (cexp s' <= cexp s) as Hc'.
  rewrite sDef in Zs.
  pose proof canonic_exp_round_ge two fexp (Znearest choice) s' Zs as Hcan.
  now unfold s' at 2 in Hcan; rewrite <- sDef in Hcan.
(* <- Since (-1 + (cexp s' - p') <= 1 + p + cexp b - p'). *)
assert (-1 + (cexp s' - p') < 1+p+cexp b) as Hc2 by (clear -Hc' Hc1 p'_ge_2; omega).
pattern (Rabs (s' - s)); rewrite ->Rabs_minus_sym.
replace (s - s')%R with (rnd_p s' - s')%R. 2: now rewrite sDef.
pose proof bpow_le radix2 _ _ Hc1 as Hc1'.
rewrite bpow_plus in Hc1'; simpl (pow (- 1)) in Hc1'.
pose proof bpow_lt radix2 _ _ Hc2 as Hc2'.
rewrite bpow_plus in Hc2'; simpl (pow (-1)) in Hc2'.
rewrite sDef in Hc1'; fold s' in Hc1'.
pose proof Rle_trans _ _ _ Hss' Hc1'  as Hs1.
pose proof Rle_lt_trans _ _ _ Hs' Hc2' as Hs2.
pose proof Rplus_lt_le_compat _ _ _ _ Hs2 Hs1 as Hlt.
apply Rlt_le_trans with (1 := Hlt).
replace (p + 2 + cexp b) with (1 + (1 + p + cexp b)) by ring.
right. (* Equal *)
symmetry; rewrite bpow_plus; change (pow 1) with 2%R; ring.
now unfold s'; rewrite <-sDef.
Qed.

Variables a b s : R.
Hypothesis Fa : format a.
Hypothesis Fb : format b.
Hypothesis sDef : s = rnd_p (rnd_p2 (a + b)).
Let r := (a + b - s)%R.

Theorem Rem8_Eq6 : rnd_p (rnd_p2 r) = rnd_p r.
Proof with auto'.
rewrite ->round_generic with (fexp := fexp2)...
apply generic_inclusion_ln_beta with (FLX_exp (p + 2)).
  intros _; unfold FLX_exp; clear -p_gt_0 p'_ge_2; omega.
now apply Rem8.
Qed.

End Remark8.

End Remarks.
