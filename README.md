Drincq − Double roundings in Coq
================================

Prerequisites
-------------

You will need the [Coq](https://coq.inria.fr/) proof assistant (v8.4 or v8.5) with the following library:

* [Flocq](http://flocq.gforge.inria.fr/>) 2.5

If you are managing your Coq installation using [OPAM](http://opam.ocaml.org/), you can install the Flocq library using the following command:

    $ opam install --jobs=2 coq-flocq

Note that the Flocq package is hosted in the OPAM repository dedicated to stable Coq libraries. So you have to type the following command beforehand, if your OPAM installation does not yet know about this repository.

    $ opam repo add coq-released https://coq.inria.fr/opam/released

Configuring and Compiling
-------------------------

Ideally, you should just have to type:

    $ coq_makefile -f _CoqProject -o Makefile && make

The Makefile may take an argument `COQBIN` (passed as environment variable)
to specify the directory where Coq binaries resides.
